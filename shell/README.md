# shellの使える技法

## set -e
- shellは1つのコマンドがエラーでも、そのまま実行を継続してしまう。
- このオプションをつけることで、1つでもerrorが発生したらscript全体の処理をstopしてくれる。
- 注意
    - ~~`cmd1 || cmd2`のように、コマンドが失敗したらxxx系が使えなくなる。~~ → そんなことなかった。
    - https://qiita.com/youcune/items/fcfb4ad3d7c1edf9dc96
- 検証
    - `set -e`をつけても、`cmd1 || cmd2`は評価される。
    ```
    set -e; echo '{"test":"testdayo"}' | jq --exit-status -r '.xxx' || echo "KeyError"
    null
    KeyError
    ```
## set -o pipfail
- shellでは、pipe中のコマンドがエラーしてもpipeは実行され続ける。
- このオプションをつけることで、pipe中のコマンドがfailしたらpipe全体がfailになる。

## Logging function
```
info {
    local fname=${BASH_SOURCE[1]##*/}
    echo -e "$(date '+%Y-%m-%dT%H:%M:%S') ${PROCNAME} (${fname}:${BASH_LINENO[0]}:${FUNCNAME[1]}) $@"
}

error {
    local fname=${BASH_SOURCE[1]##*/}
    echo -e "$(date '+%Y-%m-%dT%H:%M:%S') ${PROCNAME} (${fname}:${BASH_LINENO[0]}:${FUNCNAME[1]}) $@" 1>&2
    exit 1
}
```
- echoの代わりに上記関数を使えば、`2015-08-27T18:58:14 libtest.sh (libtest.sh:5:main) message`のようなlog出力ができる。
    - https://qiita.com/Ets/items/cd3baa5cecbf553f822d
- errorでは、`exit 1`をしているのでerrorを出すことが可能。(`set -e`と組み合わせれば、処理中止もできる。)
- bashが必須なので注意

## Logging all script
- main関数に入れた内容を、logging & 実行表示したい場合
    - `$@`でshell scriptが受け取った引数を全て、main関数に渡している。
    - teeコマンドで、stdoutに出しつつfile保存をしている。
```
main {
    # 処理内容
}
LOGFILE="/tmp/test_log"
main "$@" 2>&1 | tee -a "${LOGFILE}"
```
- script全体を、logging & 実行表示したい場合
    - `exec 1> <file>`: stdoutの内容を、fileにリダイレクトできる。
    - `>(command)`: プロセス置換の書き込み。(fileに出力された内容を、直接コマンドにリダイレクトできる。)
    - 結局stdoutが、ファイルとして扱われた`tee -a ${LOGFILE}`に渡されて、teeコマンドコンソール出力とファイル保存を行う。
```
LOGFILE="/tmp/test_log"
exec 1> >(tee -a ${LOGFILE})
exec 2> >(tee -a ${LOGFILE})
```
- 参考
    - プロセス置換: https://techblog.raccoon.ne.jp/archives/53726690.html
    - execコマンド: https://qiita.com/blueskyarea/items/c72adf415f8ec1b40f10
    - execによるログ保存: https://genzouw.com/entry/2020/01/06/120027/1845/

## [BEST PRACTICE] Comments
```
#######################################
# Print Hello to someone
# Usage: hello <name>
# Returns:
#   None
#######################################
function hello() {
  echo "Hello ${1}"
}
```
- 参考
    - google公式: https://google.github.io/styleguide/shellguide.html
    - Qiita翻訳: https://qiita.com/ma91n/items/5f72ca668f1c58176644

## 変数展開
- filter機能
    - URLの最後だけ切り抜く等、変数をfilterできる。
```
$ URL=https://example.com/testdayo-; echo ${URL##*/}
testdayo-
```
- defalut値機能(準変数)
    - `${var:-word}`でvarが空の場合に、<word>に置き換わる。
    - 他にも色々ある。
```
$ echo ${YOUR_NAME:-"Who are you"}
Who are you
$ YOUR_NAME="HIROKI"; echo ${YOUR_NAME:-"Who are you"}
HIROKI
```
- 参考
    - Qittaまとめ: https://qiita.com/bsdhack/items/597eb7daee4a8b3276ba

## [jq] `--exit-status`
- 通常
    - 値が存在しなくても、exit statusは0で**正常扱い**
```
$ echo '{"test":"testdayo"}' | jq -r '.xxx'; echo $? 
null
0
```
- `--exit-status`あり
    - 値が存在しないと、exit statusは1で**異常扱い**
    - これによって、error文を出すなどできる。
```
hiroki@MacBook-Pro shell % echo '{"test":"testdayo"}' | jq --exit-status -r '.xxx'; echo $?
null
1
```

## sshでawkを使うとき
- `$`が評価されないので、`\$`のようにエスケープを入れる。
- 参考
    - https://qiita.com/aibou/items/159a18ca70ac87b40bad#sshコマンドでリモートホストにawkコマンドを流すとき
