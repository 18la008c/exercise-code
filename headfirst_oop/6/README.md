# p291
## 求められているfeature
- ターン制の戦略ゲームルールだが、どのルールパターンかはゲームデザイナーが選べるような柔軟性が必要。
- 複数の時代に適応させる
- さまざまな地形のサポートが必要(高さ、幅、マスの地形)

# 298
## featureマグネット
- 新しいゲームを作成する
    - 1, 2, 5
- 既存のゲームを修正する
    - 4,
- ゲームを配置する
    - 3, 6
- 感想
   - 流石にわけわからん。。。言葉遊びのレベル

# 303
## 感想
- 確かにユースケース図を作るのは大事
- 特にアクターが誰であるかを起点に、ユースケースを作っていくことで自然と処理を分離できる
- これによって、非常に困っていた**何をclassにしてどう連携するべきか**の1つの解決策になる。

# 311
- Game関連のモジュール(ゲーム自身が使用する想定)
   - Units(コマを制御するモジュール)
   - Time(時間軸を制御するモジュール)
   - Board(マス目,地形を制御するモジュール)
   - Controller(ターン、移動を制御するモジュール)
   - Battle(戦闘を制御するモジュール)
- Create関連のモジュール(デザイナーが使用する想定)
- 両方が使うモジュール(便利ツール想定)
   - Tool

# 結局システムを作るときにどういう順番で作るか？
1. 必要なfeatureを洗い出す。
    - [今回の例]ゲームのフレームワークを作るのに必要な機能のざっくり概要
2. ユースケース図
    - アクターと、ユースケースのみ記載した図を作る。
    - [今回の例]ゲームデザイナーが、フレームワークを使用してゲームを作る機能(新規ゲーム作成、ゲームの配置等)
    - [今回の例]ゲーム自身が、ゲームを進行するのに必要な機能(コマの移動、ターンの移動等)がユースケースになる。
3. ユースケースに、featureを当てはめていく。
    - featureを元に、ユースケースを考えるも良いし、ユースケースにfeatureを当てはめていくでも良い
    - [今回の例]新規ゲーム作成に、時間軸の設定や地形の設定をするなど
4. ドメイン分析
    - 顧客がわかる言葉に置き換えて、コミュニケーションをとる
    - 一人開発&エンジニアとの会話だけなのでいらない
5. モジュール設計
    - 必要そうに思われるモジュールを列挙していく(ここは正直経験がものを言いそう)
    - 今後どんなコードが必要になるかは、ある程度パターン化できる部分もあるだろうし
6. デザインパターン検討
    - 出てきたモジュール群から、適用できそうなデザインパターンがあるか探す
    - [今回の例]MVCパターンがまさにそう