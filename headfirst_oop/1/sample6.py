# p42
# sample5.pyであげた問題点の対象
# 1. sample7.pyで変更する。
# 2. 既に実装ずみ
# 3. GuitarSpecに__eq__メソッドの追加
# 4. 変更なしでOK

from dataclasses import dataclass
from enum import Enum


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


@dataclass
class GuitarSpec:
    builder: Builder
    model: str
    type: Type
    backwood: Wood
    topwood: Wood

    def __eq__(self, other):
        # pythonの特殊メソッド
        # guitar_spec_self == guitar_spec_otherを実行すると呼び出される。

        if (other.builder) and (self.builder != other.builder):
            return False

        if (other.model) and (self.model != other.model):
            return False

        if (other.type) and (self.type != other.type):
            return False

        if (other.backwood) and (self.backwood != other.backwood):
            return False

        if (other.topwood) and (self.topwood != other.topwood):
            return False

        return True

    def __str__(self) -> str:
        # Guitarクラスに依存しないような、単純な羅列
        return f"""builder: {self.builder.value}
model: {self.model}
type: {self.type.value}
backwood: {self.backwood.value}
topwood: {self.topwood.value}"""


class Guitar:
    # Guiterとしての情報は、GuiterSpecに委譲(has-a)した。
    def __init__(self, serial_number: str, price: float, spec: GuitarSpec) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec

    def __str__(self) -> str:
        # GuiterSpecの変化に自動追従するには、自由な文字列から→paramの規則的な列挙に変えないと行けない。
        formatted_spec = str(self.spec).replace("\n", "\n    ")
        return f"${self.price}\n    {formatted_spec}"


class Inventory:
    def __init__(self) -> None:
        self.guitars = []

    def add_guitar(self, guitar: Guitar) -> None:
        self.guitars.append(guitar)

    def search(self, searched_spec: GuitarSpec):
        # 比較処理は全て、GuitarSpecに委譲した
        # これによって、GuitarSpecについては、__eq__があることのみ知っていれば良い!
        # → 開放閉塞の原理を満たす。

        matching_guitar = [
            guitar
            for guitar in self.guitars
            if guitar.spec == searched_spec
        ]
        if matching_guitar:
            print(f"お探しのguitarが、{len(matching_guitar)}件見つかりました。")
            for g in matching_guitar:
                print("---")
                print(g)
            return matching_guitar

        print("お探しのguitarは見つかりませんでした。")
        return None


def init_invetory():
    guitar_fender_a = Guitar(
        serial_number="V95693",
        price=1499.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder
        )
    )
    guitar_fender_b = Guitar(
        serial_number="V9512",
        price=1599.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder
        )
    )
    inventory = Inventory()
    inventory.add_guitar(guitar_fender_a)
    inventory.add_guitar(guitar_fender_b)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    searched_guitar = GuitarSpec(Builder.FENDER,
                                 "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder)
    inventory.search(searched_guitar)
