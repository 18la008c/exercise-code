# p16
# 文字列比較をなくすため、一部Enum型を使用し頑強に
from dataclasses import dataclass
from enum import Enum


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


@dataclass
class Guitar:
    serial_number: str
    price: float
    builder: Builder
    model: str
    type: Type
    backwood: Wood
    topwood: Wood

    def __str__(self) -> str:
        return (
            f"{self.builder.value} {self.model} {self.type.value} guitar\n"
            f"    * {self.backwood.value} back and sides\n"
            f"    * {self.topwood.value} top"
        )


class Inventory:
    def __init__(self) -> None:
        self.guitars = []

    def add_guitar(self, guitar: Guitar) -> None:
        self.guitars.append(guitar)

    def search(self, searched_guitar: Guitar):
        def is_same_guitar(guitar_a, guitar_b):
            """
            guitar_a: Invetory's guitar
            guitar_b: Searched guitar
            """

            if (guitar_b.builder) and (guitar_a.builder != guitar_b.builder):
                return False

            if (guitar_b.builder) and (guitar_a.model != guitar_b.model):
                return False

            if (guitar_b.builder) and (guitar_a.type != guitar_b.type):
                return False

            if (guitar_b.builder) and (guitar_a.backwood != guitar_b.backwood):
                return False

            if (guitar_b.builder) and (guitar_a.topwood != guitar_b.topwood):
                return False

            return True

        for guitar in self.guitars:
            if is_same_guitar(guitar, searched_guitar):
                print(guitar)
                return guitar
            print("お探しのguitarは見つかりませんでした。")
            return None


def init_invetory():
    guitar_fender_a = Guitar("V95693", 1499.95, Builder.FENDER,
                             "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder)
    inventory = Inventory()
    inventory.add_guitar(guitar_fender_a)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    # 存在しない値の場合
    try:
        searched_guitar = Guitar(None, None, Builder.NONAME,
                                 "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder)
        inventory.search(searched_guitar)
    except AttributeError as e:
        print(e)
        print("Enumを使うと存在しない入力値はAttributeErrorで弾ける")

    # 正規手順
    searched_guitar = Guitar(None, None, Builder.FENDER,
                             "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder)
    inventory.search(searched_guitar)
