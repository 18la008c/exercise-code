# p4
# この設計だと、文字列の大文字、小文字など入力ゆれに脆弱な設計
from dataclasses import dataclass


@dataclass
class Guitar:
    serial_number: str
    price: float
    builder: str
    model: str
    type: str
    backwood: str
    topwood: str

    def __str__(self) -> str:
        return (
            f"{self.builder} {self.model} {self.type} guitar\n"
            f"    * {self.backwood} back and sides\n"
            f"    * {self.topwood} top"
        )


class Inventory:
    def __init__(self) -> None:
        self.guitars = []

    def add_guitar(self, guitar: Guitar) -> None:
        self.guitars.append(guitar)

    def search(self, searched_guitar: Guitar):
        # 悪い設計例
        # 対象の比較に対して弱すぎる。
        def is_same_guitar(guitar_a, guitar_b):
            """
            guitar_a: Invetory's guitar
            guitar_b: Searched guitar
            """

            if (guitar_b.builder) and (guitar_a.builder != guitar_b.builder):
                return False

            if (guitar_b.builder) and (guitar_a.model != guitar_b.model):
                return False

            if (guitar_b.builder) and (guitar_a.type != guitar_b.type):
                return False

            if (guitar_b.builder) and (guitar_a.backwood != guitar_b.backwood):
                return False

            if (guitar_b.builder) and (guitar_a.topwood != guitar_b.topwood):
                return False

            return True

        for guitar in self.guitars:
            if is_same_guitar(guitar, searched_guitar):
                print(guitar)
                return guitar
            print("お探しのguitarは見つかりませんでした。")
            return None


def init_invetory():
    guitar_fender_a = Guitar("V95693", 1499.95, "Fender",
                             "Stratocastor", "electric", "Alder", "Alder")
    inventory = Inventory()
    inventory.add_guitar(guitar_fender_a)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    # fenderと小文字の場合
    searched_guitar = Guitar("", 0, "fender", "Stratocastor",
                             "electric", "Alder", "Alder")
    inventory.search(searched_guitar)

    # Fenderと大文字の場合
    searched_guitar = Guitar("", 0, "Fender", "Stratocastor",
                             "electric", "Alder", "Alder")
    inventory.search(searched_guitar)
