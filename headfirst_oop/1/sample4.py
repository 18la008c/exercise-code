# p32
# guitarの責務を分離するために、guitarと、specに分離
# 正直このsample codeだとguitar単体で十分な気がするが。。。

from dataclasses import dataclass
from enum import Enum


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


@dataclass
class GuitarSpec:
    # guitarとしての情報だけ別class管理に
    # これでguitarの振る舞い(演奏とか、priceとか)と、guitar自体の情報を分離できた。
    # この簡単な例だと、さほど意味ないけど。。。
    builder: Builder
    model: str
    type: Type
    backwood: Wood
    topwood: Wood


class Guitar:
    # Guiterとしての情報は、GuiterSpecに委譲(has-a)した。
    def __init__(self, serial_number: str, price: float, spec: GuitarSpec) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec

    def __str__(self) -> str:
        return (
            f"{self.spec.builder.value} {self.spec.model} {self.spec.type.value} guitar\n"
            f"    * {self.spec.backwood.value} back and sides\n"
            f"    * {self.spec.topwood.value} top\n"
            f"    * ${self.price}"
        )


class Inventory:
    def __init__(self) -> None:
        self.guitars = []

    def add_guitar(self, guitar: Guitar) -> None:
        self.guitars.append(guitar)

    def search(self, searched_spec: GuitarSpec):
        def is_same_guitar(spec_a, spec_b):
            """
            spec_a: Invetory's guitar spec
            spec_b: Searched guitar spec
            """

            if (spec_b.builder) and (spec_a.builder != spec_b.builder):
                return False

            if (spec_b.builder) and (spec_a.model != spec_b.model):
                return False

            if (spec_b.builder) and (spec_a.type != spec_b.type):
                return False

            if (spec_b.builder) and (spec_a.backwood != spec_b.backwood):
                return False

            if (spec_b.builder) and (spec_a.topwood != spec_b.topwood):
                return False

            return True

        matching_guitar = [
            guitar
            for guitar in self.guitars
            if is_same_guitar(guitar.spec, searched_spec)
        ]
        if matching_guitar:
            print(f"お探しのguitarが、{len(matching_guitar)}件見つかりました。")
            for g in matching_guitar:
                print("---")
                print(g)
            return matching_guitar

        print("お探しのguitarは見つかりませんでした。")
        return None


def init_invetory():
    guitar_fender_a = Guitar(
        serial_number="V95693",
        price=1499.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder
        )
    )
    guitar_fender_b = Guitar(
        serial_number="V9512",
        price=1599.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder
        )
    )
    inventory = Inventory()
    inventory.add_guitar(guitar_fender_a)
    inventory.add_guitar(guitar_fender_b)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    searched_guitar = GuitarSpec(Builder.FENDER,
                                 "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder)
    inventory.search(searched_guitar)
