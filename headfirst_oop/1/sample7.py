# p42
# searchメソッドを、GuiterSpecに委譲(__eq__)したことによる開放閉塞の原理の効果をみる。
# 1. num_stringsの実装をしても、他のクラスの変更が必要ないことを確認する。
#    → 変更に対して開かれていて、他のクラスは閉じていることの確認用


from dataclasses import dataclass
from enum import Enum


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


@dataclass
class GuitarSpec:
    builder: Builder
    model: str
    type: Type
    backwood: Wood
    topwood: Wood
    num_strings: int

    def __eq__(self, other):
        # pythonの特殊メソッド
        # guitar_spec_self == guitar_spec_otherを実行すると呼び出される。

        if (other.builder) and (self.builder != other.builder):
            return False

        if (other.model) and (self.model != other.model):
            return False

        if (other.type) and (self.type != other.type):
            return False

        if (other.backwood) and (self.backwood != other.backwood):
            return False

        if (other.topwood) and (self.topwood != other.topwood):
            return False

        if (other.num_strings) and (self.num_strings != other.num_strings):
            return False

        return True

    def __str__(self) -> str:
        return f"""builder: {self.builder.value}
model: {self.model}
type: {self.type.value}
backwood: {self.backwood.value}
topwood: {self.topwood.value}
num_strings: {self.num_strings}"""


class Guitar:
    def __init__(self, serial_number: str, price: float, spec: GuitarSpec) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec

    def __str__(self) -> str:
        # num_stringsを追加しても、Guitarには変更が入らないが。。。 出力分の自由が効かない。。。
        # こういう、backendの変更に対する、GUIの追従ってどうやるんだろう？
        formatted_spec = str(self.spec).replace("\n", "\n    ")
        return f"${self.price}\n    {formatted_spec}"


class Inventory:
    def __init__(self) -> None:
        self.guitars = []

    def add_guitar(self, guitar: Guitar) -> None:
        self.guitars.append(guitar)

    def search(self, searched_spec: GuitarSpec):
        # GuiterSpecに、num_stringsを追加しても、なんの変更も要らなかった！

        matching_guitar = [
            guitar
            for guitar in self.guitars
            if guitar.spec == searched_spec
        ]
        if matching_guitar:
            print(f"お探しのguitarが、{len(matching_guitar)}件見つかりました。")
            for g in matching_guitar:
                print("---")
                print(g)
            return matching_guitar

        print("お探しのguitarは見つかりませんでした。")
        return None


def init_invetory():
    guitar_fender_a = Guitar(
        serial_number="V95693",
        price=1499.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder,
            12
        )
    )
    guitar_fender_b = Guitar(
        serial_number="V9512",
        price=1599.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder,
            12
        )
    )
    inventory = Inventory()
    inventory.add_guitar(guitar_fender_a)
    inventory.add_guitar(guitar_fender_b)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    searched_guitar = GuitarSpec(Builder.FENDER,
                                 "Stratocastor", Type.ELECTRIC, Wood.Alder, Wood.Alder, 12)
    inventory.search(searched_guitar)
