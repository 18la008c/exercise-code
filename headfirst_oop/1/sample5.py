# p39

"""
Q1.
- GuitarSpecクラスに、num_stringsを追加

Q2.
- pythonかつ、さほど編集NGparameterではないので、getter/setterはいらない
- そこまでやりたいなら、dataclassのfrozenを使う

Q3.
- Inventoryのsearch methodに判定文を追加しないとNG
- Guiterクラスの__str__も変更しないとNG
    - これは、出力文を自分でカスタマイズしたいならば避けられない気がする。
    - とは言え、GuitarSpecの変更がクラスが異なるGuitarまで「考慮しないといけない」のは考えもの
    - つまりGuitarSpecの変更に対して、開放閉塞ではない

"""
