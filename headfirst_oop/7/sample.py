# p346
# boardのシナリオを満たす簡易実装


class Board:
    def __init__(self, height, width):
        self.height = height
        self.width = width  # x
        self.tiles = self._init_board()  # y

    def _init_board(self):
        return [[Tile() * self.width]] * self.height

    def move_unit(self, x, y):
        pass

    def get_unit(self, x, y):
        pass

    def get_tile(self, x, y):
        return self.tiles[y][x]

    def battle(self, x, y):
        pass

    def remove_unit(self, unit, x: int, y: int) -> None:
        # Tileに委譲
        tile = self.get_tile(x, y)
        tile.remove_unit(unit)

    def add_unit(self, unit, x: int, y: int) -> None:
        tile = self.get_tile(x, y)
        tile.remove_unit(unit)


class Unit:
    def __eq__(self):
        pass
    pass


class Tile:
    def __init__(self) -> None:
        self.units = []

    def add_unit(self, unit: Unit) -> None:
        # Type Hintsのclass定義が、書く順番によって未定義になる。。。
        self.units.append(unit)

    def remove_unit(self, unit: Unit) -> None:
        # removeを使っているので、Unitには__eq__の実装が必須
        self.units.remove(unit)
