# p208
# 1/sample7.pyの変更
# 新たな楽器(マンドリン)の追加によって、楽器という抽象classを作成する。

from dataclasses import dataclass
from enum import Enum


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


class Style(Enum):
    # Mandolin has 2types
    A = "A"
    F = "F"


@dataclass
class InstrumentSpec:
    builder: Builder
    model: str
    type: Type
    backwood: Wood
    topwood: Wood

    def __eq__(self, other):
        # GuiterSpecとMandorinSpecで比較することがあり、AttributeErrorを起こすことの回避
        # つまり自分自身と同じ具象クラスでないなら、Falseが帰る
        if type(self) != type(other):
            return False

        if (other.builder) and (self.builder != other.builder):
            return False

        if (other.model) and (self.model != other.model):
            return False

        if (other.type) and (self.type != other.type):
            return False

        if (other.backwood) and (self.backwood != other.backwood):
            return False

        if (other.topwood) and (self.topwood != other.topwood):
            return False

        return True


@dataclass
class GuitarSpec(InstrumentSpec):
    num_strings: int

    def __eq__(self, other):
        if not super().__eq__(other):
            return False

        if (other.num_strings) and (self.num_strings != other.num_strings):
            return False

        return True

    def __str__(self) -> str:
        # 正直もっと良い書き方があるはず。。。
        # これだと属性の変更に応じてここを書き換える必要がある。
        return f"""builder: {self.builder.value}
model: {self.model}
type: {self.type.value}
backwood: {self.backwood.value}
topwood: {self.topwood.value}
num_strings: {self.num_strings}"""


@dataclass
class MandolinSpec(InstrumentSpec):
    style: Style

    def __eq__(self, other):
        if not super().__eq__(other):
            return False

        if (other.style) and (self.style != other.style):
            return False

        return True

    def __str__(self) -> str:
        # 正直もっと良い書き方があるはず。。。
        # これだと属性の変更に応じてここを書き換える必要がある。
        return f"""builder: {self.builder.value}
model: {self.model}
type: {self.type.value}
backwood: {self.backwood.value}
topwood: {self.topwood.value}
Style: {self.style}"""


class Instrument:
    def __init__(
        self,
        serial_number: str,
        price: float,
        spec: InstrumentSpec
    ) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec

    def __str__(self) -> str:
        formatted_spec = str(self.spec).replace("\n", "\n    ")
        return f"${self.price}\n    {formatted_spec}"


class Guitar(Instrument):
    # ただ、InstrumentSpecの具象クラスが違うだけで、ほぼ同じコード
    def __init__(
        self,
        serial_number: str,
        price: float,
        spec: GuitarSpec
    ) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec


class Mandolin(Instrument):
    def __init__(
        self,
        serial_number: str,
        price: float,
        spec: MandolinSpec
    ) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec


class Inventory:
    def __init__(self) -> None:
        self.inventory = []

    def add_instrument(self, instrument: Instrument) -> None:
        self.inventory.append(instrument)

    def search(self, searched_spec: InstrumentSpec):
        # ここで引数には、GuiterSpecとMandolinSpecを受け入れるため、混ざった比較をすることがあるが、
        # InstrumentSpec自身で判断するため、Client(Inventory)は、__eq__を使えることだけ知っていれば良い
        # これが「抽象classにのみプログラミングする」ということ
        matching_instrument = [
            instrument
            for instrument in self.inventory
            if instrument.spec == searched_spec
        ]
        if matching_instrument:
            print(f"お探しの楽器が、{len(matching_instrument)}件見つかりました。")
            for i in matching_instrument:
                print("---")
                print(i)
            return matching_instrument

        print("お探しの楽器は見つかりませんでした。")
        return None


def init_invetory():
    guitar_fender_a = Guitar(
        serial_number="V95693",
        price=1499.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder,
            12
        )
    )
    guitar_fender_b = Guitar(
        serial_number="V9512",
        price=1599.95,
        spec=GuitarSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder,
            12
        )
    )
    mandolin_test_a = Mandolin(
        serial_number="MandolinTest",
        price=9999.9,
        spec=MandolinSpec(
            Builder.FENDER,
            "Stratocastor",
            Type.ELECTRIC,
            Wood.Alder,
            Wood.Alder,
            Style.A
        )
    )
    inventory = Inventory()
    inventory.add_instrument(guitar_fender_a)
    inventory.add_instrument(guitar_fender_b)
    inventory.add_instrument(mandolin_test_a)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    searched_guitar = GuitarSpec(
        Builder.FENDER,
        "Stratocastor",
        Type.ELECTRIC,
        Wood.Alder,
        Wood.Alder,
        12
    )
    inventory.search(searched_guitar)

    searched_mandorin = MandolinSpec(
        Builder.FENDER,
        "Stratocastor",
        Type.ELECTRIC,
        Wood.Alder,
        Wood.Alder,
        Style.A
    )
    inventory.search(searched_mandorin)
