# p259
# 問題1.Instrumentの具象クラスがいらない
#     - Guiter, Instrumentを独自の操作が存在しない = 具象クラスである必要がない。
# 問題2. InstrumentSpecの具象クラスがいらない
#     - GuiterSpecや、InstrumentSpec等、具象クラスで異なるのは、格納している属性だけである。
#     - 従ってここの属性を持つのではなく、辞書として格納すればよい。

# p208
# 1/sample7.pyの変更
# 新たな楽器(マンドリン)の追加によって、楽器という抽象classを作成する。

from dataclasses import dataclass
from enum import Enum
from os import remove


class InstrumentType(Enum):
    GUITAR = "GUITAR"
    BANJO = "BANJO"
    DOBRO = "DOBRO"
    FIDDLE = "FIDDLE"
    BASS = "BASS"
    MANDOLIN = "MANDOLIN"


class Type(Enum):
    ACOUSTIC = "acoustic"
    ELECTRIC = "electric"


class Builder(Enum):
    FENDER = "Fender"
    MARTIN = "Martin"


class Wood(Enum):
    INDIAN_ROSEWOOD = "itarian_rosewood"
    Alder = "alder"


class Style(Enum):
    # Instrument has 2types
    A = "A"
    F = "F"


class InstrumentSpec:
    def __init__(self, properties: dict) -> None:
        self.properties = properties

    def __eq__(self, other):
        # 比較対象のpropertiesが、全て含まれていたら(部分集合ならば)True
        # これだったら、本来の == と使い方が異なってくる。。。　誤解させかねない
        # Client側で部分集合か判定したほうが良いかも　or __eq__ではなく、通常methodで実装すべき？
        return other.properties.items() <= self.properties.items()

    def __str__(self):
        return str(self.properties)\
            .replace("{", "")\
            .replace("}", "")\
            .replace("\'", "")\
            .replace(",", "\n")


class Instrument:
    def __init__(
        self,
        serial_number: str,
        price: float,
        spec: InstrumentSpec
    ) -> None:
        self.serial_number = serial_number
        self.price = price
        self.spec = spec

    def __str__(self) -> str:
        formatted_spec = str(self.spec).replace("\n", "\n    ")
        return f"${self.price}\n    {formatted_spec}"


class Inventory:
    def __init__(self) -> None:
        self.inventory = []

    def add_instrument(self, instrument: Instrument) -> None:
        self.inventory.append(instrument)

    def search(self, searched_spec: InstrumentSpec):
        matching_instrument = [
            instrument
            for instrument in self.inventory
            if instrument.spec == searched_spec
        ]
        if matching_instrument:
            print(f"お探しの楽器が、{len(matching_instrument)}件見つかりました。")
            for i in matching_instrument:
                print("---")
                print(i)
            return matching_instrument

        print("お探しの楽器は見つかりませんでした。")
        return None


def init_invetory():
    guitar_fender_a = Instrument(
        serial_number="V95693",
        price=1499.95,
        spec=InstrumentSpec({
            "InstrumentType": InstrumentType.GUITAR,
            "Builder": Builder.FENDER,
            "model": "Stratocastor",
            "type": Type.ELECTRIC,
            "backwood": Wood.Alder,
            "topwood": Wood.Alder,
            "num_string": 12
        })
    )
    guitar_fender_b = Instrument(
        serial_number="V9512",
        price=1599.95,
        spec=InstrumentSpec({
            "InstrumentType": InstrumentType.GUITAR,
            "Builder": Builder.FENDER,
            "model": "Stratocastor",
            "type": Type.ELECTRIC,
            "backwood": Wood.Alder,
            "topwood": Wood.Alder,
            "num_string": 12
        })
    )
    mandolin_test_a = Instrument(
        serial_number="InstrumentTest",
        price=9999.9,
        spec=InstrumentSpec({
            "InstrumentType": InstrumentType.MANDOLIN,
            "Builder": Builder.FENDER,
            "model": "Stratocastor",
            "type": Type.ELECTRIC,
            "backwood": Wood.Alder,
            "topwood": Wood.Alder,
            "Style": Style.A
        })
    )
    inventory = Inventory()
    inventory.add_instrument(guitar_fender_a)
    inventory.add_instrument(guitar_fender_b)
    inventory.add_instrument(mandolin_test_a)
    return inventory


if __name__ == "__main__":
    inventory = init_invetory()

    searched_guitar = InstrumentSpec({
        "InstrumentType": InstrumentType.GUITAR,
        "Builder": Builder.FENDER,
        "model": "Stratocastor",
        "type": Type.ELECTRIC,
        "backwood": Wood.Alder,
        "topwood": Wood.Alder,
        "num_string": 12
    })
    inventory.search(searched_guitar)

    searched_mandorin = InstrumentSpec({
        "InstrumentType": InstrumentType.MANDOLIN,
        "Builder": Builder.FENDER,
        "model": "Stratocastor",
        "type": Type.ELECTRIC,
        "backwood": Wood.Alder,
        "topwood": Wood.Alder,
        "Style": Style.A
    })
    inventory.search(searched_mandorin)
