# sample.py
- InstrumentSpecの、__eq__に工夫を加えることで、
- Client(Inventory class)では、__eq__を使えるということだけ知っていれば良くなった
- もし具象クラス(**Spec)を気にした作りしていたら、毎回このmethodを書き換えないといけない。
- 変更がありそうなclassは、必ず抽象クラスに対してプログラミングするようにする。
```
class InstrumentSpec:
    builder: Builder

    def __eq__(self, other):
        # GuiterSpecとMandorinSpecで比較することがあり、AttributeErrorを起こすことの回避
        # つまり自分自身と同じ具象クラスでないなら、Falseが帰る
        if type(self) != type(other):
            return False

class Inventory:          
    def search(self, searched_spec: InstrumentSpec):
        # ここで引数には、GuiterSpecとMandolinSpecを受け入れるため、混ざった比較をすることがあるが、
        # InstrumentSpec自身で判断するため、Client(Inventory)は、__eq__を使えることだけ知っていれば良い
        # これが「抽象classにのみプログラミングする」ということ
        matching_instrument = [
            instrument
            for instrument in self.inventory
            if instrument.spec == searched_spec
        ]
```

# p228
## 抽象クラスにプログラミングする。
- 具象クラスにプログラミングすると、具象クラスの変化に振り回される。
- 抽象クラスという決まった設計図にのみにプログラミングすれば、具象クラスに依存しなくなる。

## カプセル化を行う
- 「変化しやすく、複数のパターンが存在する」場合は、class内にmethodとして実装するのではなく、
- 抽象クラスを別に作成し、has-aの関係で委譲するべき！！
- これによって抽象クラスにプログラミングすることができる。

## 単一責任の法則
- 変更される可能性が別で2つ以上存在するとき、そのクラスの役割が多すぎるため分割すべき