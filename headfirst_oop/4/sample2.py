# p155
# 鳴き声をstrではなく、専用class Barkを作成する。
# これにより今後鳴き声の比較にwav fileを使う等高度化してもBarkのみの変更でOKになる。
# つまり、DogDoor等の変更が必要なくなる。

import time


class Bark:
    def __init__(self, sound) -> None:
        self.sound = sound

    def __eq__(self, other: object) -> bool:
        # type hintをBarkにするとNot definedになる。。。
        return self.sound == other.sound

    def __str__(self) -> str:
        return self.sound


class DogDoor:
    def __init__(self) -> None:
        self.is_open = False
        self.closing_time = 2
        self.allowed_bark = ()

    def open(self) -> None:
        print("Door is open.")
        self.is_open = True
        time.sleep(self.closing_time)
        print("---The door will close automatically---")
        self.close()

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False

    def add_allowed_bark(self, bark, *barks):
        barks = (bark,) + barks
        print(f"add allowed bark: {', '.join(str(b) for b in barks)}")
        self.allowed_bark += barks


class BarkRecognizer:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def recognize(self, bark_sound: Bark = Bark("woof")):
        if bark_sound in self.door.allowed_bark:
            print(f"bark: {bark_sound}")
            print("---Detect bark sounds---")
            self.door.open()


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def set_closing_time(self, closing_time: int):
        print(f"Set DogDoor's closing time at {closing_time} seconds")
        self.door.closing_time == closing_time

    def press_button(self) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()


if __name__ == "__main__":
    dogdoor = DogDoor()
    dogdoor.add_allowed_bark(Bark("woof"))
    br = BarkRecognizer(dogdoor)
    br.recognize()
