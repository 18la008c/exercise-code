# p155
# 自分の考え
# - sound: strに鳴き声を格納
# - DogDoorにsoundをtuple形式で保存

import time


class DogDoor:
    def __init__(self) -> None:
        self.is_open = False
        self.closing_time = 2
        self.__registerd_sound = ()

    def _can_open(self, bark_sound):
        return bark_sound in self.sound

    def open(self) -> None:
        print("Door is open.")
        self.is_open = True
        time.sleep(self.closing_time)
        print("---The door will close automatically---")
        self.close()

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False

    @property
    def sound(self):
        return self.__registerd_sound

    @sound.setter
    def sound(self, sound, *sounds):
        sounds = (sound,) + sounds
        print(f"Regist sound: {', '.join(sounds)}")
        self.__registerd_sound = sounds


class BarkRecognizer:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def recognize(self, bark_sound: str = "woof"):
        print(f"bark: {bark_sound}")
        if self.door._can_open(bark_sound):
            print("---Detect bark sounds---")
            self.door.open()


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def set_closing_time(self, closing_time: int):
        print(f"Set DogDoor's closing time at {closing_time} seconds")
        self.door.closing_time == closing_time

    def press_button(self) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()


if __name__ == "__main__":
    dogdoor = DogDoor()
    dogdoor.sound = "woof"
    br = BarkRecognizer(dogdoor)
    br.recognize()
