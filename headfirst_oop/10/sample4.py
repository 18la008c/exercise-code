# p540

from dataclasses import dataclass


class StationExistError(Exception):
    pass


class BreakLoop(Exception):
    pass


@dataclass(frozen=True)  # dictのkeyにするためfrozen
class Station:
    name: str


@dataclass(frozen=True)
class Connection:
    station1: Station
    station2: Station
    line_name: str


class SubwayLoader:
    @ classmethod
    def load_from_file(cls, path):
        subway = Subway()
        with open(path, "r") as f:
            brankline_char = "\n\n"
            split_by_blankline = f.read().split(brankline_char)

        stations = split_by_blankline[0].split("\n")
        lines = [
            line.split("\n")
            for line in split_by_blankline[1:]
        ]
        # load all stations
        for station in stations:
            subway.add_station(station)

        # load all lines
        for line in lines:
            line_name = line.pop(0)
            for station1, station2 in zip(line, line[1:]):
                subway.add_connection(station1, station2, line_name)
        return subway


class Subway:
    def __init__(self) -> None:
        self.stations = []
        self.connections = []
        self.network = {}

    @ staticmethod
    def create(path="headfirst_oop/10/objectville_subway.txt"):
        return SubwayLoader.load_from_file(path)

    def has_station(self, name: str) -> bool:
        s = Station(name)
        return s in self.stations

    def has_connection(self, station1: str, station2: str, line_name: str) -> None:
        conn = Connection(Station(station1), Station(station2), line_name)
        return conn in self.connections

    def add_station(self, name: str):
        # has_stationで、Stationをインスタンス化して、appendでStationをまたインスタンス化してる。。。
        # ClientにStationクラスを見せないためとは言え、smartではなくない？
        if self.has_station(name):
            return None

        self.stations.append(Station(name))

    def add_connection(self, station1: str, station2: str, line_name: str) -> None:
        if self.has_station(station1) and self.has_station(station2):
            # 順方向と逆方向で2つclassを作るぐらいなら、Connectionでのstationの格納をsetとかの順序を持たないやつにすれば1つだけで済むのでは？
            # → でもこうしておけば、順方向しか持たない(始点、終点)場合にも対応できて便利かも
            station1 = Station(station1)
            station2 = Station(station2)
            conn1 = Connection(
                station1,
                station2,
                line_name
            )
            conn2 = Connection(
                station2,
                station1,
                line_name
            )
            # has_connectionの引数を、Connectionではなくstringsにしてるのでこんな感じに。。。
            if conn1 not in self.connections:
                self.connections.append(conn1)
                self._add_network(station1, station2)
            if conn2 not in self.connections:
                self.connections.append(conn2)
                self._add_network(station2, station1)
            return None

        raise StationExistError()

    def _add_network(self, station1: Station, station2: Station):
        # 辞書のキーに自作objectを使用した。
        # https://qiita.com/yoichi22/items/ebf6ab3c6de26ddcc09a
        if station1 in self.network:
            self.network[station1].append(station2)
        else:
            self.network[station1] = [station2]

    def get_directions(self, start_station: str, end_station: str):
        if (not self.has_station(start_station)) or (not self.has_station(end_station)):
            raise StationExistError()

        s = Station(start_station)
        e = Station(end_station)
        route = []
        reachable_station = []
        previous_stations = {}

        # 隣同士の駅の場合を探索
        neighbors = self.network[s]
        for station in neighbors:
            if station == e:
                route.append(self._get_connection(s, e))
                return route
            else:
                reachable_station.append(station)
                previous_stations[station] = s

        next_stations = neighbors
        try:
            for n in enumerate(self.stations):
                tmp_next_stations = []
                for station in next_stations:
                    reachable_station.append(station)
                    for neighbor in self.network[station]:
                        if neighbor == e:
                            reachable_station.append(neighbor)
                            previous_stations[neighbor] = station
                            raise BreakLoop
                        elif neighbor not in reachable_station:
                            reachable_station.append(neighbor)
                            tmp_next_stations.append(neighbor)
                            previous_stations[neighbor] = station
                    next_stations = tmp_next_stations

        except BreakLoop:
            pass

        key_station = e
        keep_looping = True
        while keep_looping:
            station = previous_stations[key_station]
            route.insert(0, self._get_connection(station, key_station))
            if station == s:
                keep_looping = False
            key_station = station

        return route

    def _get_connection(self, station1: Station, station2: Station):
        for conn in self.connections:
            if (conn.station1 == station1) and (conn.station2 == station2):
                return conn
        return None


def print_direction(station1: str, station2: str):
    subway = Subway.create("objectville_subway.txt")
    connections = subway.get_directions(station1, station2)
    directions = {}
    for conn in connections:
        if conn.line_name not in directions:
            directions[conn.line_name] = [conn.station1, conn.station2]
        else:
            directions[conn.line_name] = [
                directions[conn.line_name][0],
                conn.station2
            ]
    print(f"***Get Direction: {station1} to {station2}***")
    print("-----")
    for line_name, direction in directions.items():
        print(
            f"    Ride {line_name}: {direction[0].name} to {direction[1].name}")


if __name__ == "__main__":
    print_direction("GoF Gardens", "Head First Lounge")
    print_direction("XHTML Expressway", "CSS Center")
