# p487

- フィーチャー一覧
- ユースケース図
- 問題分割
- 要件
- ドメイン分析
- 初期設計
- 実装
- 納品

# 493
## フィーチャー一覧
- 地下鉄網全て+駅名が格納されている
- 各線の方向を気にしなくて良い
    - いまいち分からない。全て環状線ってこと？
    - 実装しなくていいこともフィーチャーに扱って良い？
- 出発駅と到着駅を指定すると、経路を出力してくれる。
    - この経路には、どの線を使い、どの駅を通り、どこで乗り換えるか表示する
    - 複数ある場合はどうする？
- 拡張可能である。
    - ふわっとしすぎ。。。何が拡張されうるの？
    - 駅はありうるだろうし。。。

# p494
## 答えを見た結果
- フィーチャーはもっとプログラム側に寄せたリスト
- 上記の回答だと、ただ顧客の要望をリストかしただけ。。。
## new フィーチャー一覧
- 地下鉄網の格納
    - 全ての駅名の格納
    - 駅毎に存在する路線の格納
- 2駅間の経路を計算する
    - 複数パターン存在する場合はどうする。。。
- 経路出力するシステム

# p496
## ユースケース図
- [アクター]駅員
    - [ユースケース]2つの駅間の経路を表示する
- [アクター]管理者
    - [ユースケース]駅の登録

# p501
## 自作
- station
- line
- print
- calc
## 答え
- subway
    - station
    - line
    - calc
- loader
    - 駅の情報を読み込む
- print
- 確かに、大分類だから、stationとlineとcalcを分ける意味もないか

# p507
1. txtファイルの読み込み
2. 路線毎に駅名を分ける(空行で分ける)
3. 駅をclassに入れる
- 回答を読んで
    - まだ既存のシステムを作っていないのだから、あたらしく組み込んだ駅の検証なんてわからなくない？
        - どうやって保持しているのかの決まりも全くないし。。。
    - 2点間の接続を作成するも、どんな仕組みにするか一切決まってないのに。。。

# p510
- 名詞(class候補)
    - 駅
    - 路線
    - ファイル
    - 接続
    - 地下鉄
- 動詞(method候補)
    - 読み込む
    - 検証する
    - 接続を作成
    - 追加する

# p511
## 自分で考えたやつ
- Subway(全体管理class)
    - attribute
        - stations: list or dict
    - method
        - read_txt
        - varidate_station
        - add_station
- Station
    - attribute
        - name :str
        - lines : ? 2路線存在する場合はstationで管理すべきか or Subwayで管理すべきか？(でもClientである、Subwayは簡素にした方がいいはずだからこっちで2路線管理すべきな気はする)
        - connections :?
    - method
        - add_connection or create_connection
- Connection: # 確かにconnection専用クラスに、2路線存在するパターンを管理すれば良い？
    - attribute
        - station1: Station 
        - station2: Station
    - method:
        ???
## 答えを見て
- read_txtは確かに、Subway自身を作成するのに必要だから__init__にして良いかも
```
# こっちの方がsmartだと思うけど
class Subway:
    def __init__(self, csv_path):
        pass
    
    @abstractmethod
    def create(cls, csv_path):
        return Subway(csv_path)

def create_subway(csv_path):
    # 作成コード
    return Subway.create(csv_path)
```
- ConnectionをStationに持たせるか、Subwayに持たせるかはこの時点では判断できない。。。
    - 多分、DRY等の法則からある程度推測できるようになるんだろうなー
    - Stationに持たせると、xxxが重複しそうだから、SubwayにConnectionを持たせよう的な
- ConnectionをClass管理させる必要性も難しい。。。
    - Stationに、attrとして持たせれば良くない？ってなる。。。
    ```
    {
        "station1": xxx,
        "station2": yyy
    }
    ```

# p522
- Stationを引数に取らない方がいいらしい
    - Stationの作成方法を、外部公開しなくて良いから(Subway内部に格納できる。。。)
    - これによってStationに変更が加わったとき(コンストラクタが増えた等)に変更しなくて良くなるらしい
- でも！！！
    - コンストラクタが増えた場合には、has_stationにも結局引数を加えないといけなくなる可能性がある。
    - すると、has_stationを使っているコードにも修正が必要になってきてしまう。
    - 更にclassの作成コードが散らばって、Stationを変更するたびに変更が必要になる。。。
    - Subwayクラスでは、何回もclassの作成が重複してしまう
        - コンストラクタ←→クラスでの行き来が多すぎる。。。
        - classで統一して比較すれば、何回もclass作らなくて済む
- だったら
    - 今回の問題は、Stationを外部公開したくない + Stationだけで比較したい
    - **BuilderやFactory methodのようにclassの作成に責任を持つ唯一のクラスを作って、あとはclassだけで会話すれば良い。**

# p523
## まとめ
- p522対するまとめが載っている
- Userには、相互作用するクラスのみ公開すべし！
- User → Client(Subway) ←→ Station, Connection
    - User: Stationを直接扱うことはしない。扱うのは駅名のstrだけ
    - Subway: StationとConnectionを使用して、さまざまな処理を行う
    - Connection, Station: Subwayに使用される
- つまり、今回はSubwayとStation, Connectionのみが相互作用となる。
- 結局UserはStationについて知る必要がないので、公開しないべき！
    - **知らなくて良い = Stationが変更されても、Userは気にしなくて良い**
- それでも!!!!
    - User側作成するclassは、Client Classの1つとすべき!!!
    - Client Classが使うけど、User自体は使わないclassはUserが作るのではなくClient Classが作るべき!!
        - そもそもUserが扱うclassを増やすな!
    - それでも、たくさんのClassをUser側が作成する場合は、factory methodの検討すべき！
