# p524, 527
# https://github.com/psacharuk/headFirstOOADCsharp/blob/master/ObjectvilleSubwayMap/ObjectvilleSubway.txt
# Userには、Station Connectionは隠蔽して、Userが使用するものはstringsのみにする。
# ただこれによって弊害が生じる。
#    - class内部では、Station, Connectionクラスで使用したい。
#    - class外部(Userから)では、stringで使用したい。
# 結局、内部ではstrで受け取る→Station, Connectionを作成という手間が入る。。。

from dataclasses import dataclass


class Station:
    def __init__(self, name):
        self.name = name

    def __eq__(self, __o: object) -> bool:
        return self.name == __o.name


@dataclass
class Connection:
    station1: Station
    station2: Station
    line_name: str

    def __eq__(self, __o: object) -> bool:
        return all([
            self.station1 == __o.station1,
            self.station2 == __o.station2,
            self.line_name == __o.line_name
        ])


class SubwayLoader:
    @ classmethod
    def load_from_file(cls, path):
        subway = Subway()
        with open(path, "r") as f:
            brankline_char = "\n\n"
            split_by_blankline = f.read().split(brankline_char)

        stations = split_by_blankline[0].split("\n")
        lines = [
            line.split("\n")
            for line in split_by_blankline[1:]
        ]
        # load all stations
        for station in stations:
            subway.add_station(station)

        # load all lines
        for line in lines:
            line_name = line.pop(0)
            for station1, station2 in zip(line, line[1:]):
                subway.add_connection(station1, station2, line_name)
        return subway


class Subway:
    def __init__(self) -> None:
        self.stations = []
        self.connections = []

    @ staticmethod
    def create(path="headfirst_oop/10/objectville_subway.txt"):
        return SubwayLoader.load_from_file(path)

    def has_station(self, name: str) -> bool:
        s = Station(name)
        return s in self.stations

    def has_connection(self, station1: str, station2: str, line_name: str) -> None:
        conn = Connection(Station(station1), Station(station2), line_name)
        return conn in self.connections

    def add_station(self, name: str):
        # has_stationで、Stationをインスタンス化して、appendでStationをまたインスタンス化してる。。。
        # ClientにStationクラスを見せないためとは言え、smartではなくない？
        if self.has_station(name):
            return None

        self.stations.append(Station(name))

    def add_connection(self, station1: str, station2: str, line_name: str) -> None:
        if self.has_station(station1) and self.has_station(station2):
            # 順方向と逆方向で2つclassを作るぐらいなら、Connectionでのstationの格納をsetとかの順序を持たないやつにすれば1つだけで済むのでは？
            # → でもこうしておけば、順方向しか持たない(始点、終点)場合にも対応できて便利かも
            conn1 = Connection(
                Station(station1),
                Station(station2),
                line_name
            )
            conn2 = Connection(
                Station(station2),
                Station(station1),
                line_name
            )
            # has_connectionの引数を、Connectionではなくstringsにしてるのでこんな感じに。。。
            if conn1 not in self.connections:
                self.connections.append(conn1)
            if conn2 not in self.connections:
                self.connections.append(conn2)
            return None

        raise KeyError("Cannot Find Station")


if __name__ == "__main__":
    subway = Subway.create("objectville_subway.txt")
    print(
        subway.connections[0].station1.name,
        "→",
        subway.connections[0].station2.name
    )
    print(
        subway.connections[1].station1.name,
        "→",
        subway.connections[1].station2.name
    )
