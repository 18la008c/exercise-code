import pytest
import sample1 as sample


def test_add_station():
    subway = sample.Subway()
    subway.add_station("stationA")
    assert len(subway.stations) == 1
    assert subway.stations[0].name == "stationA"


def test_has_station():
    subway = sample.Subway()
    subway.add_station("stationA")
    assert subway.has_station("stationA") is True
    assert subway.has_station("stationB") is False


def test_add_connection():
    subway = sample.Subway()
    subway.add_station("stationA")
    subway.add_station("stationB")
    subway.add_connection("stationA", "stationB", "lineABC")
    assert len(subway.connections) == 2
    assert subway.connections[0].line_name == "lineABC"


def test_failed_add_connection():
    subway = sample.Subway()
    subway.add_station("stationA")
    subway.add_station("stationB")
    with pytest.raises(KeyError):
        subway.add_connection("stationA", "stationC", "lineABC")
