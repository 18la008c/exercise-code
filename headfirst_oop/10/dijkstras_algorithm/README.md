# ダイクストラ法
> グラフ上のある地点を始点とする最短経路を求める(単一始点最短経路問題を解く)ためのアルゴリズムです。Dijkstra氏によって考案されたことが名前の由来です。
> https://products.sint.co.jp/topsic/blog/dijkstras-algorithm

- ヨビノリがとにかくわかりやすい
    - https://www.youtube.com/watch?v=X1AsMlJdiok
- 上記内容をpythonで実装した例
    - https://zenn.dev/tomson784/articles/dijkstra_python
- sample4.pyの`get_directions`でもこのダイクストラ法を使用している。

## get_directions
- ここでは、駅間距離は全て等しいとするため、最短経路 = 最短駅数となる。
- 従ってgoalが見つかった時点breakすれば、最短となる。

### 手順
Step1 最短ゴールを探す & ゴールまでのとりうる経路を保管する。
　　　
```
startから周辺の駅を取得 → neighbors
for neighbor in neighbors:
    if neighbor == goal:
        goalなので終了
        routeだけ、previous_stationsに記録しておく。

    # 行ったことのない駅だけ取得する。(行ったことある場合は最短経路ではないので捨てる)
    elif neighbor not in reachable_stations: 
        routeを、previous_stationsに記録する。
        reachable_staionsに記録する。(行ったことあるリストに追加)
        tmp_next_stationsに記録する。(次の探索対象)
```
Step2 ゴールまでの経路を取得する
```
previous_stationsにこれまで辿った、駅がdict形式で保管されているので、goalから1つずつ辿っていく。
```