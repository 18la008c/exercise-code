from attr import has
import pytest
import sample2 as sample


def test_add_station():
    subway = sample.Subway()
    subway.add_station("stationA")
    assert len(subway.stations) == 1
    assert subway.stations[0].name == "stationA"


def test_has_station():
    # has_stationをtestにするのに、add_stationが必要。
    # add_stationをtestにするのに、has_stationが必要。
    # この場合は片方のtest要らない？
    subway = sample.Subway()
    subway.add_station("stationA")
    assert subway.has_station("stationA") is True
    assert subway.has_station("stationB") is False


def test_add_connection():
    subway = sample.Subway()
    subway.add_station("stationA")
    subway.add_station("stationB")
    subway.add_connection("stationA", "stationB", "lineABC")
    assert len(subway.connections) == 2
    assert subway.connections[0].line_name == "lineABC"


def test_has_connection():
    subway = sample.Subway()
    subway.add_station("stationA")
    subway.add_station("stationB")
    subway.add_connection("stationA", "stationB", "lineABC")
    assert subway.has_connection("stationA", "stationB", "lineABC")


def test_failed_add_connection():
    subway = sample.Subway()
    subway.add_station("stationA")
    subway.add_station("stationB")
    with pytest.raises(KeyError):
        subway.add_connection("stationA", "stationC", "lineABC")


def test_create_subway():
    path = "objectville_subway.txt"
    subway = sample.Subway.create(path)
    assert subway.has_station("UML Walk") is True
    assert subway.has_connection(
        "UML Walk", "Objectville PizzaStore", "Booch Line") is True
