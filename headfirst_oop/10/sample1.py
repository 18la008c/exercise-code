# p516

from dataclasses import dataclass


class Station:
    def __init__(self, name):
        self.name = name

    def __eq__(self, __o: object) -> bool:
        return self.name == __o.name


@dataclass
class Connection:
    station1: Station
    station2: Station
    line_name: str


class Subway:
    def __init__(self) -> None:
        self.stations = []
        self.connections = []

    def add_station(self, name: str):
        if not self.has_station(name):
            self.stations.append(Station(name))

    def has_station(self, name: str) -> bool:
        # 普通に考えるなら、nameを引数じゃなくて、Stationを引数に取った方が良い！!
        s = Station(name)
        return s in self.stations

    def add_connection(self, station1: str, station2: str, line_name: str) -> None:
        if self.has_station(station1) and self.has_station(station2):
            # 順方向と逆方向で2つclassを作るぐらいなら、Connectionでのstationの格納をsetとかの順序を持たないやつにすれば1つだけで済むのでは？
            # → でもこうしておけば、順方向しか持たない(始点、終点)場合にも対応できて便利かも
            conn1 = Connection(
                Station(station1),
                Station(station2),
                line_name
            )
            conn2 = Connection(
                Station(station2),
                Station(station1),
                line_name
            )
            self.connections.append(conn1)
            self.connections.append(conn2)
            return None

        raise KeyError("Cannot Find Station")
