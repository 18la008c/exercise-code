import sample3


class UnitGroup:
    def __init__(self):
        self.__units = {}

    @property
    def units(self) -> dict:
        return self.__units

    def add_unit(self, unit: sample3.Unit) -> None:
        self.__units[unit.id] = unit

    def del_unit(self, id: int) -> None:
        del self.__units[id]

    def get_unit(self, id: int) -> sample3.Unit:
        return self.__units[id]
