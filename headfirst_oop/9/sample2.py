import pytest
import sample


def test_type():
    unit = sample.Unit("infantry", 1, "test")
    assert unit.type == "infantry"


def test_property():
    unit = sample.Unit("infantry", 1, "test")
    unit.set_property("hitpoints", 25)
    assert unit.get_property("hitpoints") == 25


def test_change_property():
    # testコードで被りが出るのは仕方ない？？ pytest.fixtureにも限界はあるよね。全部が全部共通するわけじゃないし
    unit = sample.Unit("infantry", 1, "test")
    unit.set_property("hitpoints", 25)
    unit.set_property("hitpoints", 15)
    assert unit.get_property("hitpoints") == 15


def test_get_none_property():
    unit = sample.Unit("infantry", 1, "test")
    assert unit.get_property("strength") is None

    unit.set_property("hitpoints", 25)
    assert unit.get_property("strength") is None


def test_id():
    unit = sample.Unit("infantry", 1, "test")
    assert unit.id == 1


def test_failed_set_id():
    unit = sample.Unit("infantry", 1, "test")
    with pytest.raises(AttributeError):
        unit.id = 100
