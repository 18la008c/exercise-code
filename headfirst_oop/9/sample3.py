# p469
# 値がないときは例外を出すために、getter, setterを設定して値のcheckを行うようにした。

class Unit:
    def __init__(
        self,
        type: str,
        id: int,
        name: str,
    ):
        self.type = type
        self.__id = id
        self.name = name
        self.__weapons = None
        self.__properties = None

    @property
    def id(self):
        return self.__id

    @property
    def weapons(self):
        # property作ったあとは、class内ではprivate変数を使うべきなのか？
        # それとも@property経由で呼び出したweaponsを使うべきなのか。。。
        if self.__weapons is None:
            raise ValueError("Not Exist Weapon")
        return self.__weapons

    def add_weapon(self, weapon):
        if self.__weapons is None:
            self.__weapons = []
        self.__weapons.append(weapon)

    def get_property(self, property_name: str):
        # pythonでは、getterとsetterはdecoratorで行うべきだが、値がdictの中身を見るなどの場合は、こうするしかないよね？？？
        if self.__properties is None:
            raise ValueError("Not Exist property")
        return self.__properties[property_name]

    def set_property(self, property_name: str, value):
        if self.__properties is None:
            self.__properties = {}
        self.__properties[property_name] = value
