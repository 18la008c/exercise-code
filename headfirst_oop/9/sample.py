class Unit:
    def __init__(
        self,
        type: str,
        id: int,
        name: str,
    ):
        self.type = type
        self.__id = id
        self.name = name
        self.weapons = None
        self.properties = None

    @property
    def id(self):
        return self.__id

    def add_weapon(self, weapon):
        if self.weapons is None:
            # コンストラクタで作成せず、必要になったときに作成を遅延することでメモリ使用量を減らす。
            self.weapons = []
        self.weapons.append(weapon)

    def get_property(self, property_name: str):
        # 属性じゃなくて、属性の中の辞書の要素の1つにアクセスするので、@propertyはつかえなそう
        if self.properties is None:
            return None
        return self.properties.get(property_name)

    def set_property(self, property_name: str, value):
        if self.properties is None:
            self.properties = {}
        self.properties[property_name] = value
