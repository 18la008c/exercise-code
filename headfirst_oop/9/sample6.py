import sample5
import sample3  # module化した時のimportの関係性ってどうするべきなの？
import pytest


def test_zero_units():
    ug = sample5.UnitGroup()
    assert ug.units == {}


def test_add_units():
    # addを確認するには、getも確認しないとNGで1つの関数で2つのtest行うしかないけどいいの？
    ug = sample5.UnitGroup()
    unit = sample3.Unit("infantry", 1, "test")
    ug.add_unit(unit)
    assert ug.get_unit(1).id == 1


def test_del_units():
    ug = sample5.UnitGroup()
    unit = sample3.Unit("infantry", 1, "test")
    ug.add_unit(unit)
    # ここまで全く同じだけどいいの？DRY満たしてない。。。
    # だったら、add→get→delってまとめてcheckしてもいいのでは？
    # or これより前にtest実行しているコードは信頼して使っていい、認識？
    # でもそれだと、testに順番依存が出てしまう。。。
    ug.del_unit(1)
    assert ug.units == {}
