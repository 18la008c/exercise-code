# p132
# 「犬が鳴いたら、自動でドアを開ける」という変更に対して、委譲を使い既存コードに変更を加えず実装できた。

import time


class DogDoor:
    def __init__(self) -> None:
        self.is_open = False

    def open(self) -> None:
        print("Door is open.")
        self.is_open = True

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False


class BarkRecognizer:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def recognize(self, bark_sounds: str = "woof"):
        print(f"bark:{bark_sounds}")
        print("---Detect bark sounds---")
        self.door.open()


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def press_button(self, timer=3) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()
            time.sleep(timer)
            print("The door will close automatically.")
            self.door.close()


if __name__ == "__main__":
    dogdoor = DogDoor()
    br = BarkRecognizer(dogdoor)
    br.recognize()
