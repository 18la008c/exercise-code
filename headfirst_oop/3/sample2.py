# p137
# Remoteで実装済みの、doorの自動closeの実装をするが、
# 「コードの重複」を避けて実装する。
# p138
# >ドアを閉めるのは、本当はドアがすべきことだよ。リモコンやBarkRecognizerではないよ。
# >DogDoorに自分で閉めさせたら、どうだろうか。
# →それはそう！！！

import time


class DogDoor:
    def __init__(self) -> None:
        self.is_open = False

    def open(self, timer: int = 2) -> None:
        print("Door is open.")
        self.is_open = True
        time.sleep(timer)
        print("---The door will close automatically---")
        self.close()

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False


class BarkRecognizer:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def recognize(self, bark_sounds: str = "woof"):
        print(f"bark: {bark_sounds}")
        print("---Detect bark sounds---")
        self.door.open()


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def press_button(self) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()


if __name__ == "__main__":
    dogdoor = DogDoor()
    br = BarkRecognizer(dogdoor)
    br.recognize()
