# p140

import time


class DogDoor:
    def __init__(self) -> None:
        self.is_open = False
        self.closing_time = 2

    def open(self) -> None:
        print("Door is open.")
        self.is_open = True
        time.sleep(self.closing_time)
        print("---The door will close automatically---")
        self.close()

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False


class BarkRecognizer:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def recognize(self, bark_sounds: str = "woof"):
        print(f"bark: {bark_sounds}")
        print("---Detect bark sounds---")
        self.door.open()


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def set_closing_time(self, closing_time: int):
        print(f"Set DogDoor's closing time at {closing_time} seconds")
        self.door.closing_time == closing_time

    def press_button(self) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()


if __name__ == "__main__":
    dogdoor = DogDoor()
    r = Remote(dogdoor).set_closing_time(5)
    br = BarkRecognizer(dogdoor)
    br.recognize()
