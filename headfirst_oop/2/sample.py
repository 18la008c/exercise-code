# p56

class DogDoor:
    def __init__(self) -> None:
        self.is_open = False

    def open(self) -> None:
        print("Door is open.")
        self.is_open = True

    def close(self) -> None:
        print("Door is close.")
        self.is_open = False


class Remote:
    def __init__(self, dogdoor: DogDoor) -> None:
        self.door = dogdoor

    def press_button(self) -> None:
        print("---push button---")
        if self.door.is_open:
            self.door.close()
        else:
            self.door.open()


if __name__ == "__main__":
    dogdoor = DogDoor()
    remote = Remote(dogdoor)
    remote.press_button()
    remote.press_button()
