import click

help_contents = {
    "main": "main function",
    "create": "create fucntion",
    "read": "read function",
    "update": "update fuction",
    "delete": "delete fuction"
}


def validate_value(ctx, param, value):
    if value == 1:
        raise click.BadParameter('pls value > 0')
    print(type(ctx))
    print(type(param))
    print(value)
    return value


@click.group(help=help_contents["main"], invoke_without_command=True)
# @click.pass_context
def cli():
    print("hello")


@cli.command(help=help_contents["create"])
@click.option("--name", default="test_file", help="filename")
def create(name):
    print(f"create {name}")


@cli.command(help=help_contents["read"])
def read():
    print("read")


@cli.command(help=help_contents["update"])
@click.option("--value", type=int, help="update value")
def update(value):
    # 引数check
    if value == 0:
        raise click.BadParameter('pls value > 0')
    print(f"update {value} value")


@cli.command(help=help_contents["delete"])
# 引数checkは、callback関数で実装ができる
@click.option("--value", type=int, callback=validate_value, help="delete value")
def delete(value):
    print(f"delete {value} value")


if __name__ == "__main__":
    cli()
