# Factory
- sample.py
    - simple factoryを使った。
- sample2.py
    - factory methodを使った。
- sample3.py
    - abstract factroyを使った。

## いつ使うのか？
- **「具象クラスの作成の隠蔽」を行い、「抽象クラスにのみプログラミングの実現」させる時**
- bad
    - 具象クラスの作成を行ってしまう = 以降のプログラミングでは具象クラスを意識する必要がある。。。
    - 例えば、CheesePizzaから、ChicagoCheesePizza, NYCheesePizzaが必要になった時
         - 変更に対して閉じていない = 直接コードを書きかける必要がある。
         - 拡張に対しても開いていない = 直接コードを書き換える必要がある。
    ```
    if pizza_type == "cheese": # if文の変更が必要になる。。。
        return CheesePizza()
    elif pizza_type == "meat":
        return MeatPizza()
    else:
        raise KeyError(f"{pizza_type} is not in menu")
    ```
- good
    - factoryを使用して、具象クラスの作成はfactory内部に隠す。
    - mainコードでは、抽象クラスしか出てこない。→ open-closeになる。
    - ChicagoCheesePizza, NYCheesePizzaが必要になった時factoryの変更で対応できるようになる。
    - 上記パターンだと、具象factoryを追加して、PizzaStoreにhas-aさせるだけで良い。
    ```
    class PizzaStore:
        # main class
        def __init__(self, pizza_factory):
            self.factory = pizza_factory

        def order_pizza(self, pizza_type):
            # 抽象クラス(Pizza)のみ気にすれば良い。
            pizza = self.create(pizza_type)
            pizza.prepare()
            pizza.bake()
            pizza.cut()
            pizza.box()
            return pizza

        def create(self, pizza_type):
            return self.factory.create_pizza(pizza_type)

    class PizzaFactory(ABC):
        # 具象クラスの作成は、全てFactoryがやってくれる。
        @abstractmethod
        def create_pizza(self, pizza_type):
            pass

    class NYPizzaFactory(PizzaFactory):
        def create_pizza(self, pizza_type):
            if pizza_type == "cheese":
                return NYCheesePizza()
            elif pizza_type == "meat":
                return NYMeatPizza()
            else:
                raise KeyError(f"{pizza_type} is not in menu")
    ```

# Factory Methodと、 Abstract Factoryの使い分け
- 結論は、「Abstract Factory」だけ使って入れば良い気がする。
- 差としては、以下だが
    - factory method: 作成するProductは1つ
    - abstract factory: 作成するProductが複数で、最後に合体が必要である。(Pizza+Sidemenuのように)
- 作成するProductが1つでも、abstract factroyは当然使える。(sample2, sample3.pyの比較参照)
    - もちろんコード数としては、factory methodの方が少ない & 簡単になるが。。。（そんなに大きな差ではないし)