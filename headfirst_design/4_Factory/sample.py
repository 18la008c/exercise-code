# simple factory patttern
# これは具象クラス(ConcretePizza)の作成を、SimpleFactory(厨房)に任せるイメージ
# 店側は、Pizzaであることしか知らなくてOK! ←変更・追加に対して閉じている。
# 厨房だけが、各種ConcretePizzaの作り方を知っていれば良い。 ←変更が多いがこのクラスだけ改変すれば良い！！(開いている)

from abc import ABC, abstractproperty


class PizzaStore:
    def __init__(self, simple_factory):
        self.factory = simple_factory

    # 以下のように、setterを用意 & simplefactoryの具象クラスを用意して動的にfactory変更すれば
    # factory methodよりも良いのでは？(has-a関係のため)
    # @property
    # def factory(self):
    #     return self.__factory

    # @factory.setter
    # def factory(self, simple_factory):
    #     self.__factory = simple_factory

    def order_pizza(self, pizza_type):
        # Storeで扱うのは、抽象クラス(Pizza)のみ！
        # 具象クラスの作成は、factoryにお任せ！
        print(f"ベースとなる{pizza_type}の作成は、SimpleFactoryにお任せしますー")
        pizza = self.factory.create(pizza_type)
        # pizza = SimpleFactoy.create(pizza_type) ← もしstaticmethodの場合
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza


class SimpleFactory:
    # 今回はわざわざ、has-aにするために、simplefactoryをインスタンスかする必要がある。
    # とはいえ、このcreate methodだけなら、classmethod, staticmethodでも十分である。
    def create(self, pizza_type):
        if pizza_type == "cheese":
            return CheesePizza()
        elif pizza_type == "meat":
            return MeatPizza()
        else:
            raise KeyError(f"{pizza_type} is not in menu")


class Pizza(ABC):
    # 単純に名前をoverride強制したいだけ。。。
    # わざわざ@property使う意味ある？
    @abstractproperty
    def name(self):
        pass

    def prepare(self):
        print(f"{self.name} pizzaを準備中。。。")

    def bake(self):
        print(f"{self.name} pizzaを焼いています。。。")

    def cut(self):
        print(f"{self.name} pizzaを切っています。。。")

    def box(self):
        print(f"{self.name} pizzaを箱に詰めています。。。")


class CheesePizza(Pizza):
    @property
    def name(self):
        return "cheese"


class MeatPizza(Pizza):
    @property
    def name(self):
        return "meat"


if __name__ == "__main__":
    simple_factory = SimpleFactory()
    store = PizzaStore(simple_factory)
    pizza = store.order_pizza("cheese")
    pizza = store.order_pizza("meat")
