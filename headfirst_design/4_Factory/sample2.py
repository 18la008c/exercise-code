# factory method
# simple factoryの違いは、
# 　　factory → is-aの関係にあるConcreteFactoryMethodにて、ConcreteProductを生成
#     simple → has-aの関係にある、SimpleFactoryにて、ConcreteProductを生成
# つまり、factory methodの方が具象クラスを分ければ、より多くのパターンに対応できるらしい。。。 P135より
# とはいえ、simple factoryもhas-aの関係のように、 ConcreteSimpleFactoryをたくさん作って、
# initで渡すfactoryを変える or set_factory(factory)のように動的に変えるようにすれば良いのでは？？ → **これは正にabstract factoryのこと**
# むしろこっちの方が、継承よりもcompositeの法則に従っているのでは？？？

from abc import ABC, abstractmethod
from pizza import NYCheesePizza, NYMeatPizza, ChicagoCheesePizza, ChicagoMeatPizza


class PizzaStore(ABC):
    # create classでは、抽象クラスに対するプログラミングしか存在しない！
    # 具象クラスの作成は、全て具象factory methodに任せている(カプセル化)！！
    # これによって、変更・追加は具象クラスのみで完結するので、open-closeを保てる。

    def order_pizza(self, pizza_type):
        print(f"{self.__class__.__name__}で作成します。")
        pizza = self.create(pizza_type)
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza

    @abstractmethod
    def create(self, pizza_type):
        # これがfactroy methodとなる！！！！
        pass


class NYPizzaStore(PizzaStore):
    def create(self, pizza_type):
        if pizza_type == "cheese":
            return NYCheesePizza()
        elif pizza_type == "meat":
            return NYMeatPizza()
        else:
            raise KeyError(f"{pizza_type} is not in menu")


class ChicagoStore(PizzaStore):
    def create(self, pizza_type):
        if pizza_type == "cheese":
            return ChicagoCheesePizza()
        elif pizza_type == "meat":
            return ChicagoMeatPizza()
        else:
            raise KeyError(f"{pizza_type} is not in menu")


if __name__ == "__main__":
    ny_store = NYPizzaStore()
    chicago_store = ChicagoStore()
    pizza = ny_store.order_pizza("cheese")
    pizza2 = chicago_store.order_pizza("meat")
