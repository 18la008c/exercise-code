from abc import ABC, abstractproperty


class Pizza(ABC):
    def __init__(self):
        self.toppings = []

    @abstractproperty
    def name(self):
        pass

    @abstractproperty
    def dough(self):
        pass

    @abstractproperty
    def sauce(self):
        pass

    def prepare(self):
        print(f"{self.name} を準備中。。。")
        for topping in self.toppings:
            print(f"{topping}を追加")

    def bake(self):
        print(f"{self.name} を焼いています。。。")

    def cut(self):
        print(f"{self.name} を切っています。。。")

    def box(self):
        print(f"{self.name} を箱に詰めています。。。")


class NYCheesePizza(Pizza):
    def __init__(self):
        super().__init__()
        self.toppings.append("reggiano_cheese")

    @property
    def name(self):
        return "NYstyle Cheese Pizza"

    @property
    def dough(self):
        return "Thin craft dough"  # このNY = 薄い生地は、NYPizza共通なので、NYFactoryで定義しても良い。

    @property
    def sauce(self):
        return "marinara sauce"


class NYMeatPizza(Pizza):
    def __init__(self):
        super().__init__()
        self.toppings.append("pepperoni")

    @property
    def name(self):
        return "NYstyle Meat Pizza"

    @property
    def dough(self):
        return "Thin craft dough"  # このNY = 薄い生地は、NYPizza共通なので、NYFactoryで定義しても良い。

    @property
    def sauce(self):
        return "marinara sauce"


class ChicagoCheesePizza(Pizza):
    def __init__(self):
        super().__init__()
        self.toppings.append("reggiano_cheese")

    @property
    def name(self):
        return "Chicago Cheese Pizza"

    @property
    def dough(self):
        return "Thick craft dough"  # このChicago=厚い生地は、ChicagoPizza共通なので、ChicagoFactoryで定義しても良い。

    @property
    def sauce(self):
        return "Tomato sauce"


class ChicagoMeatPizza(Pizza):
    def __init__(self):
        super().__init__()
        self.toppings.append("pepperoni")

    @property
    def name(self):
        return "Chicago Cheese Pizza"

    @property
    def dough(self):
        return "Thick craft dough"  # このChicago=厚い生地は、ChicagoPizza共通なので、ChicagoFactoryで定義しても良い。

    @property
    def sauce(self):
        return "Tomato sauce"
