# abstract factory
# sample2(factory method)を abstract factoryに書き換えたver
# 違う点
# - factory methodでは、NYStoreなどの具象クラス自体が、Pizzaを作成していた。
# - abstract factoryでは、 具象Factoryを、Storeにhas-aで持たせることでNY, Chicagoのような差を生み出せる。
# 違う点2
# - factory methodでは、1つmethodがPizzaの作成を担当 → 1つのProductしか作れない。
# - abstract factoryでは、 class全体が作成を担当する → 多数のProductを作成できる。
# 同じ点
# - どちらも、具象Prduct(NYPizzaなど)の作成を、隠蔽(カプセル化)する。 → これによって、抽象クラスにのみプログラミングできる。
# - 「abstract factoryが持つmethod」 = 「factory method」である。
# つまり
# - abstract factoryは、factory methodの集合したクラス。
# - abstract factroyから作成された、具象factory(NYPizzaFactroy等)を、has-aの関係にすることで使う。
# - 目的は、「具象クラスの作成の隠蔽」を行い、「抽象クラスにのみプログラミングの実現」
# - というか、simple factoryに抽象クラスを追加したver

from abc import ABC, abstractmethod
from pizza import NYCheesePizza, NYMeatPizza, ChicagoCheesePizza, ChicagoMeatPizza


class PizzaStore:
    # create classでは、抽象クラスに対するプログラミングしか存在しない！
    # 具象クラスの作成は、全て具象factory methodに任せている(カプセル化)！！
    # これによって、変更・追加は具象クラスのみで完結するので、open-closeを保てる。

    def __init__(self, pizza_factory):
        self.factory = pizza_factory

    def order_pizza(self, pizza_type):
        # ここでは、AbstractProductにのみコーディングしているので、どんなConcreteProductでも良い。
        # ★ ただし、現実問題こんな抽象化できるProductって存在しないのでは？
        # 少なくとも、methodは類似していても、必要な変数が異なる等はよくある。
        # この場合は、__init__で渡す、変数がConcreteProductごとに異なっていても良いの？？？
        pizza = self.create(pizza_type)
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()
        return pizza

    def create(self, pizza_type):
        # 実際に作るのは、ConcreteFactoryに移譲
        # 更にここでは、AbstractFactoryにのみコーディングしてるので、どんなConcreteFactoryでも良い。
        return self.factory.create_pizza(pizza_type)


class PizzaFactory(ABC):
    @abstractmethod
    def create_pizza(self, pizza_type):
        pass

    # abstrct factoryにすることで、Productを複数作成できるようになる。
    # 今回は面倒なので実装しないが、sidemenuも以下のようにして作れるようになる。

    # @abstractmethod
    # def create_sidemenu(self, sidemenu_type):
    #     pass


class NYPizzaFactory(PizzaFactory):
    def create_pizza(self, pizza_type):
        if pizza_type == "cheese":
            return NYCheesePizza()
        elif pizza_type == "meat":
            return NYMeatPizza()
        else:
            raise KeyError(f"{pizza_type} is not in menu")


class ChicagoPizzaFactory(PizzaFactory):
    def create_pizza(self, pizza_type):
        if pizza_type == "cheese":
            return ChicagoCheesePizza()
        elif pizza_type == "meat":
            return ChicagoMeatPizza()
        else:
            raise KeyError(f"{pizza_type} is not in menu")


if __name__ == "__main__":
    ny_store = PizzaStore(NYPizzaFactory())
    chicago_store = PizzaStore(ChicagoPizzaFactory())
    pizza = ny_store.order_pizza("cheese")
    pizza2 = chicago_store.order_pizza("meat")
