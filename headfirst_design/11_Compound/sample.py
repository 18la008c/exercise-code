# p432
# DuckSimulator1
# design pattern使用は特になし
from abc import abstractmethod


class BaseQuackable:
    @abstractmethod
    def quack(self):
        pass


class MallardDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class RedheadDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class DuckCall(BaseQuackable):
    def quack(self):
        print("ガアガア")


class RubberDuck(BaseQuackable):
    def quack(self):
        print("キューキュー")


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    mallard_duck = MallardDuck()
    red_head_duck = RedheadDuck()
    duck_call = DuckCall()
    rubber_duck = RubberDuck()

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck
    )

    duck_simu.do_test()
