# p433-4
# DuckSimulater2
# - Adapter Pattern使用

from abc import abstractmethod


class BaseQuackable:
    @abstractmethod
    def quack(self):
        pass


class MallardDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class RedheadDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class DuckCall(BaseQuackable):
    def quack(self):
        print("ガアガア")


class RubberDuck(BaseQuackable):
    def quack(self):
        print("キューキュー")


class Goose:
    # ガチョウクラス
    def honk(self):
        # 鳴き方が、鴨と少し違う。→ Adapter Patternを使用する
        print("ガー")


class GooseAdapter(BaseQuackable):
    # BaseQuackableを継承して、wrapperするだけ
    def __init__(self, goose: Goose) -> None:
        self.goose = goose

    def quack(self):
        # has-aにして、委譲するだけ
        self.goose.honk()


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    mallard_duck = MallardDuck()
    red_head_duck = RedheadDuck()
    duck_call = DuckCall()
    rubber_duck = RubberDuck()
    goose_duck = GooseAdapter(Goose())

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck,
        goose_duck
    )

    duck_simu.do_test()
