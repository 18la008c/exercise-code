# p443-12
# DuckSimulater5
# - Composite Pattern使用

from abc import abstractclassmethod, abstractmethod


class BaseQuackable:
    @abstractmethod
    def quack(self):
        pass


class MallardDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class RedheadDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class DuckCall(BaseQuackable):
    def quack(self):
        print("ガアガア")


class RubberDuck(BaseQuackable):
    def quack(self):
        print("キューキュー")


class Goose:
    # ガチョウクラス
    def honk(self):
        print("ガー")


class GooseAdapter(BaseQuackable):
    def __init__(self, goose: Goose) -> None:
        self.goose = goose

    def quack(self):
        self.goose.honk()


class QuackCounter(BaseQuackable):
    num_of_ducks = 0

    def __init__(self, duck: BaseQuackable) -> None:
        self.duck = duck

    def quack(self):
        self.duck.quack()
        QuackCounter.num_of_ducks += 1


class BaseDuckFactory:
    @abstractclassmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_duck_call(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        pass


class DuckFactory(BaseDuckFactory):
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return MallardDuck()

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return RedheadDuck()

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return DuckCall()

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return RubberDuck()


class CountingDuckFactory(BaseDuckFactory):
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return QuackCounter(MallardDuck())

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return QuackCounter(RedheadDuck())

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return QuackCounter(DuckCall())

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return QuackCounter(RubberDuck())


class Flock(BaseQuackable):
    # duckをleafに見立てて、duckの集まりであるFlockを作成する。(Composite Pattern)

    def __init__(self, *quackers: BaseQuackable):
        self.quackers = []
        if quackers:
            self.add(*quackers)

    def add(self, quacker, *quackers):
        self.quackers.extend((quacker,) + quackers)

    def quack(self):
        for quacker in self.quackers:
            quacker.quack()


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    mallard_duck = CountingDuckFactory.create_mallerd_duck()
    red_head_duck = CountingDuckFactory.create_red_head_duck()
    duck_call = CountingDuckFactory.create_duck_call()
    rubber_duck = CountingDuckFactory.create_rubber_duck()
    goose_duck = GooseAdapter(Goose())

    # 鴨groupを作成する。
    flock_of_mallards = Flock(
        CountingDuckFactory.create_mallerd_duck(),  # 鴨1
        CountingDuckFactory.create_mallerd_duck(),  # 鴨2
        CountingDuckFactory.create_mallerd_duck(),  # 鴨3
        CountingDuckFactory.create_mallerd_duck()   # 鴨4
    )

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck,
        goose_duck,
        flock_of_mallards  # Compostite Patternにより、groupもleafも同じように扱える。
    )

    duck_simu.do_test()

    print(f"<<「鴨」が鳴いた数: {QuackCounter.num_of_ducks} 回>>")
