# p436-8
# DuckSimulater3
# - decorator使用

from abc import abstractmethod


class BaseQuackable:
    @abstractmethod
    def quack(self):
        pass


class MallardDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class RedheadDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class DuckCall(BaseQuackable):
    def quack(self):
        print("ガアガア")


class RubberDuck(BaseQuackable):
    def quack(self):
        print("キューキュー")


class Goose:
    # ガチョウクラス
    def honk(self):
        print("ガー")


class GooseAdapter(BaseQuackable):
    def __init__(self, goose: Goose) -> None:
        self.goose = goose

    def quack(self):
        self.goose.honk()


class QuackCounter(BaseQuackable):
    # class毎に値を共有するため、class変数にする。
    num_of_ducks = 0

    def __init__(self, duck: BaseQuackable) -> None:
        self.duck = duck

    def quack(self):
        # 通常のquackに数のカウントを装飾する。
        # 絶対pythonならもっと良い書き方があるはず！！
        self.duck.quack()
        QuackCounter.num_of_ducks += 1


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    # duckクラスを、QuackCounterでdecorateする。
    # ただしこれを、mainやclient側が行うと、処理忘れ等が非常に起きやすい！！！ → 次でfactorypatternを使用する。
    mallard_duck = QuackCounter(MallardDuck())
    red_head_duck = QuackCounter(RedheadDuck())
    duck_call = QuackCounter(DuckCall())
    rubber_duck = QuackCounter(RubberDuck())
    goose_duck = GooseAdapter(Goose())

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck,
        goose_duck
    )

    duck_simu.do_test()

    print(f"<<「鴨」が鳴いた数: {QuackCounter.num_of_ducks} 回>>")
