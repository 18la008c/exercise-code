# p443-12
# DuckSimulater6
# - Observer使用

from abc import abstractclassmethod, abstractmethod


class BaseQuackable:
    def __init__(self):
        self.observers = []

    @abstractmethod
    def quack(self):
        pass

    # 監視される側のmethodを、Quackableに追加
    def add_observer(self, observer, *observers):
        self.observers.extend((observer,) + observers)

    def notify_observers(self):
        for observer in self.observers:
            observer.update(self)


class MallardDuck(BaseQuackable):
    def __str__(self):
        return "マガモ"

    def quack(self):
        print("ガーガー")
        self.notify_observers()


class RedheadDuck(BaseQuackable):
    def __str__(self):
        return "アメリカホシハジロ"

    def quack(self):
        print("ガーガー")
        self.notify_observers()


class DuckCall(BaseQuackable):
    def __str__(self):
        return "鴨笛"

    def quack(self):
        print("ガアガア")
        self.notify_observers()


class RubberDuck(BaseQuackable):
    def __str__(self):
        return "おもちゃの鴨"

    def quack(self):
        print("キューキュー")
        self.notify_observers()


class Goose:
    def honk(self):
        print("ガー")

    def __str__(self):
        return "ガチョウ"


class GooseAdapter(BaseQuackable):
    def __init__(self, goose: Goose) -> None:
        self.goose = goose
        super().__init__()

    def __str__(self):
        return self.goose.__str__()

    def quack(self):
        self.goose.honk()  # ガチョウクラスに委譲
        self.notify_observers()  # duckクラスに委譲


class QuackCounter(BaseQuackable):
    num_of_ducks = 0

    def __init__(self, duck: BaseQuackable) -> None:
        self.duck = duck

    def __str__(self):
        return str(self.duck)

    def quack(self):
        self.duck.quack()
        QuackCounter.num_of_ducks += 1

    def add_observer(self, *observers):
        # duckクラスに委譲
        self.duck.add_observer(*observers)


class BaseDuckFactory:
    @abstractclassmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_duck_call(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        pass


class DuckFactory(BaseDuckFactory):
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return MallardDuck()

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return RedheadDuck()

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return DuckCall()

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return RubberDuck()


class CountingDuckFactory(BaseDuckFactory):
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return QuackCounter(MallardDuck())

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return QuackCounter(RedheadDuck())

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return QuackCounter(DuckCall())

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return QuackCounter(RubberDuck())


class Flock(BaseQuackable):
    def __init__(self, *quackers: BaseQuackable):
        self.quackers = []
        if quackers:
            self.add(*quackers)

    def add(self, quacker, *quackers):
        self.quackers.extend((quacker,) + quackers)

    def quack(self):
        for quacker in self.quackers:
            quacker.quack()

    def add_observer(self, *observers):
        # 子供のそれぞれに委譲する。
        for quacker in self.quackers:
            quacker.add_observer(*observers)


class Quackologist:
    # observer class
    def update(self, duck):
        print(f"鴨鳴き声学者: {duck}が鳴きました")


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    # instance(duck)の作成を全てfactoryに任せた。
    mallard_duck = CountingDuckFactory.create_mallerd_duck()
    red_head_duck = CountingDuckFactory.create_red_head_duck()
    duck_call = CountingDuckFactory.create_duck_call()
    rubber_duck = CountingDuckFactory.create_rubber_duck()
    goose_duck = GooseAdapter(Goose())

    # 鴨groupを作成する。
    flock_of_mallards = Flock(
        CountingDuckFactory.create_mallerd_duck(),  # 鴨1
        CountingDuckFactory.create_mallerd_duck(),  # 鴨2
        CountingDuckFactory.create_mallerd_duck(),  # 鴨3
        CountingDuckFactory.create_mallerd_duck()   # 鴨4
    )

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck,
        goose_duck,
        flock_of_mallards  # Compostite Patternにより、groupもleafも同じように扱える。
    )

    guackologist = Quackologist()

    for duck in duck_simu.ducks:
        duck.add_observer(guackologist)

    print("<<鴨シミュレーターwith Observer>>")
    duck_simu.do_test()
    print(f"<<「鴨」が鳴いた数: {QuackCounter.num_of_ducks} 回>>")
