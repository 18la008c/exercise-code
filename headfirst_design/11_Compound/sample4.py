# p438-10
# DuckSimulater4
# - Abstract Factory使用

from abc import abstractclassmethod, abstractmethod


class BaseQuackable:
    @abstractmethod
    def quack(self):
        pass


class MallardDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class RedheadDuck(BaseQuackable):
    def quack(self):
        print("ガーガー")


class DuckCall(BaseQuackable):
    def quack(self):
        print("ガアガア")


class RubberDuck(BaseQuackable):
    def quack(self):
        print("キューキュー")


class Goose:
    # ガチョウクラス
    def honk(self):
        print("ガー")


class GooseAdapter(BaseQuackable):
    def __init__(self, goose: Goose) -> None:
        self.goose = goose

    def quack(self):
        self.goose.honk()


class QuackCounter(BaseQuackable):
    num_of_ducks = 0

    def __init__(self, duck: BaseQuackable) -> None:
        self.duck = duck

    def quack(self):
        self.duck.quack()
        QuackCounter.num_of_ducks += 1


class BaseDuckFactory:
    @abstractclassmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_duck_call(cls) -> BaseQuackable:
        pass

    @abstractclassmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        pass


class DuckFactory(BaseDuckFactory):
    # 普通のduckを作る、abstract factory
    # 正直、単純にclass作成をしているだけなので必要ないが、わかりやすさの為作成
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return MallardDuck()

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return RedheadDuck()

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return DuckCall()

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return RubberDuck()


class CountingDuckFactory(BaseDuckFactory):
    # 鳴いた数を数えられるduckを作るclass
    # QuackCounterでdecorateしたduckを返す。これをfactoryで行うことで、clientから隠蔽できる。
    @classmethod
    def create_mallerd_duck(cls) -> BaseQuackable:
        return QuackCounter(MallardDuck())

    @classmethod
    def create_red_head_duck(cls) -> BaseQuackable:
        return QuackCounter(RedheadDuck())

    @classmethod
    def create_duck_call(cls) -> BaseQuackable:
        return QuackCounter(DuckCall())

    @classmethod
    def create_rubber_duck(cls) -> BaseQuackable:
        return QuackCounter(RubberDuck())


class DuckSimulator:
    # Client Class

    def __init__(self, *ducks: BaseQuackable) -> None:
        self.ducks = []
        if ducks:
            self.add(*ducks)

    def add(self, duck, *ducks):
        self.ducks.extend((duck,) + ducks)

    def simulate(self, duck: BaseQuackable) -> None:
        duck.quack()

    def do_test(self):
        print("<<<鴨シミュレーターを起動します。>>>")

        for duck in self.ducks:
            self.simulate(duck)


if __name__ == "__main__":
    # instance(duck)の作成を全てfactoryに任せた。
    mallard_duck = CountingDuckFactory.create_mallerd_duck()
    red_head_duck = CountingDuckFactory.create_red_head_duck()
    duck_call = CountingDuckFactory.create_duck_call()
    rubber_duck = CountingDuckFactory.create_rubber_duck()
    goose_duck = GooseAdapter(Goose())

    duck_simu = DuckSimulator(
        mallard_duck,
        red_head_duck,
        duck_call,
        rubber_duck,
        goose_duck
    )

    duck_simu.do_test()

    print(f"<<「鴨」が鳴いた数: {QuackCounter.num_of_ducks} 回>>")
