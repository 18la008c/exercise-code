# ハリウッドの原則
- 低水準コンポーネント(具象クラス)が、高水準コンポーネント(基底クラス)を呼びださない。

## 通常(よくあるパターン)
- 基底クラスに共通method定義
- 具象クラスに、共通methodを使用して、実際に目的の動作を実行するmethodを定義する。
```
class Base:
    def __init__(self, user, pass):
        self.si = self._connect(user, pass)

    def _connect(self):
        pass
    
    def get_cluster_obj(self):
        # よく使う共通の処理
        pass
    
    def get_vm_obj(self):
        # よく使う共通の処理
        pass

class Main(Base):
    def get_special_vm(self):
        # 基底クラスのmethodを使う。低水準コンポーネントが高水準コンポーネントを呼び出している。
        vms = self.get_vm_obj() 
        return [vm for vm in vms if "special" in vm.name]

    ```

## template method(ハリウッドの原則使用)
- 抽象クラスに存在する、template method(例えば、sample.pyのprepare_recipe)が、具象クラスのmethodを使用する。
- これは、高水準コンポーネントが、低水準コンポーネントを呼び出している。
- 本来はこちらの方が良いが、少なくとも循環は避けるべきと言われている。(p298)