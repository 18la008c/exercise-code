# Template Method
# classの中に手順を作っても良い！！！
# 更には、類似した手順を持つ場合は、抽象クラスに手順を割り振って良い！

from abc import abstractmethod


class CaffinBeverage:

    def prepare_recipe(self):
        # template method
        # overrideしないので、これを確定手順できる。
        # 細かい作業は、具象クラスに任せられる。
        self.boil_water()
        self.brew()
        self.pour_in_cup()
        self.add_condiments()

    @abstractmethod
    def brew(self):
        pass

    @abstractmethod
    def add_condiments(self):
        pass

    def boil_water(self):
        # 具象method扱い。具象クラスでoverrideしない。
        print("お湯を沸かします。")

    def pour_in_cup(self):
        # 具象method扱い。具象クラスでoverrideしない。
        print("カップに注ぎます。")


class Tea(CaffinBeverage):
    # template methodで定義されている手順(method)を実装する。
    def brew(self):
        print("紅茶を浸します。")

    def add_condiments(self):
        print("レモンを追加します。")


class Coffee(CaffinBeverage):
    def brew(self):
        print("コーヒーをドリップします。")

    def add_condiments(self):
        print("砂糖とミルクを追加します。")


if __name__ == "__main__":
    print("---Teaを作ります---")
    Tea().prepare_recipe()
    print("---Coffeeを作ります---")
    Coffee().prepare_recipe()
