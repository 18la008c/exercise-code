# Template Method add hook method
# classの中に手順を作っても良い！！！
# 更には、類似した手順を持つ場合は、抽象クラスに手順を割り振って良い！
# hook methodを使う基準は、そのmethod自体は手順自身ではなく、手順を補佐するもののとき

from abc import abstractmethod


class CaffinBeverage:

    def prepare_recipe(self):
        # template method
        # overrideしないので、これを確定手順できる。
        # 細かい作業は、具象クラスに任せられる。
        self.boil_water()
        self.brew()
        self.pour_in_cup()
        if self.is_add_condiments():
            self.add_condiments()

    @abstractmethod
    def brew(self):
        pass

    @abstractmethod
    def add_condiments(self):
        pass

    def boil_water(self):
        # 具象method扱い。具象クラスでoverrideしない。
        print("お湯を沸かします。")

    def pour_in_cup(self):
        # 具象method扱い。具象クラスでoverrideしない。
        print("カップに注ぎます。")

    def is_add_condiments(self):
        # hook method
        # Template Methodでのオプション手順を設定できる。
        # 具象クラスでこのmethodをoverrideして、実行するorしないを変更可能。
        return True


class Tea(CaffinBeverage):
    # template methodで定義されている手順(method)を実装する。
    def brew(self):
        print("紅茶を浸します。")

    def add_condiments(self):
        print("レモンを追加します。")


class Coffee(CaffinBeverage):
    def brew(self):
        print("コーヒーをドリップします。")

    def add_condiments(self):
        print("砂糖とミルクを追加します。")

    def is_add_condiments(self):
        # コーヒーだけトッピング追加をオプションにできた。(hook methodの効果？)
        # 別にhook methodではなく抽象メソッド作成→具象クラスでoverrideすれば良い気がする。
        # headfirst曰く
        # optionで行っても、行わなくても良い場合はhookを使う。→なぜなら具象クラスでhook methodを実装しなくても良いから。
        # template method内の各手順の依存性判断を、hook methodに行わせることで、手順：手順を実行するだけ、依存性:hookと分離できる。(p295)
        var = input("コーヒーに砂糖とミルクを入れますか？(y/n):")
        if var in ["y", "Y", "yes"]:
            return True
        elif var in ["n", "N", "no"]:
            return False
        else:
            raise ValueError("Only accept y/n.")


if __name__ == "__main__":
    print("---Teaを作ります---")
    Tea().prepare_recipe()
    print("---Coffeeを作ります---")
    Coffee().prepare_recipe()
