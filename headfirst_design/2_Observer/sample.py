# 良いところ
# - 追加のobserverは、Observerを継承した具象クラスを作る & add_observerするだけで良い!
# 疑問
# - update(self.data)のように、update時に渡す定数に関しては依存関係がある。
# - 1つ定数を変える = 全てのobserverに編集が必要。。。
from abc import ABC, abstractmethod
import time
import datetime


class Subject(ABC):
    def __init__(self):
        self.observer_set = set()

    def add_observer(self, observer, *observers):
        for o in (observer,) + observers:
            self.observer_set.add(o)

    def remove_observer(self, observer):
        self.observer_set.discard(observer)

    @abstractmethod
    def notify_observer(self):
        pass


class WeatherData(Subject):
    def __init__(self, data=None):
        super().__init__()
        self.__data = None
        if data is None:
            self.data = self._init_data()
        else:
            self.data = data

    def _init_data(self):
        return {
            "tempature": 0,
            "humidity": 0,
            "pressure": 0
        }

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        if self.__data != data:
            self.__data = data
            self.notify_observer()

    def notify_observer(self,):
        for o in self.observer_set:
            o.update(self.data)


class Observer(ABC):
    @abstractmethod
    def update(self, data):
        pass

    @abstractmethod
    def display(self):
        pass


class CurrentConditonsDisplay(Observer):
    def update(self, data):
        self.data = data
        self.display()

    def display(self):
        tmp = "現在の気象情報: 温度{}度 湿度{}%".format(
            self.data["tempature"],
            self.data["humidity"]
        )
        print(tmp)


class LoggerDisplay(Observer):
    def __init__(self):
        self.log = []

    def update(self, data):
        self.data = data
        self.log.append(self.format())

    def format(self):
        return "tmp:{}, hum:{}, pre:{}, {}".format(
            self.data["tempature"],
            self.data["humidity"],
            self.data["pressure"],
            datetime.datetime.fromtimestamp(time.time())
        )

    def display(self):
        print("\n".join(self.log))


if __name__ == "__main__":
    cc_display = CurrentConditonsDisplay()
    log_display = LoggerDisplay()
    weather_data = WeatherData()
    weather_data.add_observer(cc_display, log_display)
    weather_data.data = {
        "tempature": 23,
        "humidity": 70,
        "pressure": 10
    }
