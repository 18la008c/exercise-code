# 色んな処理が追加・拡張されるから、open-closeにしたい == 抽象, 具象クラスを作りたい。
# でも、色んな処理(具象クラス)同士に依存関係がある場合はどうするの？
# 依存関係1
# - ProcessAが、Trueかつ、ProcessBもTrueだったらERROR扱いにしたい
# 依存関係2
# - ProcessBを実行した後に、ProcessAを実行したい
#


from abc import abstractmethod
from headfirst.question import ProductB


def main():
    class AbstractProcess:

        def __init__(self, hosts_info):
            self.hosts_info = hosts_info

        @abstractmethod
        def execute():
            pass

    class ProcessA(AbstractProcess):
        def execute():
            pass

    trouble_appeared_host_info = "適当なhost情報が格納されたinstance or dataframe"
    # この後に、色んなprocessを実行して、最終的に対処が必要か確認したい。
    # chain of responsibilityとか良いけど。。。依存関係あるから使えない。。。
    # 以下みたいにConcreteProcessを直書きするのも、よくない。。。結局新たなConcreteProcessが追加された時に修正が必要

    if ProcessA(trouble_appeared_host_info).execute():
        return "Error"
    elif ProductB(trouble_appeared_host_info).execute() and ProductC(trouble_appeared_host_info).execute():
        return "Error"
    else:
        return "OK"


def main2():
    # 理想!!!!!

    class Main:
        def __init__(self, host_info):
            self.processes = []
            self.host_info = host_info

        def add_process(self, process):
            self.processes.append(process)

        def check_serverity(self):
            # ここで抽象にのみプログラミングが実現できている。　→ でも実際には具象クラスに依存関係がある。。。
            return any(process.execute() for process in self.processes)

    host_info = "適当なhostinfo"
    main = Main(host_info)
    main.add_process(ProcessA, ProductB, ProductC)  # こんな感じであれば
    main.check_serverity()


def main3():
    # 理想!!!

    host_info = "適当なhostinfo"
    # chain of responsibilityを使いつつ、どんな順番で実行しても良い
    processes = ProcessA(ProcessB(ProcessC(NullProcess())))
    processes.execute(host_info)
