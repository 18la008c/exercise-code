# sample.py vs sample2.py
- sample2.pyは、sample.pyに対して以下の要素を追加した。
    - Beverageにsizeの要素を追加
    - sizeに応じてcondimentの値段の変更
- この2つのfileを比較すると、既存のクラスにはほとんど変更が入っておらずに要素を追加することができている。
    - 変化しやすいcondimentは、当然decoratorとして分離したが、
    - 中心となるBeverage自体を変更しても、修正が少なくすむ！