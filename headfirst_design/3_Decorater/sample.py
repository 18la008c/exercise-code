from abc import ABC, abstractmethod, abstractproperty


class Beverage(ABC):

    @abstractproperty
    def description(self):
        pass

    @abstractmethod
    def cost(self):
        pass


class Houseblend(Beverage):

    @property
    def description(self):
        return "houseblend"

    def cost(self):
        return 0.89


class Espresso(Beverage):

    @property
    def description(self):
        return "espresso"

    def cost(self):
        return 1.99


class CondimentDecorator(Beverage):
    # abstract class(Beverage) から、　abstract class(Decorator)を作る方法とは？
    # abstract classを継承したからには、abstractmethodを書かないといけないが。。。
    # このクラスもabstract class...

    def __init__(self, beverage):
        self.beverage = beverage

    @property
    def description(self):
        pass

    def cost(self):
        pass


class Moca(CondimentDecorator):

    @property
    def description(self):
        return self.beverage.description + ", moca"

    def cost(self):
        return self.beverage.cost() + 0.2


class Whip(CondimentDecorator):

    @property
    def description(self):
        return self.beverage.description + ", whip"

    def cost(self):
        return self.beverage.cost() + 0.5


if __name__ == "__main__":
    esspresso = Espresso()
    print(f"{esspresso.description} is ${esspresso.cost()}")

    esspresso_with_condiment = Whip(Moca(Espresso()))
    print(f"{esspresso_with_condiment.description} is ${esspresso_with_condiment.cost()}")
