# practice p99
# beverageにsizeを追加、sizeに応じたtopping料金に変更

from abc import ABC, abstractmethod, abstractproperty


class Beverage(ABC):
    def __init__(self, size="small"):
        self.__size = size

    @abstractproperty
    def description(self):
        pass

    @abstractmethod
    def cost(self):
        pass

    @property
    def size(self):
        return self.__size

    @size.setter
    def size(self, size):
        menu = ["small", "medium", "large"]
        if size not in menu:
            raise KeyError(f"Choose from {', '.join(menu)}")
        self.__size = size


class Houseblend(Beverage):

    @property
    def description(self):
        # basename = houseblendのようにして、ここだけ具象クラスでoverride
        # descriptionはabst classで定義しても良いかも。。。
        return f"houseblend({self.size})"

    def cost(self):
        return 0.89


class Espresso(Beverage):

    @property
    def description(self):
        return f"espresso({self.size})"

    def cost(self):
        return 1.99


class CondimentDecorator(Beverage):
    # abstract class(Beverage) から、　abstract class(Decorator)を作る方法とは？
    # abstract classを継承したからには、abstractmethodを書かないといけないが。。。
    # このクラスもabstract class...

    def __init__(self, beverage, size="small"):
        super().__init__(size)
        self.beverage = beverage

    @property
    def description(self):
        pass

    def cost(self):
        pass


class Moca(CondimentDecorator):

    @property
    def description(self):
        return self.beverage.description + ", moca"

    def cost(self):
        return self.beverage.cost() + 0.2


class Whip(CondimentDecorator):

    @property
    def description(self):
        return self.beverage.description + ", whip"

    def cost(self):
        return self.beverage.cost() + 0.5


class Soy(CondimentDecorator):

    @property
    def description(self):
        return self.beverage.description + ", soy"

    def cost(self):
        # sizeによってcondimentの金額が変わる！
        price = {
            "small": 0.1,
            "medium": 0.15,
            "large": 0.2
        }
        return self.beverage.cost() + price[self.beverage.size]


if __name__ == "__main__":
    esspresso = Espresso()
    print(f"{esspresso.description} is ${esspresso.cost()}")

    esspresso_with_condiment = Whip(Moca(Espresso()))
    print(f"{esspresso_with_condiment.description} is ${esspresso_with_condiment.cost()}")

    houseblend = Soy(Houseblend(size="small"))
    print(f"{houseblend.description} is ${houseblend.cost()}")

    large_houseblend = Soy(Houseblend(size="large"))
    print(f"{large_houseblend.description} is ${large_houseblend.cost()}")
