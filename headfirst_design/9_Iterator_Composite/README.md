# Iterator
- Iteratorとは、内部実装を一切見せずに、**順番にアクセスする方法のみを公開**する。
- これによってClient(使う側)は、順番にアクセスする方法だけ知っていれば良いことになる。
     - 例えば、順番にアクセスする際に必要な複雑な手法を全て隠すことができる。
- これによって、list, set, tuple等さまざまなobjectであっても、`for ... in ...`の形でループできる。
     - 注意: list等はIterableであるが、Iteratorではない。
     - https://blog.hirokiky.org/entry/2020/02/03/181938