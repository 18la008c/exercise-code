"""
memo

p318
sample2.pyの改善例1
- PancakeHouseMenuと、DinnerHouseMenuの、menu_itemsを自作iteratorにした。
- これによりIteratorにより、統一インターフェースをもち、for ... in ...が使えるようになり\
    抽象に対してプログラミングできるようになった。
"""

from dataclasses import dataclass


@dataclass
class MenuItem:
    name: str
    description: str
    vegetarian: bool
    price: float


class PancakeHouseMenu:
    def __init__(self):
        self.menu_items = {}
        self.__position = 0

    def __iter__(self):
        # 自分自身がiteratorになったので、自分自身を返す。
        return self

    def __next__(self):
        # next methodを定義したことによって、PancakeとDinnerがどちらも同じループ方法になる。
        if self.__position >= len(self.menu_items):
            raise StopIteration
        value = self.menu_items[self.__position]
        self.__position += 1
        return value

    def add_item(self, name, description, vegetarian, price):
        num = len(self.menu_items)
        self.menu_items[num] = MenuItem(
            name,
            description,
            vegetarian,
            price
        )


def init_pancake_menu():
    menu = PancakeHouseMenu()
    menu.add_item(
        "K&Bパンケーキ朝食",
        "スクランブルエッグとトースト付きのパンケーキ",
        True,
        2.99
    )
    menu.add_item(
        "パンケーキ朝食",
        "卵焼きとソーセージ付きのパンケーキ",
        False,
        3.49
    )
    menu.add_item(
        "ブルーベリーパンケーキ",
        "ブルーベリー付きパンケーキ",
        True,
        3.49
    )
    return menu


class DinnerMenu:
    MAX_SIZE = 6

    def __init__(self):
        self.menu_items = []
        self.__position = 0

    def __iter__(self):
        # menu_itemsではなく、DinnerMenu自体をiteatorにする。
        return self

    def __next__(self):
        # next methodを定義したことによって、PancakeとDinnerがどちらも同じループ方法になる。
        if self.__position >= len(self.menu_items):
            raise StopIteration
        value = self.menu_items[self.__position]
        self.__position += 1
        return value

    def add_item(self, name, description, vegetarian, price):
        if len(self.menu_items) >= DinnerMenu.MAX_SIZE:
            raise IndexError(f"menuのサイズは、{DinnerMenu.MAX_SIZE}までです。")

        self.menu_items.append(
            MenuItem(
                name,
                description,
                vegetarian,
                price
            )
        )


def init_dinner_menu():
    menu = DinnerMenu()
    menu.add_item(
        "ベジタリアンBLT",
        "レタス、トマト、大豆ミートを挟んだサンドイッチ",
        True,
        2.99
    )
    menu.add_item(
        "BLT",
        "レタス、トマト、ベーコンを挟んだサンドイッチ",
        False,
        2.99
    )
    menu.add_item(
        "本日のスープ",
        "ポテトサラダを添えた、本日のスープ",
        False,
        3.29
    )
    return menu


class Waitress:
    def __init__(self):
        self.pancake_menu = init_pancake_menu()
        self.dinner_menu = init_dinner_menu()

    def _print_menu(self, menu_iterator):
        # pancakeもdinnerもiteratorにしたので、抽象クラスに対してプログラミングできた。(for ... in ..で統一)
        # 仮にmenuが追加/変更されても、このmethodは変える必要がない。
        for menu in menu_iterator:
            print(f"{menu.name}-{menu.price}: {menu.description}")

    def print_menu(self):
        # ここはlunch(pancake)とdinnerという名称の差から具象クラス依存が出てくる
        # →気になるなら、set_menu(menu)等で、追加する形にすれば良い。
        # ただし肝心のmenuの出力は、_print_menuで、抽象クラスに対しての実装となっている。
        print("Menu\n---")
        print("昼食")
        self._print_menu(self.pancake_menu)  # 統一化
        print("夕食")
        self._print_menu(self.dinner_menu)  # 　統一化


if __name__ == "__main__":
    waitress = Waitress()
    waitress.print_menu()
