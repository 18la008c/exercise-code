"""
memo

p318
悪い例
- PancakeHouseMenuと、DinerHouseMenuの型が違うためforループでまとめて扱えない
    - わざとPancakeHouseMenuはdict、DinerHouseMenuはlist型にしている。
- 本来は統一することで、抽象クラスに対してプログラミングできるようになるが、\
    型が違う際に、元のクラス(PancakeHouseMenu等)を変えずに、抽象化(Iterator)するか見る
"""

from dataclasses import dataclass


@dataclass
class MenuItem:
    name: str
    description: str
    vegetarian: bool
    price: float


class PancakeHouseMenu:
    def __init__(self):
        self.menu_items = {}

    def add_item(self, name, description, vegetarian, price):
        num = len(self.menu_items)
        self.menu_items[num] = MenuItem(
            name,
            description,
            vegetarian,
            price
        )


def init_pancake_menu():
    menu = PancakeHouseMenu()
    menu.add_item(
        "K&Bパンケーキ朝食",
        "スクランブルエッグとトースト付きのパンケーキ",
        True,
        2.99
    )
    menu.add_item(
        "パンケーキ朝食",
        "卵焼きとソーセージ付きのパンケーキ",
        False,
        3.49
    )
    menu.add_item(
        "ブルーベリーパンケーキ",
        "ブルーベリー付きパンケーキ",
        True,
        3.49
    )
    return menu


class DinnerMenu:
    MAX_SIZE = 6

    def __init__(self):
        self.menu_items = []

    def add_item(self, name, description, vegetarian, price):
        if len(self.menu_items) >= DinnerMenu.MAX_SIZE:
            raise IndexError(f"menuのサイズは、{DinnerMenu.MAX_SIZE}までです。")

        self.menu_items.append(
            MenuItem(
                name,
                description,
                vegetarian,
                price
            )
        )


def init_dinner_menu():
    menu = DinnerMenu()
    menu.add_item(
        "ベジタリアンBLT",
        "レタス、トマト、大豆ミートを挟んだサンドイッチ",
        True,
        2.99
    )
    menu.add_item(
        "BLT",
        "レタス、トマト、ベーコンを挟んだサンドイッチ",
        False,
        2.99
    )
    menu.add_item(
        "本日のスープ",
        "ポテトサラダを添えた、本日のスープ",
        False,
        3.29
    )
    return menu


class Waitress:
    def __init__(self):
        self.pancake_menu = init_pancake_menu()
        self.dinner_menu = init_dinner_menu()

    def print_menu(self):
        # 悪い例
        # pancakeとdinnerの型が異なるせいで、具象クラスに対してプログラミングしている。。。
        # 仮に追加のmenuが来た場合(ex.lunch menu)や、既存menuの変更があった場合は、print_menuの書き換えが必須
        # → 開放閉鎖の原則を守れてない。

        for key in self.pancake_menu.menu_items:
            menu = self.pancake_menu.menu_items[key]
            print(f"{menu.name}-{menu.price}: {menu.description}")

        for menu in self.dinner_menu.menu_items:
            print(f"{menu.name}-{menu.price}: {menu.description}")


if __name__ == "__main__":
    waitress = Waitress()
    waitress.print_menu()
