"""
memo

sample3.py(Iterator)に、Compositeを追加したバターン
- compositeパターンで、compositeとleafの役割を1つの抽象classでまかなう。
- 役割の差は具象クラスである、composite, leafで実装するorしないで生み出す。
    - 例えば、leafにのみpriceが存在し、compositeにのみadd, removeが存在するなど
- これによって、Client側はコンポーネントが、leafなのかcompositeなのか気にせずプログラミング可能である。
- tree構造のややこしいものを気にしなくてよくて非常に便利
"""
from dataclasses import dataclass, field


class BaseMenuComponent:
    def add(self, menu_component):
        raise AttributeError()

    def remove(self, menu_component):
        raise AttributeError()

    def print(self):
        raise AttributeError()


@dataclass
class MenuItem(BaseMenuComponent):
    name: str
    description: str
    vegetarian: bool
    price: float

    def print(self):
        name = f"{self.name}(v)" \
               if self.vegetarian else self.name

        print(f"{name} ${self.price}\n  - {self.description}")


@dataclass
class Menus(BaseMenuComponent):
    name: str
    description: str
    # mutableの場合は初期値設定は、dataclasses.fieldを使う必要があるらしい
    # https://zenn.dev/enven/articles/8b80ff38461b4ff329aa
    # imutableの場合は、以下で良い。
    # sample_var: int=0
    menu_components: list = field(default_factory=list)

    @property
    def vegetarian(self):
        raise AttributeError()

    @property
    def price(self):
        raise AttributeError()

    def add(self, menu_component: BaseMenuComponent):
        assert isinstance(menu_component, BaseMenuComponent)
        self.menu_components.append(menu_component)

    def remove(self, menu_component):
        self.menu_components.remove(menu_component)

    def print(self):
        """
        print順序は、menus→items→別menus→別menusのitemsの順で表示したい。
        従って、menu_componentsの順序をitems→menusの順に並び替える必要がある。
        """
        # せっかくcompositeパターンで、composite(menus)なのか、leaf(items)なのか、気にしなくて良いはずなのに...
        # とは言えこのtype依存関係は、compositeの中だけで収まっている。
        # Client(使う人)から見たら、compositeなのかleafなのか気にしなくて良いままなので別にいい気がする！！！
        # (そもそも、componentsとleafで使えるmethodも違うし)
        menus = [
            x for x in self.menu_components
            if isinstance(x, Menus)
        ]
        items = [
            x for x in self.menu_components
            if isinstance(x, MenuItem)
        ]
        print(f"\n ### [{self.description}] {self.name} ###\n---")
        for component in (items + menus):
            component.print()


class Waitless:
    def __init__(self, name, menu_components: BaseMenuComponent):
        self.all_menu = menu_components
        self.name = name

    def print_menu(self):
        # Client(waitless)は、printを呼び出すことだけ知っていれば良い。
        # それがcompositeだろうが、leafだろうが、BaseMenuComponentsでありさえすれば良い。
        print(f">>>{self.name}です。メニューをお知らせします。")
        self.all_menu.print()


def main():
    # diner: 定食屋、食堂
    pancake_menu = Menus(name="パンケーキハウスメニュー", description="朝食")
    diner_menu = Menus(name="食堂メニュー", description="昼食")
    cafe_menu = Menus(name="カフェメニュー", description="夕食")
    dessert_menu = Menus(name="デザートメニュー", description="デザート")
    all_menu = Menus(name="全てのメニュー", description="全てを統合したメニュー")

    # treeのtopであるall_menuに、Menusを追加する。
    all_menu.add(pancake_menu)
    all_menu.add(diner_menu)
    all_menu.add(cafe_menu)

    # Menusの中に、Menusが入るtree構造になる。
    diner_menu.add(dessert_menu)

    # Menuの中に、MenuItemを追加していく。
    pancake_menu.add(MenuItem(
        "K&Bパンケーキ朝食",
        "スクランブルエッグとトースト付きのパンケーキ",
        True,
        2.99
    ))
    pancake_menu.add(MenuItem(
        "パンケーキ朝食",
        "卵焼きとソーセージ付きのパンケーキ",
        False,
        3.49
    ))
    pancake_menu.add(MenuItem(
        "ブルーベリーパンケーキ",
        "ブルーベリー付きパンケーキ",
        True,
        3.49
    ))
    diner_menu.add(MenuItem(
        "ベジタリアンBLT",
        "レタス、トマト、大豆ミートを挟んだサンドイッチ",
        True,
        2.99
    ))
    diner_menu.add(MenuItem(
        "BLT",
        "レタス、トマト、ベーコンを挟んだサンドイッチ",
        False,
        2.99
    ))
    diner_menu.add(MenuItem(
        "本日のスープ",
        "ポテトサラダを添えた、本日のスープ",
        False,
        3.29
    ))
    dessert_menu.add(MenuItem(
        "アップルパイ",
        "バニラアイスを乗せたアップルパイ",
        True,
        1.59
    ))
    cafe_menu.add(MenuItem(
        "野菜バーガーとフライドポテト",
        "パンにレタスとトマトを挟んだバーガーと、フライドポテト",
        True,
        3.99
    ))
    waitless = Waitless("ベテランウエイトレス", all_menu)
    waitless.print_menu()


if __name__ == "__main__":
    main()
