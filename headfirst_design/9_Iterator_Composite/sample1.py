# p322
"""
A. Yes,
PancakeHouseMenuではarraylist, DinnerMenuではlistを使用しており
それぞれに対応した、print方法を記述しているため。
→改善方法としては、BaseMenuを作成し、その具象クラスとして、Pancake, Dinnerを作れば良い

B. No,
よくわからん

C. Yes
Aで話したように、具象クラスに対して実装しているため、新たな具象クラスが登場すれば、
再コーディングが必要

D. Yes
具象クラスに対してコーディングしているので、そもそもカプセル化されていません。
実際に、arraylistとlistの表現の差を考慮しながらプログラミングしています。

E. Yes
具象クラスに対してコーディングしており、Menu毎に別のforループを用意しているためです。

F. No
よくわからん

"""

# つまり？
"""
PancakeHouseMenu: ArrayList
DinnerMenu:List
と種類が違うため一緒にforループできない！！！
↓
じゃあ、同じになるwrapper(抽象クラス)を用意しましょう
↓
Iterator
"""

# p340
"""
Game. Yes
- login関連動作
- game関連動作(move等)

Person. Yes
- Person自身の属性(SetName)
- 保存処理(save)

Phone Yes
- 電話かける関連(dial)
- Data関連(sendData)

GumballMachine No
- Gumball自身に対する処理のみ

DeckOfCards Yes
- Cardに対する処理
- Iteraterとしての処理
"""
