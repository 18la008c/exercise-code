# factoryでの疑問1
# ConcreteProductの性質が異なりすぎている場合。。。
# それはそもそもOOPとして、成り立っていない気もするけど。。。

from abc import abstractmethod


class Main:
    def __init__(self, factory):
        self.factory = factory

    def create(self):
        return self.factory.create_product()

    def run(self):
        product = self.create()
        product.method1()  # Q. そもそもこんな感じに、作ったConcreteProductに同じmethodでくくれるの？？？？
        product.method2()  # A. くくれないなら、抽象化できてないしOOPを使うべきでない事例


class AbstractProduct:
    def __init__(self, name, product_id):
        self.name = name
        self.product_id = product_id

    @abstractmethod
    def method1(self):
        pass

    @abstractmethod
    def method2(self):
        pass


class ProductA(AbstractProduct):
    def methodA(self):
        pass

    def methodB(self):
        pass


class ProductB(AbstractProduct):
    def __init__(self, name, product_id, additional_param):
        # Q. ProductBだけ、additonal_paramが必要になるなど、ConcreteProduct同士で必要な変数が異なっても良いの？　 抽象化からずれてくるけど。
        # A. 少なくとも今回は、ConcreteProductが吸収してくれるから問題ないはず？？？ そうするとConcreteProductで差異が出てきそうだけど。。。
        super().__init__(name, product_id)
        self.param = additional_param

    def methodA(self):
        # addtional_paramが必要なmethod
        pass

    def methodB(self):
        pass


class AbstractFactory:
    @abstractmethod
    def create_product(self):
        pass


class ProductAFactory(AbstractFactory):
    def create_product(self):
        return ProductA(self._generate_name(), self._generate_id())

    def _generate_name(self):
        return "nameA"

    def _generate_id(self):
        return "product-A"


class ProductBFactory(AbstractFactory):
    # Q. addtional_paramが必要なため、ConcreteFactory間で引数が異なってくるけど良いの？
    # A. 分からん。 完璧な抽象化でなくなるとは言え、Factory生成後はしっかりAbstractFactoryを引き継いでるから良いはず。
    #    ちなみに、sample.diagram.pyでは、default変数を駆使することで対処していた。この場合は、ConcreteFactoryが等しいのでabstractmethodにしていた。
    #    でも今回のように明らかに外から、値を渡さないといけない時は使えないし。。。
    # ★ ちょっとpandasのDataFrameとSeriseを見てみる。(もしかしたら、同じConcreteProductかも？)
    def __init__(self, addtional_param):
        self.addtional_param = addtional_param

    def create_product(self):
        return ProductB(self._generate_name(),
                        self._generate_id(),
                        self.additional_param)
