# weaponに関しては、Charaterに対してhas-aの関係つまり、コンストラクタで持たせることによって
# weaponとCharacterを分離している。これにより、weaponごとに好きな実装ができる&
# Characterは、weaponを使用するだけで良いというカプセル化ができる。
from abc import ABC, abstractmethod
import random


class Character(ABC):
    def __init__(self, name, hp, weapon):
        self.name = name
        self.hp = hp
        self.weapon = weapon

    def attack(self):
        return self.weapon.attack()

    def set_weapon(self, weapon):
        self.weapon = weapon


class Wizard(Character):
    def __init__(self, name, hp):
        weapon = Wand(lv=1)
        super().__init__(name, hp, weapon)


class Knight(Character):
    def __init__(self, name, hp):
        weapon = Sword(lv=1)
        super().__init__(name, hp, weapon)


class Weapon(ABC):
    # base_attack関連を、具象クラスごとに固定値にしたい。。。
    # つまり、Sword等のinitを書き換えずに、base_attackをoverrideして、
    # Sword(lv)とするだけで、定義したい。。。
    def __init__(self, lv, base_attack_point, base_m_attack_point):
        self.lv = lv
        self.attack_point = lv * base_attack_point
        self.m_attack_point = lv * base_m_attack_point

    @abstractmethod
    def attack(self):
        pass


class Sword(Weapon):
    def __init__(self, lv):
        super().__init__(lv, base_attack_point=5, base_m_attack_point=1)

    def attack(self):
        std_attack_point = int(self.attack_point/5)
        return self.attack_point + random.randint(-std_attack_point, std_attack_point)


class Wand(Weapon):
    def __init__(self, lv):
        super().__init__(lv, base_attack_point=1, base_m_attack_point=5)

    def attack(self):
        std_m_attack_point = int(self.m_attack_point/5)
        return self.m_attack_point + random.randint(-std_m_attack_point, std_m_attack_point)
