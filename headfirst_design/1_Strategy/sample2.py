# p18の実装

from abc import abstractmethod


class BaseDuck:
    def __init__(self, fly_behavior, quack_behavior):
        self.fly_behavior = fly_behavior
        self.quack_behavior = quack_behavior

    def fly(self):
        # 自分のクラスで行わずに委譲(has-a)している！
        # 持たせるclassを変えることで、自由に処理を変えることができる。
        self.fly_behavior.fly()

    def quack(self):
        self.quack_behavior.quack()

    @abstractmethod
    def display(self):
        pass


class MallardDuck(BaseDuck):
    def __init__(self):
        # ここに具象クラスがベタがきになるのは仕方ないのかな？？？
        # 気になるようなら、factoryを使いましょう
        super().__init__(FlyBehavior(), QuackBehavior())

    def display(self):
        print("Engilsh: MallardDuck\n日本語:マガモ")


class RubberDuck(BaseDuck):
    def __init__(self):
        # おもちゃのアヒルは飛ばない必要があるが、委譲するクラスを変えるだけで良い！！
        # 変更に対して、既存クラスは変更せず、新クラスの追加だけで良い→開放閉鎖の原則に！
        super().__init__(NoFlyBehavior(), SqueackBehavior())

    def display(self):
        print("Engilsh: RubberDuck\n日本語:おもちゃのアヒル")


class BaseFlyBehavior:
    @abstractmethod
    def fly(self):
        pass


class FlyBehavior(BaseFlyBehavior):
    def fly(self):
        print("飛んでいます！！")


class NoFlyBehavior(BaseFlyBehavior):
    def fly(self):
        print("飛べません！！")


class BaseQuackBehavior:
    @abstractmethod
    def quack(self):
        pass


class QuackBehavior(BaseQuackBehavior):
    def quack(self):
        print("ガーガー")


class SqueackBehavior(BaseQuackBehavior):
    def quack(self):
        print("キューキュー")


def test_duck_class(duck):
    # 抽象クラスにのみプログラミングしてるので、BaseDuckクラスであれば、なんでも受け入れられる！！
    duck.display()
    duck.fly()
    duck.quack()


if __name__ == "__main__":
    mallerd_duck = MallardDuck()
    rubber_duck = RubberDuck()
    test_duck_class(mallerd_duck)
    test_duck_class(rubber_duck)
