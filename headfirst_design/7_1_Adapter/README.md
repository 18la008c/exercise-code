# adapter
- Target: Duck
- Adaptee: Turkey

# 継承を用いたadapter (クラスアダプター)
- Target(抽象クラスと)と、Adaptee(具象クラス)の2つを継承した、adapterクラスを作る。
```
class TurkeyAdapter(WildTurkey, BaseDuck):
    pass
```
- メリット
    - 継承なので、継承元のクラスをそのまま使える。
    - つまり全部wrapしなくて良いので、具象クラスに新たなmethodが追加されてもスルーできる。
    - Targetに必要なmethodだけ、adapter(オーバーライドすれば良い)
- デメリット
    - Adapteeとして受け取るのは具象クラスなので、特定のクラス(WildTurkey)に特化した実装となる。
    - 同じ名前のmethodを持つが、adapter内で振る舞いを変える場合に、継承で勝手にオーバーライドが走って面倒(flyのように)


# 委譲を用いたadapter (オブジェクトアダプター)
- Targetクラスのみ継承する。これによりAdapterクラスはTargetクラスとして扱える。
- Adapteeは、initでcompositionして渡す。(has-aの関係)
- Targetクラスに必要なmethodをwrapする。
```
class TurkeyAdapter(BaseDuck):

    def __init__(self, turkey):
        self.turkey = turkey
```
- メリット
    - has-aの関係なので、持たせるadapteeを変える(つまり、adapteeの具象クラスなんでもOK)になる。

- デメリット
    - Targetクラスに必要なmethod全てをwrapして実装しなければならない。
    - 持たせるadapteeを変えることができるとはいえ！！！、具象クラス固有のmethodが出てきた場合は対応できなくなる。 → 新たなadapterクラス(具象adapter)を作る必要が出てくる。


# つまりどっちを使うの？
- 継承adapter: 作るの簡単 but 特化型(汎用性がない)
- 委譲adapter: 作るの大変 but 汎用型
- Adapteeが1つしかない or Adapteeが複数あるがどれも性質が大きく異なる → 継承adapter
    - 継承で特化したadapterを沢山作ることになるので、作るの簡単な継承が良い
- Adapteeが複数あり、性質が似ている → 委譲
    - 汎用型のadapterを1つ作り、has-aの形で持たせるadapteeを変える方が良い