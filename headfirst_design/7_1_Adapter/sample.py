# p240の実装
# Adapterクラスを使用しwrapすれば、別のクラスに見せかけれる。
# 更にclient(指示出す側。今回はtest_duck)は、adapterの存在を知らなくて良い。
#

from abc import abstractmethod


class BaseDuck:
    @abstractmethod
    def quack():
        pass

    @abstractmethod
    def fly():
        pass


class MallerdDuck(BaseDuck):
    def quack(self):
        print("ガーガー")

    def fly(self):
        print("飛んでいます")


class BaseTurkey:
    @abstractmethod
    def gobble():
        pass

    @abstractmethod
    def fly():
        pass


class WildTurkey(BaseTurkey):
    def gobble(self):
        print("ゴロゴロ")

    def fly(self):
        print("短い距離を飛んでいます")


class TurkeyAdapter(BaseDuck):
    # turkey classもDuckクラスと同様に扱いたい
    # そこで、turkey classのwrapperとしてAdapaterを用意する。
    # これはBaseDuckを継承しているので、Duckクラスである。

    def __init__(self, turkey):
        self.turkey = turkey

    def quack(self):
        self.turkey.gobble()

    def fly(self):
        for n in range(5):
            self.turkey.fly()


def test_duck(duck_instance):
    try:
        duck_instance.quack()
        duck_instance.fly()

    except Exception as e:
        print(e)
        print("[ERROR] It is not duck instance.")


if __name__ == "__main__":
    m_duck = MallerdDuck()
    w_turkey = WildTurkey()
    turkey_adapater = TurkeyAdapter(w_turkey)

    print("---\nTest mellard duck")
    test_duck(m_duck)

    # turkeyクラスは、duckクラスではないので失敗する。
    print("---\nTest turkey instance")
    test_duck(w_turkey)

    # adapterを経由することで、turkeyはduckクラス扱いになる
    print("---\nTest turkey adapter")
    test_duck(turkey_adapater)
