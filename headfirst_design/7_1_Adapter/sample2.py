# p240の実装
# Adapterクラスを使用しwrapすれば、別のクラスに見せかけれる。
# 更にclient(指示出す側。今回はtest_duck)は、adapterの存在を知らなくて良い。
# ---継承を使用した場合----
# 既存のクラスを拡張して、新たに付け足すイメージ(まんま継承)


from abc import abstractmethod


class BaseDuck:
    @abstractmethod
    def quack(self):
        pass

    @abstractmethod
    def fly(self):
        pass


class MallerdDuck(BaseDuck):
    def quack(self):
        print("ガーガー")

    def fly(self):
        print("飛んでいます")


class BaseTurkey:
    @abstractmethod
    def gobble(self):
        pass

    @abstractmethod
    def fly(self):
        pass


class WildTurkey(BaseTurkey):
    def gobble(self):
        print("ゴロゴロ")

    def fly(self):
        print("短い距離を飛んでいます")

    def display(self):
        print("鴨です。")


class TurkeyAdapter(WildTurkey, BaseDuck):
    # turkey classもDuckクラスと同様に扱いたい
    # そこで、BaseTurkeyとWildTurkeyを両方継承する。
    # メリット
    # Duckクラス分はオーバーライドする必要があるが、WildTrukeyのmethodはそのまま使える。
    # デメリット
    # 基本的に、具象クラスを継承するので、ある具象クラスにのみ使えるadapterとなる。

    def quack(self):
        self.gobble()

    def fly(self):
        # 多重継承のため、かなり複雑になる。
        # 1. BaseDuckにもfly methodが存在
        # 2. WildTurkeyにも flyが存在 ← adapteeであるこれを呼び出して、更に本クラス(adapter)でwrapしたい
        # 3. BaseTurkeyにも flyが存在
        # super(class_name, self).method_name多重継承中でも、任意のclass_nameのmethodを呼び出せる
        # が！！！、 super(WildTurkey, self).fly()だと、BaseTurkeyのflyが呼び出されてしまう。。。
        # なので今回は多重継承時には、一番左に書いたclassが優先される仕組みを使っている。

        for n in range(5):
            super().fly()


def test_duck(duck_instance):
    try:
        duck_instance.quack()
        duck_instance.fly()

    except Exception as e:
        print(e)
        print("[ERROR] It is not duck instance.")


if __name__ == "__main__":
    m_duck = MallerdDuck()
    turkey_adapater = TurkeyAdapter()

    print("---\nTest mellard duck")
    test_duck(m_duck)

    # adapterを経由することで、turkeyはduckクラス扱いになる
    print("---\nTest turkey adapter")
    test_duck(turkey_adapater)

    # 更に、adpaterは、WildTurkeyを継承しているので、wrapしてない他のmethodもつかえる。
    turkey_adapater.display()
