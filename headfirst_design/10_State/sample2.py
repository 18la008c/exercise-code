# p401
# sample.pyの改善ver
# - State Patternの適用
# - これにより、変化しやすいState毎の作業が、具象クラスに委譲できるようになった。
# - bugがあるので、実行自体はsample3.pyを使用すること。コード比較用

from abc import abstractmethod


class BaseState:
    def __init__(self, gumball_machine):
        # stateは、clientをhas-aして、　clientはstateをhas-aするため、相互に依存関係ができてしまう。。。
        # いいの？ お互いがお互いを知らないといけない状態に　
        self.gumball_machine = gumball_machine

    @abstractmethod
    def insert_quarter(self):
        pass

    @abstractmethod
    def eject_quarter(self):
        pass

    @abstractmethod
    def turn_crank(self):
        pass


class NoQuarterState(BaseState):
    def insert_quarter(self):
        print("お金を入れました")
        # client(gumball_machine)のmethodを呼んでいるので、相互に依存関係が出てしまってるよ？？
        # またState遷移の関係上、具象Stateクラス同士も結び付きができちゃうよ？？
        self.gumball_machine.set_state(self.gumball_machine.has_quarter_state)

    def eject_quarter(self):
        print("お金を入れてないよ？")

    def turn_crank(self):
        print("お金を入れてないよ？")


class HasQuarterState(BaseState):
    def insert_quarter(self):
        print("既にお金が入っているよ")

    def eject_quarter(self):
        print("お金を返却しました。")
        self.gumball_machine.set_state(self.gumball_machine.no_quarter_state)

    def turn_crank(self):
        print("クランクを回しました。")
        self.gumball_machine.set_state(self.gumball_machine.sold_state)


class SoldState(BaseState):
    def insert_quarter(self):
        print("ガムボールを出している最中です。。。")

    def eject_quarter(self):
        print("ガムボールを出している最中です。。。")

    def turn_crank(self):
        print("ガムボールを出している最中です。2回は回せないよ？")

    def dispense(self):
        self.gumball_machine.count -= 1
        print("ガムボールが出ましたよ！！")

        if self.gumball_machine.count == 0:
            print("売り切れになりました。")
            self.gumball_machine.set_state(
                self.gumball_machine.sold_out_status
            )
        else:
            self.gumball_machine.set_state(
                self.gumball_machine.no_quarter_state
            )


class SoldOutState(BaseState):
    def insert_quarter(self):
        print("売り切れです。ごめんね")

    def eject_quarter(self):
        print("お金を入れてないよ？")

    def turn_crank(self):
        print("お金を入れてないよ？")


class GumballMachine:
    def __init__(self, count):
        self.sold_out_state = SoldOutState(self)
        self.no_quarter_state = NoQuarterState(self)
        self.has_quarter_state = HasQuarterState(self)
        self.sold_state = SoldState(self)
        self.count = count
        self.state = self.sold_out_state \
            if self.count == 0 else self.no_quarter_state

    def insert_quarter(self):
        # 状態ごとの処理を全て委譲することで、client側は処理内容を知らなくて良い
        # けども具象State同士には、遷移の関係上つながりを知らないといけないので、\
        #   新規Stateが追加されると、既存Stateにも変更を入れる必要ある。 → 開放閉鎖ではないのでは？？
        self.state.insert_quarter()

    def eject_quarter(self):
        self.state.eject_quarter()

    def turn_crank(self):
        self.state.turn_crank()
        self.state.dispense()

    def set_state(self, state):
        # 多分setterを使った方が良い
        self.state = state
