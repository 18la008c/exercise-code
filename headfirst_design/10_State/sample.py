# p390
# 悪い例
# - Stateを使わずifだけで状態を判断する場合
# - それぞれのmethodが、stateによって大きく変わるため、stateの変更・追加に対して閉じていない。
# 改善予定
# - 変化する部分をカプセル化 & 抽象クラスにのみプログラミングを行う。
# - 今回はstateにより変化する部分が多いため、stateごとにclassを作る。

class GumballMachine:
    STATE_PATTERN = ("SOLD_OUT", "NO_QUARTER", "HAS_QUARTER", "SOLD")

    def __init__(self, count=0):
        self.count = count
        self.state = "SOLD_OUT" if self.count == 0 else "NO_QUARTER"

    def insert_quarter(self):
        if self.state == "HAS_QUARTER":
            print("既にお金が入っているよ")
        elif self.state == "NO_QUARTER":
            print("お金を入れました")
        elif self.state == "SOLD_OUT":
            print("売り切れです。ごめんね")
        elif self.state == "SOLD":
            print("お待ちください。既にガムボールを出しています。。。")
        else:
            raise ValueError(f"Unknown Status: {self.state}")

    def eject_quarter(self):
        # お金取り出して、状態遷移する。
        pass

    def turn_crank(self):
        # お金が入っている(HAS_QUARTER)ならば、dispenseを呼ぶ
        pass

    def dispense(self):
        if self.state == "SOLD":
            print("ガムボールが出てきたよ")
            self.count -= 1
            if self.count == 0:
                print("売り切れになりました。")
                self.state == "SOLD_OUT"
            else:
                self.state == "NO_QUARTER"
        else:
            raise ValueError(
                f"Dispense required state is SOLD, but now state is {self.state}")
