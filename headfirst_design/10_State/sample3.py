# p401
# sample2.pyに追加stateを与えたver
# - 10%で当たり状態になる。
# - しかし前回懸念していたように、状態遷移があるため、WinnerStateの変更がHasQuarterStateにも影響を与えてる。
# - 仕方ないことなの？


from abc import abstractmethod
from random import randint


class BaseState:
    def __init__(self, gumball_machine):
        # stateは、clientをhas-aして、　clientはstateをhas-aするため、相互に依存関係ができてしまう。。。
        # いいの？ お互いがお互いを知らないといけない状態に　
        self.gumball_machine = gumball_machine

    @abstractmethod
    def insert_quarter(self):
        pass

    @abstractmethod
    def eject_quarter(self):
        pass

    @abstractmethod
    def turn_crank(self):
        pass

    @abstractmethod
    def dispense(self):
        pass


class NoQuarterState(BaseState):
    def insert_quarter(self):
        print("お金を入れました")
        # client(gumball_machine)のmethodを呼んでいるので、相互に依存関係が出てしまってるよ？？
        # またState遷移の関係上、具象Stateクラス同士も結び付きができちゃうよ？？
        self.gumball_machine.set_state(self.gumball_machine.has_quarter_state)

    def eject_quarter(self):
        print("お金を入れてないよ？")

    def turn_crank(self):
        print("お金を入れてないよ？")

    def dispense(self):
        raise ValueError(f"現在の状態{self.gumball_machine.state}では実行できません。")


class HasQuarterState(BaseState):
    def insert_quarter(self):
        print("既にお金が入っているよ")

    def eject_quarter(self):
        print("お金を返却しました。")
        self.gumball_machine.set_state(self.gumball_machine.no_quarter_state)

    def turn_crank(self):
        print("クランクを回しました。")
        if randint(0, 10) == 10:
            # 当たり状態へ
            self.gumball_machine.set_state(self.gumball_machine.winner_state)
        else:
            self.gumball_machine.set_state(self.gumball_machine.sold_state)

    def dispense(self):
        raise ValueError(f"現在の状態{self.gumball_machine.state}では実行できません。")


class SoldState(BaseState):
    def insert_quarter(self):
        print("ガムボールを出している最中です。。。")

    def eject_quarter(self):
        print("ガムボールを出している最中です。。ß。")

    def turn_crank(self):
        print("ガムボールを出している最中です。2回は回せないよ？")

    def dispense(self):
        self.gumball_machine.count -= 1
        print("ガムボールが出ましたよ！！")

        if self.gumball_machine.count == 0:
            print("売り切れになりました。")
            self.gumball_machine.set_state(
                self.gumball_machine.sold_out_state
            )
        else:
            self.gumball_machine.set_state(
                self.gumball_machine.no_quarter_state
            )


class SoldOutState(BaseState):
    def insert_quarter(self):
        print("売り切れです。ごめんね")

    def eject_quarter(self):
        print("お金を入れてないよ？")

    def turn_crank(self):
        print("お金を入れてないよ？")

    def dispense(self):
        raise ValueError(f"現在の状態{self.gumball_machine.state}では実行できません。")


class WinnerState(SoldState):
    # SoldStateを基本的に流用する。
    def dispense(self):
        self.gumball_machine.count -= 1
        print("ガムボールが出ましたよ！！")

        # ガムボールが売り切れなら、内部的には当たり状態だけども隠す。
        # これによって在庫数によらずに当たり状態になっても良くなる。
        if self.gumball_machine.count == 0:
            self.gumball_machine.set_state(
                self.gumball_machine.sold_out_state
            )
            return
        print("大当たり！！！もう1個ガムボールが出てくるよ！")
        super().dispense()


class GumballMachine:
    def __init__(self, count):
        self.sold_out_state = SoldOutState(self)
        self.no_quarter_state = NoQuarterState(self)
        self.has_quarter_state = HasQuarterState(self)
        self.sold_state = SoldState(self)
        self.winner_state = WinnerState(self)
        self.count = count
        self.state = self.sold_out_state \
            if self.count == 0 else self.no_quarter_state

    def insert_quarter(self):
        # 状態ごとの処理を全て委譲することで、client側は処理内容を知らなくて良い
        # けども具象State同士には、遷移の関係上つながりを知らないといけないので、\
        #   新規Stateが追加されると、既存Stateにも変更を入れる必要ある。 → 開放閉鎖ではないのでは？？
        self.state.insert_quarter()

    def eject_quarter(self):
        self.state.eject_quarter()

    def turn_crank(self):
        self.state.turn_crank()
        self.state.dispense()

    def set_state(self, state):
        # 多分setterを使った方が良い
        self.state = state


if __name__ == "__main__":
    gum_machine = GumballMachine(10)

    for n in range(11):
        try:
            gum_machine.insert_quarter()
            gum_machine.turn_crank()
        except ValueError as e:
            print(e)
