# commandの実装のみ

from abc import abstractmethod
from receiver import Light, CeilingFan


class Command:
    # 実行に関する命令をexecute()を押すだけとカプセル化する。
    @abstractmethod
    def execute(self):
        pass


class NoCommand(Command):
    # 何もしない空コマンド　 headfirst p214
    def execute(self):
        pass


class LightOnCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.on()


class LightOffCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.off()


class CeilingFanOnCommand(Command):
    def __init__(self, fan):
        self.fan = fan

    def execute(self):
        self.fan.on()


class CeilingFanOffCommand(Command):
    def __init__(self, fan):
        self.fan = fan

    def execute(self):
        self.fan.off()


class RemoteControl:
    # invoker
    # こいつは、具体的な命令対象は一切知らずに、execute()を押すだけ。
    def __init__(self):
        self.on_commands = [NoCommand()] * 7
        self.off_commands = [NoCommand()] * 7

    def set_command(self, slot, on_command, off_command):
        self.on_commands[slot] = on_command
        self.off_commands[slot] = off_command

    def turn_on(self, slot):
        # ConcreteCommandに関しては何も知らないが、execute()を押すだけ。
        self.on_commands[slot].execute()

    def turn_off(self, slot):
        self.off_commands[slot].execute()

    def show_registerd_commands(self):
        print("----Remote----")
        print("[ON]")
        for n, c in enumerate(self.on_commands):
            print(f"  [slot{n}] {c.__class__.__name__}")
        print("[OFF]")
        for n, c in enumerate(self.off_commands):
            print(f"  [slot{n}] {c.__class__.__name__}")


if __name__ == "__main__":
    remote = RemoteControl()

    living_light = Light("living room")
    kitchen_light = Light("kitchen")
    living_fan = CeilingFan("living room")

    remote.set_command(
        0,
        LightOnCommand(living_light),
        LightOffCommand(living_light)
    )
    remote.set_command(
        1,
        CeilingFanOnCommand(living_fan),
        CeilingFanOffCommand(living_fan)
    )
    remote.set_command(
        2,
        LightOnCommand(kitchen_light),
        LightOffCommand(kitchen_light)
    )

    remote.show_registerd_commands()
    remote.turn_on(0)
    remote.turn_on(1)
    remote.turn_on(1)
    remote.turn_on(1)
    remote.turn_on(2)
    remote.turn_off(0)
    remote.turn_off(1)
    remote.turn_off(2)
    print("Show NoCommand")
    remote.turn_on(5)
