class Light:
    # receiver
    # 実際に実行される対象
    def __init__(self, name):
        self.name = name

    def on(self):
        print(f"{self.name}: Turn on the light")

    def off(self):
        print(f"{self.name}: Turn off the light")


class CeilingFan:

    def __init__(self, name):
        self.name = name
        self.power = "off"

    def on(self):
        if self.power == "off":
            self.power = "low"
            print(f"{self.name}: Turn on the fan. Power is {self.power}")
            return

        elif self.power == "low":
            self.power = "mid"

        elif self.power == "mid":
            self.power = "high"

        else:
            self.power = "low"

        print(f"{self.name}: Change fan power level at {self.power}")

    def off(self):
        self.power = "off"
        print(f"{self.name}: Turn off the fan")
