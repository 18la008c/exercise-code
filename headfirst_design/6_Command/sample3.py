# add macro command
from abc import abstractmethod
from receiver import Light, CeilingFan


class Command:
    # 実行に関する命令をexecute()を押すだけとカプセル化する。
    @abstractmethod
    def execute(self):
        pass

    @abstractmethod
    def undo(self):
        pass


class NoCommand(Command):
    # 何もしない空コマンド　 headfirst p214
    def execute(self):
        pass

    def undo(self):
        pass


class LightOnCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.on()

    def undo(self):
        self.light.off()


class LightOffCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.off()

    def undo(self):
        self.light.on()


class CeilingFanOnCommand(Command):
    def __init__(self, fan):
        self.fan = fan
        self.prev_power = None

    def execute(self):
        self.prev_power = self.fan.power
        self.fan.on()

    def undo(self):
        if self.prev_power == "off":
            self.fan.off()
        else:
            for n in range(2):  # low→mid→high→low→...
                self.fan.on()


class CeilingFanOffCommand(Command):
    def __init__(self, fan):
        self.fan = fan
        self.prev_power = None

    def execute(self):
        self.prev_power = self.fan.power
        self.fan.off()

    def undo(self):
        # off→low→mid→high
        if self.prev_power == "low":
            self.fan.on()
        elif self.prev_power == "mid":
            for n in range(2):
                self.fan.on()
        elif self.prev_power == "high":
            for n in range(3):
                self.fan.on()


class RemoteControl:
    # invoker
    # こいつは、具体的な命令対象は一切知らずに、execute()を押すだけ。
    def __init__(self):
        self.on_commands = [NoCommand()] * 7
        self.off_commands = [NoCommand()] * 7
        self.undo_command = NoCommand()

    def set_command(self, slot, on_command, off_command):
        self.on_commands[slot] = on_command
        self.off_commands[slot] = off_command

    def turn_on(self, slot):
        # ConcreteCommandに関しては何も知らないが、execute()を押すだけ。
        self.on_commands[slot].execute()
        self.undo_command = self.on_commands[slot]

    def turn_off(self, slot):
        self.off_commands[slot].execute()
        self.undo_command = self.off_commands[slot]

    def undo(self):
        self.undo_command.undo()

    def show_registerd_commands(self):
        print("----Remote----")
        print("[ON]")
        for n, c in enumerate(self.on_commands):
            print(f"  [slot{n}] {c.__class__.__name__}")
        print("[OFF]")
        for n, c in enumerate(self.off_commands):
            print(f"  [slot{n}] {c.__class__.__name__}")


class MacroCommand(Command):
    # 抽象クラス(Command)を引き継いだまま、マクロという大幅に変更したクラスも作れる
    def __init__(self, commands):
        self.commands = commands

    def execute(self):
        for cmd in self.commands:
            cmd.execute()

    def undo(self):
        for cmd in self.commands[::-1]:
            cmd.undo()


if __name__ == "__main__":
    remote = RemoteControl()

    living_light = Light("living room")
    kitchen_light = Light("kitchen")
    living_fan = CeilingFan("living room")

    on_macro_command = MacroCommand([
        LightOnCommand(living_light),
        CeilingFanOnCommand(living_fan),
        LightOnCommand(kitchen_light)
    ])

    off_macro_command = MacroCommand([
        LightOffCommand(living_light),
        CeilingFanOffCommand(living_fan),
        LightOffCommand(kitchen_light)
    ])

    # 抽象クラスにのみプログラミングしているので、MacroCommandでも問題なく動く
    remote.set_command(
        0,
        on_macro_command,
        off_macro_command
    )

    remote.show_registerd_commands()
    print("---macro-on---")
    remote.turn_on(0)
    print("---macro-of---")
    remote.turn_off(0)
    print("--macro-undo")
    remote.undo()
