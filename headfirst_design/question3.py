# factoryで作ったproduct & 他のclassに共通するclassを持たせたい。
# でも依存関係が多すぎる問題になる。
# client, Factory, Productの全てで使いたい。

# あるクラスで作成した、属性を他のものでも使用したい。
# ex.) Service Instance
# class VCMangerの_connectによって生成される siを、TaskControllerで使いたいし、 TaskControllerもVCManagerで使いたい。
# これは、継承すれば良いのでは？


# 抽象クラスには2pattern存在する。
# [baseとしての抽象クラス]
#   - 共通method, attributeを集めて、具象クラスで使う。
#   → 抽象クラスは「使われる側」で、表に出てくることはない。
# [interfaceとしての抽象クラス]
#   - 抽象クラスに対してのみ、プログラミングすることでそのコードはポリモーフィズム(開放閉塞の原則)を守れるようになる。
#   → 抽象クラスは「使う側」で、常に表に出てくる。(むしろ具象クラスのmethodが表に出てはいけない！！)

# 上記の両方を使いたい場合はどうするべきなのだろうか？？？
# 例えば、
#   - service instanceを使いたいというbaseとしての抽象クラス
#   - ポリモーフィズムを使いたいという、interfaceとしての抽象クラス
# 具体的だと、
#   - vCenterの操作関連をまとめる(base)と、さまざまなVMを作る(interface)の共存の場合
# class BaseVMFacotry(BaseVC)とかすれば良いか？？？


# それとも、baseとしての抽象クラスは使うべきではない？ or compositionで渡すべき？
# [compositionで渡す？]
#   - compositionで渡すのも良いが、持ち替える必要性がない。
#   - なぜなら、共通作業をまとめたclassであるから。
# [baseとしての抽象クラス]
#   - そもそも抽象クラスは、interfaceとしての役割がほぼであり、共通methodをまとめるべきではない？？？(特に外に出ないようなmethod集)
#   - そんなことは無いはず！！！ 　そうすると、strategyとか作れないのでは？？ (MallerdDuckのやつ)

xxx = []


class BaseVC:
    def __init__(self, user, pwd):
        self.si = self._connect(user, pwd)


class VCManager(BaseVC):
    def __init__(self, user, pwd):
        super().__init__(user, pwd)
        self.all_cluster = self._get_all_cluster()

    def _get_all_cluster(self):
        return {
            cluster.name: cluster
            for cluster in xxx
        }

    def get_cluster_obj(self, cluster_name):
        return self.all_cluster[cluster_name]

    def get_cluster_id(self, cluster_name):
        return self.all_cluster[cluster_name]._moid


class TaskController(BaseVC):
    def wait_for_task(task_obj):
        # wait to finish task until timeout
        try:
            pass
        finally:
            pass


def test_alarm(factory):
    test_vm = factory.create()
