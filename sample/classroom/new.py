# 未完成。。。
from dataclasses import dataclass

schedule = {
    "personA": {
        "1": "math",
        "2": "bio",
        "3": "english",
        "4": "social"
    },
    "personB": {
        "1": "english",
        "2": "bio",
        "3": "math",
        "4": "social"
    },
    "personC": {
        "1": "social",
        "2": "bio",
        "3": "english",
        "4": "math"
    },
}

status = {
    "personA": {
        "grade": "B3"
    },
    "personB": {
        "grade": "B1"
    },
    "personC": {
        "grade": "M1"
    }
}

classroom = {
    "math": {
        "status": "absent",
        "place": "RoomA",
        "level": "1F"
    },
    "bio": {
        "status": "normal",
        "place": "RoomB",
        "level": "2F"
    },
    "english": {
        "status": "normal",
        "place": "RoomC",
        "level": "3F"
    },
    "social": {
        "status": "late",
        "place": "RoomD",
        "level": "3F"
    }
}


class Student:
    def __init__(self, name, school):
        self.name = name
        self.school = school
        self.grade = status[name]["grade"]
        self.schedule = schedule[name]

    def next_to_do(self):
        count = self.school.count
        if len(self.schedule) < count:
            print("下校の時間です。")
            return

        subject = self.schedule[str(count)]
        classroom = self.school.search_by_subject(subject)
        print(
            "次の授業は、{}です。 場所は{}の{}です。".format(
                classroom.subject,
                classroom.level,
                classroom.place
            )
        )
        return

    def attend_classroom(self):
        count = self.school.count
        subject = self.schedule[str(count)]
        classroom = self.school.search_by_subject(subject)
        classroom.teacher.add_student(self)


class MasterStudent(Student):
    def next_to_do(self):
        print("アンケート結果を回収して、事務局まで届ける")
        return super().next_to_do()


class Counter:
    def __init__(self, start: int = 0):
        self.__count = 0

    @ property
    def count(self):
        return self.__count

    def up(self):
        self.__count += 1

    def clear(self):
        self.__count = 0


class Teacher:
    def __init__(self, school, subject: str) -> None:
        self.school = school
        self.subject = subject
        self.__students = []

    def add_student(self, student: Student):
        self.__students.append(student)

    def call_finish(self):
        for student in self.__students:
            student.next_to_do()

        for student in self.__students:
            student.attend_classroom()


@ dataclass(frozen=True)
class Classroom:
    subject: str
    status: str
    place: str
    level: str
    teacher: Teacher


class School:
    def __init__(self):
        self.__counter = Counter()
        self._register_students()
        self._register_classrooms()
        self.call_finish()
        for student in self.__students:
            student.next_to_do()
            student.attend_classroom()

    @ property
    def count(self):
        return self.__counter.count

    def _register_students(self):
        self.__students = [Student(name, self) for name in schedule]

    def _register_classrooms(self):
        self.__classrooms = [
            Classroom(
                subject=key,
                teacher=Teacher(subject=key, school=self),
                **value
            )
            for key, value in classroom.items()
        ]

    def call_finish(self):
        print(f"{self.count}時限目の授業が終わりました。")
        self.__counter.up()
        for classroom in self.__classrooms:
            classroom.teacher.call_finish()

    def search_by_subject(self, subject: str):
        for classroom in self.__classrooms:
            if classroom.subject == subject:
                return classroom


if __name__ == "__main__":
    school = School()
    school.call_finish()
    school.call_finish()
    school.call_finish()
