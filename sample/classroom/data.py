from service import Student, MasterStudent, Classroom  # 循環参照になるのでは？？？


class ScheduleRepository:
    DATA = {
        "personA": {
            "1": "math",
            "2": "bio",
            "3": "english",
            "4": "social"
        },
        "personB": {
            "1": "english",
            "2": "bio",
            "3": "math",
            "4": "social"
        },
        "personC": {
            "1": "social",
            "2": "bio",
            "3": "english",
            "4": "math"
        },
    }

    @classmethod
    def get_by_name(cls, name: str):
        return cls.DATA[name]


class StudentStatusRepository:
    DATA = {
        "personA": {
            "grade": "B3"
        },
        "personB": {
            "grade": "B1"
        },
        "personC": {
            "grade": "M1"
        }
    }

    @classmethod
    def get_grade_by_name(cls, name: str):
        return cls.DATA[name]

    @classmethod
    def get_by_name(cls, name: str):
        grade = cls.get_grade_by_name(name)
        schedule = ScheduleRepository.get_by_name(name)
        if "m" in grade:
            return MasterStudent(name, schedule)

        return Student(name, schedule)


class ClassroomRepository:
    DATA = {
        "math": {
            "status": "absent",
            "place": "RoomA",
            "level": "1F"
        },
        "bio": {
            "status": "normal",
            "place": "RoomB",
            "level": "2F"
        },
        "english": {
            "status": "normal",
            "place": "RoomC",
            "level": "3F"
        },
        "social": {
            "status": "late",
            "place": "RoomD",
            "level": "3F"
        }
    }

    @classmethod
    def get_by_subject(cls, subject: str):
        return Classroom(subject, **cls.DATA[subject])
