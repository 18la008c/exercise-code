from dataclasses import dataclass

"""pattern1
domain-objectでは、isの判断するmethodだけ提供
service-classでは、契約を守るように、domain-objectを使用
結果:
    - 使う側が、契約を守るように使用しているのは → OK
    - service-classに、「ビジネスロジック(下校)が入ってる」→ NG
"""


@dataclass(frozen=True)
class Schedule:
    # domain-object
    classrooms: dict

    def is_last_classroom(self, count):
        return len(self.classrooms) < count

    def get_classroom_from_count(self, count):
        return self.classrooms[str(count)]


class Student:
    # service-class
    def __init__(self, schedule):
        self.schedule = schedule

    def next_to_do(self, count):
        if self.schedule.is_last_classroom(count):
            print("下校")

        classroom = self.schedule.get_classroom_from_count(count)
        print(f"次は{classroom.subject}です。")


"""pattern2
domain-objectで、ビジネスロジックを判断
service-classでは使うだけ
結果:
    - domain-objectでビジネスロジック(=下校)を判断 → OK
    - service-classは使うだけ → NG
         - 今回は返り値が、strだからOKになっているが、判断の結果Noneや、例外を投げる場合はどうするの？
"""


@dataclass(frozen=True)
class Schedule:
    # domain-object
    classrooms: dict

    def is_last_classroom(self, count):
        return len(self.classrooms) < count

    def get_classroom_from_count(self, count):
        if self.is_last_classroom(count):
            return "下校"
        return self.classrooms[str(count)]


class Student:
    # service-class
    def __init__(self, schedule):
        self.schedule = schedule

    def next_to_do(self, count):
        classroom = self.schedule.get_classroom_from_count(count)
        print(f"次は{classroom.subject}です。")
