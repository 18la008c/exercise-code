"""
Student:
- 自分の時間割を知っておく必要がある。
- 授業に出席する必要がある。
- 授業が終わったら、次の授業に出席する必要がある。

Teacher:
- 出席している生徒を知っている必要がある。
- 授業の終わりを生徒に伝える必要がある。
- 授業の終わりに気づく必要がある。

Timer:
- 時間を進めて、授業を終わりを知らせる必要がある。

School:
- 全生徒の格納
- 全教師の格納
- 全クラスの格納
- Timerの保持

usecase1:
1. schoolがtimerを進める
2. 全teacherにtimerが進んだことを伝える。
3. teacherは授業を終了し、全生徒に授業の終了を伝える。

usecase2:
1. 生徒は、授業の終了を聞く
2. 生徒は、次の授業を把握する
3. 生徒は、次の教室に移動する。

usecase3:
1. 教師は、出席している生徒を数える
"""

class 
