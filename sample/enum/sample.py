# ref: https://betterprogramming.pub/take-advantage-of-the-enum-class-to-implement-enumerations-in-python-1b65b530e1d
from enum import Enum


class Direction(Enum):
    # self.nameとself.valueが自動で設定される。
    NORTH = 1
    EAST = 2
    SOUTH = 3
    WEST = 4

    # 分岐分けと、実際の処理が1つにまとまった。
    @property
    def angle(self):
        base_angle = 90.0
        return base_angle * (self.value - 1)

    @staticmethod
    def angle_interval(direction0, direction1):
        return abs(direction0.angle - direction1.angle)


if __name__ == "__main__":
    # 単体だけinstance化して、methodを使うこともできる。
    north = Direction.NORTH
    print(f"North Angle is {north.angle} deg")

    # forで回すこともできる。
    for direction in Direction:
        print(f"{direction.name} angle is {direction.angle} deg")

"""
Returns:
    North Angle is 0.0 deg
    NORTH angle is 0.0 deg
    EAST angle is 90.0 deg
    SOUTH angle is 180.0 deg
    WEST angle is 270.0 deg
"""
