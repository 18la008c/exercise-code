# sample2.py
- multiprocessingがprocessを作る方法は3つある。
    - fork: UNIXでのデフォルト。普通にforkをシステムコールする。
    - spawn: mac/winでのデフォルト。forkではなく再度pythonを別で起動して、必要な情報を新pythonに渡す。
    - forkserver: 今回は無視
    - 参考
        - https://docs.python.org/ja/3/library/multiprocessing.html#contexts-and-start-methods
        - htps://itsuka-naritai.com/2021/04/18/multiprocessing-forkとspawnの違いを理解する/
- forkでは何も気にしなくて良い。
- spawnでの面倒事
　　 - 再度pythonを別で起動するため、メインモジュールを再度読み込み実行が入る。
　　 - すると、無限ループに陥る可能性がある。 → その場合はRuntimeError
    ```
    RuntimeError: 
            An attempt has been made to start a new process before the
            current process has finished its bootstrapping phase.

            This probably means that you are not using fork to start your
            child processes and you have forgotten to use the proper idiom
            in the main module:

                if __name__ == '__main__':
                    freeze_support()
    ```
    - この回避には、`if __name__ == '__main__':`を使えば良い。
    ```
    # main.py
    from multiprocessing import Process

    def foo():
        print('hello')

    if __name__ == '__main__':
        p = Process(target=foo) # メインモジュールが再実行されるが、`if __name__ == '__main__':`によりブロックされる。
        p.start()
    ```
    - 参考: https://docs.python.org/ja/3/library/multiprocessing.html#the-spawn-and-forkserver-start-methods

# sample4.py
>OSのファイル書き込みの仕組み上、複数のプロセスが同じファイルにログデータを書き込もうとするとログファイルがぶっ壊れます。
>https://dslab.work/2021/02/23/post-274/
- 従って、process毎のlogを集約して、集約されたlogを1つのプロセスのみが扱う必要がある。
- これがcookbookに書いてある。
    - https://docs.python.org/ja/3/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
- 登場人物
    - listener process: queueに集約されたlogをfileに出力するだけのprocess
        - `listener_configurer()`: listener processに対してloggingの設定を入れ込む
        - `listener_process(queue, configure)`: logをfileに出力し続ける関数(While loop)
    - worker process: 他の作業をするprocess + 出力されたlogをqueueに保存するようにlogging設定しておく
        - `worker_configurer()`: worker processに対してloggingの設定を入れ込む
        - `worker_process(queue, configure, *)`: 他の作業する関数 or methodならなんでも良い
  