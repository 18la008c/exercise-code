# pythonから別processを作ってみた1 multiprocessingではなく、osを使う。
# os.fork → glibc → Cのfork → システムコールforkを呼び出し or os.fork → 直接システムコールfork
# refs: https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#2-multiprocessing

import os

print(f"Main pid is {os.getpid()}")
pid = os.fork()  # この瞬間分裂する。つまり変数pidは、親では子プロセスのpid、子では必ず0になる。

if pid == 0:
    print("I'm Child")
    print(f"Main pid is {os.getpid()}, Child pid is {os.getppid()}")
else:
    print("I'm Parent")
    print(f"Main pid is {os.getpid()}, Child pid is {pid}")

"""
Returns:
    Main pid is 76688
    I'm Parent
    Main pid is 76688, Child pid is 76689
    I'm Child
"""
