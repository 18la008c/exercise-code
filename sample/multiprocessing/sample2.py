# pythonから別processを作ってみた2 ~os.fork()のwrapperとして、multiprocessingを使う
# refs: https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#2-1-プロセスprocess
#     : https://itsuka-naritai.com/2021/04/18/multiprocessing-forkとspawnの違いを理解する
#     : https://docs.python.org/ja/3/library/multiprocessing.html#contexts-and-start-methods

from multiprocessing import Process
import os
import time


def run_subprocess(name):
    time.sleep(2)
    print(f"[Sub : {os.getpid()}] Run process {name}")


if __name__ == "__main__":  # windows/macOSの場合はこれが必須→README参考
    p = Process(
        target=run_subprocess,
        args=("test",)  # iterじゃないといけないらしい
    )
    print(f"[Main: {os.getpid()}] Run start")
    p.start()  # startで起動
    print(f"[Main: {os.getpid()}] Wait subprocess")
    p.join()  # subが終了するまで待機
    print(f"[Main: {os.getpid()}] Finish subprocess")

"""
Returns
    [Main: 77096] Run start
    [Main: 77096] Wait subprocess
    [Sub : 77099] Run process test
    [Main: 77096] Finish subprocess
"""
