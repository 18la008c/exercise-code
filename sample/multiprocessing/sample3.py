# pythonから別processを作ってみた3 ~process間で値の共有~
# refs: https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#2-3-1-キューqueue
#     : https://docs.python.org/ja/3/library/multiprocessing.html#exchanging-objects-between-processes

from multiprocessing import Process, Queue
import os
import time
import random


# Queueにデータを書き込む
def write(q: Queue):
    print(f"[Sub : {os.getpid()}] Write process")
    for value in ['A', 'B', 'C']:
        print(f"[Sub : {os.getpid()}] Put {value} to queue")
        q.put(value)
        time.sleep(random.random())  # 書き込み時間にランダム性を付与


# Queueからデータを読み取り
def read(q: Queue):
    print(f"[Sub : {os.getpid()}] Read process")
    while True:
        value = q.get(True)
        print(f"[Sub : {os.getpid()}] Get {value} from queue")


if __name__ == "__main__":
    q = Queue()
    p_write = Process(target=write, args=(q,))
    p_read = Process(target=read, args=(q,))

    print(f"[Main: {os.getpid()}] Write/Read start")
    p_write.start()
    p_read.start()

    print(f"[Main: {os.getpid()}] Wait subprocess")
    p_write.join()

    print(f"[Main: {os.getpid()}] Kill read subprocess")
    p_read.terminate()

# FIFO(ロケットえんぴつ方式)なので、入れ込むスレッドと読み込むスレッドが非同期であっても、必ず読み込み順番が保たれる。
# つまり、write → read → readという形で、read2連続で挟まる場合は、readが待機してくれる
#       write → write → readという形で、writeが2連続で挟まる場合でも、1回目のwriteが上書きされることはない。(保管しておいてくれる。)
"""
Returns:
    [Main: 77621] Write/Read start
    [Main: 77621] Wait subprocess
    [Sub : 77623] Write process
    [Sub : 77623] Put A to queue
    [Sub : 77624] Read process
    [Sub : 77624] Get A from queue
    [Sub : 77623] Put B to queue
    [Sub : 77624] Get B from queue
    [Sub : 77623] Put C to queue
    [Sub : 77624] Get C from queue
    [Main: 77621] Kill read subprocess
"""
