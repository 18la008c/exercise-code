# Process.mapの使い方
# https://blog.imind.jp/entry/2019/07/20/025317

from multiprocessing import Pool


def test(x: int):
    return x**2


if __name__ == "__main__":
    x = [0, 2] * 5
    x2 = range(10)

    with Pool(2) as p:  # withを使えばpool終了後に自動でcloseしてくれる。 closeしないと処理がずっと継続する可能性あり
        result = p.map(test, x)
        result2 = p.map(test, x2)

    print(result)
    print(result2)

# mapなので、渡した配列の順序がkeepされる!!
"""
Returns:
    [0, 4, 0, 4, 0, 4, 0, 4, 0, 4]
    [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
"""
