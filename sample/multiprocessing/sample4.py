# 複数processでのlogging1
# https://docs.python.org/ja/3/howto/logging-cookbook.html#logging-to-a-single-file-from-multiple-processes
# OSのファイル書き込みの仕組み上、複数のプロセスが同じファイルにログデータを書き込もうとするとログファイルがぶっ壊れます
# https://dslab.work/2021/02/23/post-274/

import logging
import logging.handlers
import multiprocessing

from random import choice, random
import time

# DEMO PARAMETERS
LEVELS = [logging.DEBUG, logging.INFO, logging.WARNING,
          logging.ERROR, logging.CRITICAL]
LOGGERS = ['a.b.c', 'd.e.f']
MESSAGES = [
    'Random message #1',
    'Random message #2',
    'Random message #3',
]


def listener_configurer():
    """log集約process(listener)のlogger設定用"""
    root = logging.getLogger()
    h = logging.handlers.RotatingFileHandler(
        filename='multiprocess_logtest.log',
        mode='a',
        maxBytes=300000,    # 1fileの最大容量
        backupCount=10  # 　保存する世代数
    )
    f = logging.Formatter(
        '%(asctime)s %(processName)-10s %(name)s %(levelname)-8s %(message)s')
    h.setFormatter(f)
    root.addHandler(h)

    # stdoutにも出したいなら、handlerを追加しましょう
    # sh = logging.StreamHandler()
    # sh.setFormatter(f)
    # root.addHandler(sh)


def listener_process(queue, configurer):
    """Queueに格納された、各process(worker)のlogを取り出し、listener内でfileに保存を実施し続ける関数"""
    configurer()
    while True:
        try:
            # queueが空なら自動で待機する。
            # https://docs.python.org/ja/3/library/multiprocessing.html#multiprocessing.Queue.get
            record = queue.get()
            # queueにNoneが入っていたら、listener processは終了！
            if record is None:
                break
            logger = logging.getLogger(record.name)
            logger.handle(record)
        except Exception:
            import sys
            import traceback
            print('Whoops! Problem:', file=sys.stderr)
            traceback.print_exc(file=sys.stderr)


def worker_configurer(queue):
    """worker process毎に出力されたlogをqueueに格納するlogger設定をする関数"""
    h = logging.handlers.QueueHandler(queue)
    root = logging.getLogger()
    # このrootの設定は、process間で引き継がれない？？
    # つまりlistenerではFileHandler, workerでは、QueueHandlerと2つつけているようにも見えるが。。。
    root.addHandler(h)
    root.setLevel(logging.DEBUG)


def worker_process(queue, configurer):
    """ランダムにlogを吐くだけのdemo worker-process"""
    configurer(queue)
    name = multiprocessing.current_process().name
    print('Worker started: %s' % name)
    for i in range(10):
        time.sleep(random())
        logger = logging.getLogger(choice(LOGGERS))
        level = choice(LEVELS)
        message = choice(MESSAGES)
        logger.log(level, message)
    print('Worker finished: %s' % name)


if __name__ == '__main__':
    queue = multiprocessing.Queue(-1)
    # listener用のprocessを作成
    listener = multiprocessing.Process(
        target=listener_process,
        args=(queue, listener_configurer)
    )
    listener.start()

    # demo用のworkerを作成
    workers = []
    for i in range(10):
        # worker用のprocessを起動
        worker = multiprocessing.Process(
            target=worker_process,
            args=(queue, worker_configurer)
        )
        workers.append(worker)
        worker.start()

    # 全workerの動作が終わるのを待機
    for w in workers:
        w.join()

    # queueの空きを考慮せず、即座にitemを追加する。Queueがfullなら、例外が出るが、Queue(-1)で無限キューを作ってるので関係ない
    # Queue.put_nowait(obj) == Queue.put(obj, block=False)
    # またQueueにNoneを入れることで、listener processは終了するように設計している。
    queue.put_nowait(None)

    # listenerの終了を待つ
    listener.join()


# 複数のprocessに渡るlogを集約できている。
"""
Returns:
    2022-04-07 15:29:42,233 Process-4  a.b.c CRITICAL Random message #2
    2022-04-07 15:29:42,372 Process-5  d.e.f WARNING  Random message #3
    2022-04-07 15:29:42,445 Process-8  a.b.c WARNING  Random message #1
    2022-04-07 15:29:42,462 Process-9  d.e.f INFO     Random message #2
    2022-04-07 15:29:42,471 Process-4  d.e.f INFO     Random message #1
"""
