import ipaddress
from dataclasses import dataclass

# 色々問題点
#   1. maint-nwに関しては以下のようなgateway, subnetの情報も必須
#   2. kvm-nwに関しては以下のような情報が必要ない(ipだけで良い)
#   結局どうするべきなのか？？？


class Network:
    # こんな感じにすれば、ipをただのstrではなくipaddrオブジェクトとして扱うようになり、安全性が増える
    #   安全機構1 : ip="aaa", cidr=1111のようなipaddrではないものを弾ける
    #   安全機構2 : gatewayがsubnetの範囲外のものを弾ける
    # ただし問題点もある
    #   問題1: Network.ipの返り値がstringではなくなる。
    #   問題2: setter, getterを使った方がいいのか問題
    def __init__(self, ip, cidr, gateway):
        self.ip = ipaddress.IPv4Address(ip)
        self.network = ipaddress.ip_network(
            address=f"{self.ip}/{cidr}",
            strict=False
        )
        if ipaddress.IPv4Address(gateway) in self.network:
            self.gateway = ipaddress.IPv4Address(gateway)
        else:
            raise ValueError


class Network2:
    # 通常通り格納して、最後にcheckを入れる形式
    # class Networkの問題点を改善したが、これはpythonicな書き方かどうか疑問。。。
    def __init__(self, ip, cidr, gateway):
        self.ip = ip
        self.cidr = cidr
        self.gateway = gateway
        self._is_valid()

    def _is_valid(self):
        ip = ipaddress.IPv4Address(self.ip)
        network = ipaddress.ip_network(
            address=f"{ip}/{self.cidr}",
            strict=False
        )
        if ipaddress.IPv4Address(self.gateway) not in network:
            raise ValueError


@dataclass(frozen=True)
class Network3:
    # dataclassだと__post_init__の特殊methodが存在する。
    ip: str
    cidr: str
    gateway: str

    def __post_init__(self):
        ip = ipaddress.IPv4Address(self.ip)
        network = ipaddress.ip_network(
            address=f"{ip}/{self.cidr}",
            strict=False
        )
        if ipaddress.IPv4Address(self.gateway) not in network:
            raise ValueError


@dataclass(frozen=True)
class Network4:
    # classmethodにすれば、外部からもipが正しいかどうか判定に使えるけど。。。
    # ここまでする必要があるか微妙 & 毎回IPv4Addressを生成していて不毛。。。
    ip: str
    cidr: str
    gateway: str

    def __post__init__(self):
        self.is_valid_ip(self.ip)
        self.is_valid_gateway(
            ip=self.ip,
            cidr=self.cidr,
            gateway=self.gateway
        )

    @classmethod
    def is_valid_ip(cls, ip: str):
        try:
            ipaddress.ip_address(ip)
            return True
        except ValueError:
            return False

    @classmethod
    def is_valid_gateway(cls, ip: str, gateway: str, cidr: str):
        try:
            network = ipaddress.ip_network(
                address=f"{ip}/{cidr}",
                strict=False
            )
            return ipaddress.ip_address(ip) in network
        except ValueError:
            return False
