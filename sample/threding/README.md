# スレッドとは
- 1つのprocessの中で複数の処理を並行して実行する。
- processは同じなので
    - memoryを共有する
    - コアを複数使うわけではないので、高速でcpuが処理を切り替えるだけ
- 参考
    - https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#threadingとmultiprocessing
    - https://webpia.jp/thread_process/#index_id4

# daemon化
- daemon化 = 作成するthreadはsub-threadであることを明示的にする
- これによって、残っているthreadがdaemonのみであればpython自体が終了する。
    - つまり、main-threadが終了したら、sub-threadも自動で終わってくれる。
>スレッドには "デーモンスレッド (daemon thread)" であるというフラグを立てられます。 このフラグには、残っているスレッドがデーモンスレッドだけになった時に Python プログラム全体を終了させるという意味があります。フラグの初期値はスレッドを生成したスレッドから継承します。フラグの値は daemon プロパティまたは daemon コンストラクタ引数を通して設定できます。https://docs.python.org/ja/3/library/threading.html#thread-objects
- ただし、強制終了扱いなので注意
>デーモンスレッドは終了時にいきなり停止されます。デーモンスレッドで使われたリソース (開いているファイル、データベースのトランザクションなど) は適切に解放されないかもしれませんhttps://docs.python.org/ja/3/library/threading.html#thread-objects


# sample.py
- pythonのthreadingはスレッドセーフではない例
- thread内部で作った変数: 独立
- thread外部で作った変数: 共有
- 従ってglobalでthread外部で作った変数を呼んでしまうと、競合が発生する！！
- https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#1-6-スレッド制御