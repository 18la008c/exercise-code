import threading


class MyThread(threading.Thread):
    def __init__(self, name):
        super(MyThread, self).__init__()
        self.n = name

    # runをoverrideして、自作classに組み込むことができる！！！
    def run(self):
        """thread内で実行したい内容"""
        for n in range(10):
            print(f"[{self.name}]Run thread")


if __name__ == "__main__":
    t1 = MyThread("thread1")
    t2 = MyThread("thread2")
    t1.start()
    t2.start()

# thread1,2の実行順はrandam
"""
Return:
    [Thread-1]Run thread
    [Thread-2]Run thread
    [Thread-1]Run thread
    [Thread-1]Run thread
    [Thread-1]Run thread
    [Thread-1]Run thread
    [Thread-1]Run thread
    [Thread-1]Run thread
    [Thread-2]Run thread
"""
