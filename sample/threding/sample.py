# python threadingはスレッドセーフでない例
# https://qiita.com/kaitolucifer/items/e4ace07bd8e112388c75#1-6-スレッド制御
# threadでの変数共有について
# - thread内部で作られた変数は独立
# - thread以前に作られた変数は共有 → とは言えglobalで呼ばれない限り、thread内で変更はできない。
# つまりglobalで呼ばなければ問題ない。


import threading

# 貯金額とする
balance = 0


def change_it(n):
    global balance  # globalにすることで、各スレッド間で値が共有される。
    balance = balance + n  # global balanceに(+n)
    balance = balance - n  # global balanceに(-n)


def run_thread(n):
    for i in range(10000):
        change_it(n)


t1 = threading.Thread(target=run_thread, args=(5,))
t2 = threading.Thread(target=run_thread, args=(8,))
t1.start()
t2.start()
t1.join()
t2.join()
print(balance)

# 毎回計算結果が異なる → スレッドセーフではない！
"""
Return:
    hiroki@MacBook-Pro exercise-code % python sample.py
    5
    hiroki@MacBook-Pro exercise-code % python sample.py
    0
    hiroki@MacBook-Pro exercise-code % python sample.py
    -8
"""
