import pytest
import executer


def test_add_env_vars():
    sh = executer.Shell()
    sh.add_env_var({"TEST_ENV_VAR": "1234321"})
    assert sh.env_vars["TEST_ENV_VAR"] == "1234321"


def test_run():
    sh = executer.Shell()
    res = sh.run("echo 1234321")
    assert res == "1234321\n"  # 改行が入るのね


def test_failed_run():
    sh = executer.Shell()
    with pytest.raises(executer.ShellCmdError):  # errorクラスも明示importって面倒じゃない？
        sh.run("echo 1234321 >&2")
