import logging
import subprocess
import os
import json

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


class ShellCmdError(Exception):
    pass


class Shell:
    def __init__(self):
        self.env_vars = os.environ.copy()

    def add_env_var(self, env_var: dict) -> None:
        self.env_vars.update(env_var)

    def run(self, cmd: str) -> str:
        p = subprocess.run(
            cmd, shell=True, capture_output=True, env=self.env_vars)

        if p.stderr.decode() != "":
            logger.error(f"Failed command: {cmd}\n{p.stderr.decode()}")
            raise ShellCmdError()

        return p.stdout.decode()


class Govc:
    def __init__(self, user, pwd):
        self.sh = Shell()
        env_vars = {
            "GOVC_USERNAME": user,
            "GOVC_PASSWORD": pwd,
            "GOVC_INSECURE": "1"
        }
        self.sh.add_env_var(env_vars)

    def _set_hostname(self, hostname: str):
        self.sh.add_env_var({"GOVC_URL": f"{hostname}/sdk"})

    def esxcli_network_nic_list(self, hostname):
        cmd = "govc host.esxcli -json=1 network nic list"
        self._set_hostname(hostname)
        return json.loads(self.sh.run(cmd))["Values"]


if __name__ == "__main__":
    sh = Shell()
    govc = Govc("aaa", "bbb")
    print(sh.run("df -h"))
    print(govc.esxcli_network_nic_list("test"))
