# p160
from dataclasses import dataclass


@dataclass(frozen=True)
class Amount:
    # ValueObject
    value: int

    def __post_init__(self):
        if self.value <= 0:
            raise ValueError("金額が0未満です。")

    def has(self, amount: Amount) -> bool:
        # 自分自身と同じclass比較したい場合は、typehintのundefineってどう回避するの？
        # pythonなら`__eq__`等の特殊methodを使うべき。わかりやすさ重視で本記載に準拠する。
        return self.value >= amount.value


class BankAccountRepository:
    # 実際のDB操作を、業務ロジックでwrapするクラス
    # こうすることで、サービスクラスではDB操作ではなく、業務ロジックに従って処理を実施している事になる。
    # 証拠として、サービスクラスではDB操作を実施することはなくなる。
    def __init__(self):
        pass

    def can_withdraw(self) -> bool:
        # 実際のDB操作の実施(Read)
        pass

    def balance(self) -> Amount:
        # 実際のDB操作(Read) → ドメインオブジェクトの生成
        pass

    def withdraw(self, amount: Amount) -> None:
        # 実際のDB操作(Update)
        pass


class BankAccountService:
    # NGな例
    # 残高の更新と、残高の引き出しを1つのmethodで行っている。
    def __init__(self, repository):
        self.repository = repository

    def withdraw(self, amount):
        self.repository.withdraw()
        return self.repository.balance()


class BankAccountReadService:
    def __init__(self, repository):
        self.repository = repository

    def balance(self) -> Amount:
        # repositoryのwrapper method...
        return self.repository.balance()

    def can_withdraw(self, amount: Amount) -> bool:
        balance = self.balance()
        return balance.has(amount)


class BankAcountUpdateService:
    def __init__(self, repository):
        self.repository = repository

    def withdraw(self, amount: Amount):
        self.repository.withdraw(amount)


class BanckAccountScenario:
    # 基本サービスクラスと、ReadService, UpdateService
    # 複合サービスクラスの、Scenarioとclass分類が分かりやすくなった
    def __init__(
        self,
        read_service: BankAccountReadService,
        update_service: BankAcountUpdateService
    ):
        self.read_serivce = read_service
        self.update_service = update_service

    def withdraw(self, amount: Amount):
        # 確かにread系のclassと、update系のclassに分けることで単一責任になるようになった。
        # しかし、wrapperが多すぎない？？？
        # repositoryのwithdraw → UpdateServiceのwithdraw → Scinarioのwithdraw...
        if not self.read_serivce.can_withdraw(amount):
            raise ValueError("残高不足")
        self.update_service.withdraw(amount)
        return self.read_serivce.balance()
