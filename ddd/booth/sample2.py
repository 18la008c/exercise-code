# p77
from dataclasses import dataclass


class TaskPostponeUsecase:
    def __init__(self, task_repository):
        self.task_repository = task_repository

    def postpone(self, taskid):
        # usecase層からは、DB操作については1mmも知らなくて良い。(カプセル化)
        task = self.task_repository.find_by_id(taskid)

        # usecase層からは、postponeに関するビジネスロジックを1mmも知らなくて良い。(延期は3回まで等は全てドメインオブジェクトに隠蔽)
        # じゃあ延期4回目の場合はどうするの？？？ 例外がドメインオブジェクトから上がるの？それをusecase層はどうキャッチするの？
        # そもそも、契約によるプログラミング的には、延期4回目にならないように使う側(usecase層)で管理すべきとある。
        # そうすると、ドメインオブジェクトの基礎である、ビジネスロジックの隠蔽ができない。。。
        task.postpone()

        # ドメインオブジェクトは一部だけの変更を許さないはず。→つまり再生成が必要なはず。
        # でも上の、task.postpone()では新規objectを作っているようには見えない。
        # これはpostpone()で、実は新規objcet再生成して返しているってこと？
        self.task_repository.save(task)


class OverPostponeException(Exception):
    pass


@dataclass(frozen=True)
class Task:
    taskid: str
    postpone_num: int

    def __post_init__(self):
        # こんな感じの実装？？？
        if self.postpone > 3:
            raise OverPostponeException("規定回数以上の延期をしています。")

    def postpone(self):
        # ビジネスロジックが格納。(延期は3回まで等)
        # 延期3回超えたら、このmethodの返り値はどうなるのか？？？

        # ドメインオブジェクトは、一部ののみの変更は禁止である。
        # 従って以下のような新Objectを作るということ？？？
        # この場合旧taskのinstanceはゴミになり続けるの？
        # そもそもこんな実装で良いの？
        new_task = Task(self.taskid, self.postpone_num + 1)
        return new_task
