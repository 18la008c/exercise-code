# p50
# 低結合の例

# 高凝縮の例: countupするだけである。
class Counter:
    def __init__(self, start: int = 0):
        self.__count = start

    @property
    def count(self):
        return self.__count

    def increment(self):
        self.__count += 1


class BadPrinter:
    def __init__(self, counter: Counter) -> None:
        self.counter = counter

    def print(self):
        print(self.counter.count)


class Printer:
    @staticmethod
    def print(num: int):
        print(num)


if __name__ == "__main__":
    counter = Counter()
    counter.increment()

    # 悪い例 counterの内部実装にprinterが依存している　→ 高い結合
    BadPrinter(counter).print()

    # 良い例 counterの内部実装にはよらず、値の受け渡しだけで良い → 低い結合
    Printer.print(counter.count)
