# infra層と一番低いレイヤーなのに、importしてdomain層に依存している！！ → 依存性逆転の法則!!
# つまり、domain層は、infra層の束縛から解放された。
from domain import NetworkRepositoryInterface, Network


class NetboxNetworkRepository(NetworkRepositoryInterface):
    def get_kvm_nw(self, hostname) -> Network:
        # netboxからKVM線のNWに関して取得するmethod
        pass

    def get_maint_nw(self, hostname) -> Network:
        # netboxからメンテ線のNWに関して取得するmethod
        pass


class PhpipamNetworkRepository(NetworkRepositoryInterface):
    def get_kvm_nw(self, hostname) -> Network:
        # ipamからKVM線のNWに関して取得するmethod
        pass

    def get_maint_nw(self, hostname) -> Network:
        # ipamからメンテ線のNWに関して取得するmethod
        pass


class TestNetworkRepository(NetworkRepositoryInterface):
    SAMPLE = [
        {
            "hostname": "serverA",
            "ip": "172.22.158.100",
            "subnet": "255.255.255.0",
            "gateway": "172.22.158.1",
            "type": "maint"
        },
        {
            "hostname": "serverA",
            "ip": "192.168.0.100",
            "subnet": "255.255.255.0",
            "gateway": "192.168.0.1",
            "type": "kvm"
        },
        {
            "hostname": "serverB",
            "ip": "172.22.158.101",
            "subnet": "255.255.255.0",
            "gateway": "172.22.158.1",
            "type": "maint"
        },
        {
            "hostname": "serverB",
            "ip": "192.168.0.101",
            "subnet": "255.255.255.0",
            "gateway": "192.168.0.1",
            "type": "kvm"
        }
    ]

    def __init__(self, ip, usr, pwd):
        # mock(本来であれば、DB等にアクセスしにいく)
        self.ip = ip
        self.usr = usr
        self.pwd = pwd

    def get_kvm_nw(self, hostname):
        for nw in self.SAMPLE:
            if (nw["hostname"] == hostname) and (nw["type"] == "kvm"):
                return Network(nw["ip"], nw["subnet"], nw["gateway"])

        raise NotExistException


class NotExistException(Exception):
    pass
