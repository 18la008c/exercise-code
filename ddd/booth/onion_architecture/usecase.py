def main1():
    """
    確かに、domain層はinfra層から解き放ったけど、具象クラスを使わないと使わないと何も作れないよ？？
    従ってuse-case層はinfra層に依存するよ？ → 当然だよね？？→結局domain→use-case層に依存が変わっただけ。。。
    もしかしてjavaや例では、interface == concreteが1対1対応かつ、interfaceだけimportすれば良い的な???
    でも、以下では、Repositoryの具象として、RDBとDynamoの2パターンあるよ？？？
    https://qiita.com/little_hand_s/items/2040fba15d90b93fc124#オニオンアーキテクチャ
    やっぱり、usecase層で依存するのはしょうがないみたい(そりゃ具象クラスがないと何もできないでしょと)
    https://www.slideshare.net/ShintaroKurosawa/20160526-62511723

    #domain.py
    class NetworkRepository: # 名前が全く一緒?
        # interface

    # infra.py
    class NetworkRepository: # 名前が全く一緒?
        # concrete
        # domain.pyの、NetworkReopositoryをimportして実際に実装する。

    # usecase.py
    from domain import NetworkRepository
    # 名前が同じな上、interfaceの方が上位互換だから、domainのimportでOKになる？？？
    """
    from infra import TestNetworkRepository  # infraをimportしている == infraに依存するよ???
    hostname = "serverA"
    test_nw_repository = TestNetworkRepository("10.0.0.3", "test", "test")

    kvm_nw = test_nw_repository.get_kvm_nw(hostname)
    maint_nw = test_nw_repository.get_maint_nw(hostname)


def main2():
    """
    それとも、まだuse-case層では具象出さない的な？？？？
    そんなことないよね？？？
    これは、結局具象クラスを使うレイヤーを先送りにしかしてないので意味がない。。。
    追記
    - そんなことあった。use-case層はあくまで共通のmethodを提供するだけっぽい
    - つまり具体化は、presentation層らしい。
    - https://buildersbox.corp-sansan.com/entry/2019/07/10/110000
    - 結局オニオンアークテクチャーの外側は相互に依存して良いみたい。
    - そして外が一番具体的 → 中に入るたびに抽象(というかどこでも使い回せる)になるっぽい。
    """
    from domain import NetworkRepositoryInterface

    def get_kvm_nw(repository: NetworkRepositoryInterface, hostname):
        return repository.get_kvm_nw(hostname)

    def get_maint_nw(repository: NetworkRepositoryInterface, hostname):
        return repository.get_maint_nw(hostname)


# 余談。いかが一番オニオンアークテクチャーがわかりやすい
# https://qiita.com/cocoa-maemae/items/e3f2eabbe0877c2af8d0
