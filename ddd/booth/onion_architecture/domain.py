from dataclasses import dataclass
from abc import abstractmethod


@dataclass(frozen=True)
class Network:
    # domain-objet
    # Networkとして、必要なmethodはほぼないが、とりうる値の制約はある。(ipの形式である。gatewayのsubnetが正しいか)
    ip: str
    subnet: str
    gateway: str

    def __post_init__(self):
        # gatewayとipのsubnetが同じであることをcheckする。
        pass


class NetworkRepositoryInterface:
    # interface
    # p68 本来は集約単位でしか、repositoryは存在してはいけない!!が、今回は例のため、Networkだけを作成するRepoを作る。

    @abstractmethod
    def get_kvm_nw(self, hostname):
        pass

    @abstractmethod
    def get_maint_nw(self, hostname):
        pass
