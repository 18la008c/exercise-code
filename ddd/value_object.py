from dataclasses import dataclass


@dataclass
class Customer:
    # 微妙point
    #     1. classの肥大化かつ、更に分割できそう(name, 住所, 連絡)
    #     2. 住所系、連絡系をstringで格納しているので、不正な値を保持できる
    #     3. dataclass自体を外部から勝手に変更できてしまう

    # name系
    firstname: str
    lastname: str

    # 住所系
    post_code: str
    city: str
    address: str

    # 連絡系
    telephone: str
    mail_address: str
    can_call_telephone: bool

    # data加工method
    @property
    def fullname(self):
        return self.firstname + self.lastname


@dataclass(frozen=True)
class PersonName:
    firstname: str
    lastname: str

    @property
    def fullname(self):
        return self.firstname + self.lastname


@dataclass(frozen=True)
class Address:
    post_code: str
    city: str
    address: str

    def __post_init__(self):
        # 不正な値がないかcheckする
        pass


@dataclass(frozen=True)
class ContactMethod:
    telephone: str
    mail_address: str
    can_call_telephone: bool

    def call_by_telephone(self):
        # dataと業務ロジック(電話をかけても良い顧客なら電話をかける)を同居させた
        if self.can_call_telephone:
            pass
        else:
            raise ValueError("Cannot call by telephone")

    def call_by_email(self):
        pass


@dataclass(frozen=True)
class Customer:
    # class分割の実施
    person_name: PersonName
    address: Address
    contact: ContactMethod
