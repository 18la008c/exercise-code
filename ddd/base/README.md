# DDD基礎知識

# ドメイン層

## Entity

## ValueObject
- refs
     - https://dddinpython.com/index.php/2021/11/22/value-objects-in-python/

## DomainEvent

## Repository
- 

## Factory

## DomainService
- **複数のDOにまたがる操作を実施する**
    - 単一のDOだけでは実現不可のものが多々存在する。
    - 例:
        - user名の更新は1ヶ月に1回のみ → User一人で完結しているのでDO内部で処理できる。
        - user名はユニークである → 他のUserも確認しないNG、DO内部で完結不可 → DomainServiceへ
    - その他例
        - booth本FAQ5-1
- **DomainServiceという複数のDOを管理する、DOを作成する**
    - repositoryを使って複数のユーザーを管理するDO
    - これによって、UserはUser単体の責務を、DomainServiceによってUser全体の管理の責務をと単一責任になる。
    - もしUser(DO)に、repositoryを持たせると、Userが他のUserもcheckするようになり責務over
- **DomainServiceはRepositoryを使っても良い？→OK**
    - FAQの6.1.2参照
    - 集合に対する操作はrepositoryを扱うことが必須。しかし単体の責務しか持たないDOに、repositoryを持たせるのは責務違反。
    - そこで、DomainServiceでrepositoryを持たせて、DOは単体操作だけという責務を分ける。
    - ただしrepositoryの使用はなるべく簡素にして、基本的にはUsecase層で行うこと!!!
- **sample-codeを読んで**
    - Repositoryに、`is_unique()`移植すれば良くない？
        - そうするとUser(DO)にrepositoryが必要になり過剰責務
        - とにかくrepoistoryはsimpleにするべきだから。(booth本p69)
        - でもclass多すぎて分かりにくくなりそう。。。
- refs
    - sample
        - https://dddinpython.com/index.php/2021/11/06/where-to-place-the-business-logic-in-ddd/
    - sample-code
        - https://github.com/pgorecki/python-ddd/tree/0073ccce35c651be263f5d7d3d63f9a49bc0b78a/src/seedwork/domain
```
-疑問-
* DO内部で完結する、ビジネスルールは、AggregateRootというクラスに、BussinessRuleというクラスで実装していた。この意図をDDD本見て知る必要がある。
* ただ、1ヶ月に1回しか名前更新できない→BusinessRuleに、名前がuniqueでないとNG→DomainServiceにとまとまりがなくなるのでは？？
```

# infra層
- **外部サービス(vsphere,redfish)との連携**
    - interfaceはドメイン層、実装はinfra層にする。つまりrepositoryと一緒
    - 外部サービス自体がドメイン知識となる場合は、ドメイン層に外部サービスについて書き込んでも良い。(booth本FAQ8-2-1)
    - 