# p60
# 果たしてこのEnumに意味はあるのか？？？　無駄なwrapperになっているだけな気がする。
# BaseFeeの継承だけでグループ化できていて、わざわざ更にグループ化としてEnumを定義する意味とは？
# 確かに、Enumの中に好きな属性(adlut, child, senior)を定義できて、self.value等で呼べはするけど。。。

from enum import Enum
from abc import abstractproperty
from dataclasses import dataclass


@dataclass(frozen=True)
class Yen:
    value: int

    def __post_init__(self):
        if self.value < 0:
            raise ValueError


class BaseFee:
    @abstractproperty
    def yen(self):
        pass

    @abstractproperty
    def label(self):
        pass


class AdultFee:
    def yen(self):
        return Yen(1000)

    def label(self):
        return "大人"


class ChildFee(BaseFee):
    def yen(self):
        return Yen(500)

    def label(self):
        return "子供"


class SeniorFee(BaseFee):
    def yen(self):
        return Yen(800)

    def label(self):
        return "シニア"


class FeeType(Enum):
    adult = AdultFee()
    child = ChildFee()
    senior = SeniorFee()

    @property
    def yen(self):
        return self.value.yen()

    @property
    def label(self):
        return self.value.label()


if __name__ == "__main__":
    for fee in FeeType:
        print(f"{fee.label} is ${fee.yen}")

"""
Returns:
    大人 is $Yen(value=1000)
    子供 is $Yen(value=500)
    シニア is $Yen(value=800)
"""
