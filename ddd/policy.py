# p128
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


class Rule:
    @property
    def name(self):
        return "rule_name"

    def is_comply(self, value) -> bool:
        logger.debug(f"Rule: {self.name} is True/False")
        pass


class Policy:
    # 確かに、これならruleに対して解放閉鎖の原則が守られる
    # しかし！ 現実のruleは、「ruleAとruleBの複合条件」や、「ruleAを評価後、ruleBを評価する順序条件」
    # など、具象rule同士の繋がりが強すぎるので、以下パターンでは対応できないのでは？
    #    ruleAとruleBの複合: メンテモードじゃない & packetsが0である
    #    ruleAとruleBの順序: packetsが0であるならば、vcenterから情報を取得したい
    def __init__(self):
        self.rules = []

    def add_rule(self, rule: Rule):
        self.rules.append(rule)

    def is_comply_with_all_rules(self, value) -> bool:
        judged_rules = [
            rule.is_comply()
            for rule in self.rules
        ]
        return all(judged_rules)

    def is_comply_with_some_rules(self, value) -> bool:
        judged_rules = [
            rule.is_comply()
            for rule in self.rules
        ]
        return any(judged_rules)
