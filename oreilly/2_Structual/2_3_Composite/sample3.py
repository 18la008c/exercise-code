# stationary2.pyの改造版
#　addが、leafに対しても使えるの不安定なのでNG処理にする。

class Item:
    def __init__(self, name, *items, price=0.00):
        self.name = name
        self.price = price
        self.children = []
        if items:
            self.add(*items, can_add=True)

    @classmethod
    def create(cls, name, price):
        return cls(name, price=price)

    @classmethod
    def compose(cls, name, *items):
        return cls(name, *items)

    @property
    def composite(self):
        return bool(self.children)

    def add(self, item, *items, can_add=None):
        # leafにはaddできないよう修正！！！
        #   - しかしコンストラクタ生成時は、必然的にchildren=[]で、leaf扱いになりaddできない
        #   - そこでcan_add flagを用意して、コントロールできるようにした。
        if can_add is None:
            can_add = self.composite
        if can_add:
            self.children.extend((item,) + items)
            return

        raise ValueError("leafに対して、addは使用不可です。")

    def remove(self, item):
        self.children.remove(item)

    def __iter__(self):
        return iter(self.children)

    @property
    def price(self):
        return sum(item.price for item in self) \
            if self.children else self.__price

    @price.setter
    def price(self, price):
        self.__price = price

    def print(self, indent=""):
        print(f"{indent}${self.price:.2f} {self.name}")
        for child in self:
            child.print(indent + "    ")


def make_item(name, price):
    return Item(name, price=price)


def make_composite(name, *items):
    return Item(name, *items)


if __name__ == "__main__":
    pencil = Item.create("Pencil", 0.40)
    ruler = Item.create("Ruler", 1.60)
    eraser = make_item("Eraser", 0.20)
    pencilSet = Item.compose("Pencil Set", pencil, ruler, eraser)
    box = Item.create("Box", 1.00)
    boxedPencilSet = make_composite("Boxed Pencil Set", box, pencilSet)
    boxedPencilSet.add(pencil)
    for item in (pencil, ruler, eraser, pencilSet, boxedPencilSet):
        item.print()

    # leafにはaddができないように変更
    # これによってclientは多少、leafなのかnodeなのか意識が必要になるが、安定性の方が大事でしょう。。。
    pencil.add(eraser, box)
