# stationary1.py
# leafとnodeを別のクラスで構成する場合
# これを使うと、必要ないmethodを実装しないので処理上はごくわずかにオーバーヘッドを産まないが、
# Clientがleafなのかnodeなのか多少意識する必要がある。
# 例えば、Instance生成時に、SimpleItemなのかCompositeItemなのか等。。。

from abc import ABC, abstractmethod
import sys


class AbstractItem(ABC):

    @property
    @abstractmethod
    def composite(self):
        pass

    def __iter__(self):
        # leafは、当然他の子供(leaf)を持たないが、統一するためからのiteratorを返す
        # 今回のコードだと、leaf自体にforループをさせることはないので意味ないが。。。
        return iter([])


class SimpileItem(AbstractItem):
    # treeでいうleaf
    def __init__(self, name, price=0.00):
        self.name = name
        self.price = price

    @property
    def composite(self):
        return False

    def print(self, indent="", file=sys.stdout):
        # `.:2f`は小数第2位まで表示
        # printのfileはwriteメソッドを持つオブジェクトを入れられる。(つまり直接txtは不可なので、with open等を使う)
        print(f"{indent}${self.price:.2f} {self.name}", file=file)


class AbstractCompositeItem(AbstractItem):

    def __init__(self, *items):
        self.children = []
        if items:
            self.add(*items)

    def add(self, item, *items):
        # こうすることで、itemが1つだけでも、複数であっても対応可能
        self.children.append(item)
        if items:
            self.children.extend(items)

    def remove(self, item):
        self.children.remove(item)

    def __iter__(self):
        # list iteratorを取得する。
        return iter(self.children)


class CompositeItem(AbstractCompositeItem):

    def __init__(self, name, *items):
        super().__init__(*items)
        self.name = name

    @property
    def composite(self):
        return True

    @property
    def price(self):
        return sum(item.price for item in self)

    def print(self, indent="", file=sys.stdout):
        print(f"{indent}${self.price:.2f} {self.name}", file=file)

        for child in self:
            child.print(indent + "    ")


if __name__ == "__main__":
    pencil = SimpileItem("Pensil", 0.40)
    ruler = SimpileItem("ruler", 1.60)
    eraser = SimpileItem("Eraser", 0.20)
    pencil_set = CompositeItem("Pencil Set", pencil, ruler, eraser)
    box = SimpileItem("Box", 1.00)
    boxed_pencil_set = CompositeItem("Boxed Pencil Set", box, pencil_set)
    boxed_pencil_set.add(pencil)
    for item in (pencil, ruler, eraser, pencil_set, boxed_pencil_set):
        item.print()

    def client(item: AbstractItem):
        # AbstractItemならnodeでもleafでもOK!
        # これは、leafも空Iteratorにしたから。
        # これによりClientはleafかnodeか気にせずプログラミングできる。
        for x in item:
            x.print()
    print("leafの場合→空のIteratorになる。")
    client(pencil)

    print("nodeの場合→self.childrenが返ってくる")
    client(pencil_set)
