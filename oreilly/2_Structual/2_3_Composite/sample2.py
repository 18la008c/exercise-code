# stationary2.py
# leafもnodeも、1つのclass Itemで管理する場合

class Item:
    def __init__(self, name, *items, price=0.00):
        self.name = name
        # setterを作ると、コンストラクタでもsetterが適用される。
        self.price = price
        self.children = []
        if items:
            self.add(*items)

    @classmethod
    def create(cls, name, price):
        # classmethodは、instance化せずに使えるmethod
        # インスタンス自体を生成したい時に使える！！(コンストラクタのwrapperみたいに機能する。)
        # initは、leafでも、nodeでも使えるようになっていて引数がわかりにくい
        # 従ってこのwrapperでleafを作る
        return cls(name, price=price)

    @classmethod
    def compose(cls, name, *items):
        return cls(name, *items)

    @property
    def composite(self):
        # bool(if判定分)でTrue or Falseを返せる
        return bool(self.children)

    def add(self, item, *items):
        # oreillyでは、itertools.chainを使っているが、*argsは全てtupleで渡されるので必要ない。。。
        # itertools.chain: tupleとlist等異なる型でも結合できる。
        # self.children.extend(itertools.chain((item,), items))
        self.children.extend((item,) + items)

    def remove(self, item):
        self.children.remove(item)

    def __iter__(self):
        # listではなく、list iteratorを返す。
        return iter(self.children)

    @property
    def price(self):
        return sum(item.price for item in self) \
            if self.children else self.__price

    @price.setter
    def price(self, price):
        self.__price = price

    def print(self, indent=""):
        print(f"{indent}${self.price:.2f} {self.name}")
        for child in self:
            child.print(indent + "    ")


def make_item(name, price):
    # oreilly(p72)では、こうするとモジュール化した時に便利って書いてあったけど。。
    # あまり重要度がわからない。。。 namespaceの深さと、呼び出し名が変わっているだけでは？？？
    # <module_name>.Item.create ← 3層
    # <module_name>.make_item ←　2層
    return Item(name, price=price)


def make_composite(name, *items):
    return Item(name, *items)


if __name__ == "__main__":
    pencil = Item.create("Pencil", 0.40)
    ruler = Item.create("Ruler", 1.60)
    eraser = make_item("Eraser", 0.20)
    pencilSet = Item.compose("Pencil Set", pencil, ruler, eraser)
    box = Item.create("Box", 1.00)
    boxedPencilSet = make_composite("Boxed Pencil Set", box, pencilSet)
    boxedPencilSet.add(pencil)
    for item in (pencil, ruler, eraser, pencilSet, boxedPencilSet):
        item.print()

    # leafにもaddができる弊害！！！
    # 初期は、pencilはleaf
    assert not pencil.composite
    # でも、leafにもaddして、node(composite)に変換できる。。。
    pencil.add(eraser, box)
    assert pencil.composite
    # すると、元々持っていたpencilが消えてnodeの器だけになる。。。 →　副作用。。。
    pencil.print()
    pencil.remove(eraser)
    assert pencil.composite
    pencil.remove(box)
    assert not pencil.composite
    pencil.print()
