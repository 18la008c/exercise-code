# iteratorとlistについて
- listは、iterableであってitratorではない。
    - iterator: __next__が存在
    - iterable: __iter__が存在
- iteratorは、nextを使える = 内部で実行順番を管理している必要がある
- しかしlistは実行順番を管理せず、常にindex=0からスタートする。 → 従ってiterableである。
- でも、forループを使うにはnext()が必須だよ？？
- そこで、__iter__で、nextが使える & 初期化したiteratorを返す挙動になっている。
- これにより、forループできつつ、毎回初期化されるので実行順番を気にしないlistができる

# iterableのforループでの挙動
1. for ... in <iterable>が、iterableの__iter__を実行する。
```
>>> a = [1,2,3]
>>> dir(a)
__iter__ # ←iterしかない
```
2. iteratorが生成される。
```
>>> b = iter(a)
>>> dir(b)
__iter__, __next__ # nextが存在する！
```
3. iteratorなので、next()を備えている。
4. forが、next()を実行する。

# つまりlistオブジェクトは
- listオブジェクト本体と、listイテレーターに分けれる。
    - listオブジェクト: listイテレーターを毎回生成する役
    - listイテレーター: 実際にループを実行する役

#　自作iteratorで、__iter__の実装について
- sample1.pyでの、__iter__の実装は何がしたかったというと、
- iteratorの生成は、自作オブジェクト(AbstractCompositItem)側で行うので、listのItrator部分だけ欲しかった。
```
class AbstractCompositeItem(AbstractItem):

    def __iter__(self):
        return iter(self.chiidren)
```

# ref
- https://pouhon.net/python-iter/2067/