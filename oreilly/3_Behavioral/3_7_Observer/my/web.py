from flask import Flask, render_template
from observer import HistoryView, LiveView, SliderModel
import datetime
import sys
app = Flask(__name__)


@app.route("/")
def index():
    return render_template('index.html')


@app.route("/live")
def live():
    return render_template("sample.html")


@app.route("/history")
def history():
    pass


if __name__ == "__main__":
    historyView = HistoryView()
    liveView = LiveView()
    model = SliderModel(0, 0, 40)  # minimum, value, maximum
    model.observers_add(historyView, liveView)  # liveView produces output
    for value in (7, 23, 37):
        model.value = value                     # liveView produces output
    for value, timestamp in historyView.data:
        print("{:3} {}".format(value, datetime.datetime.fromtimestamp(
            timestamp)), file=sys.stderr)

    app.run(debug=True, host="0.0.0.0")
