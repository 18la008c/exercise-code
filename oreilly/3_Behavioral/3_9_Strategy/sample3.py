# tabulator3.py, tabulator4.py
# → ここまで簡略化できる状況が、現実にありうるのか？？？
# → 普通前処理とか入るはず。 or ここまで簡略化する状態でclass設計しろということ？？
# そして、ここまで省略するとわかりにくくない？　　どれが抽象化されているのか判断困難。。。
# → これもmodule設計等でpythonファイルを分ければ良いだけ？？

from html import escape


WINNERS = ("Nikolai Andrianov", "Matt Biondi", "Bjørn Dæhlie",
           "Birgit Fischer", "Sawao Kato", "Larisa Latynina", "Carl Lewis",
           "Michael Phelps", "Mark Spitz", "Jenny Thompson")


def main():
    htmlLayout = Layout(html_tabulator)
    for rows in range(2, 6):
        print(htmlLayout.tabulate(rows, WINNERS))
    textLayout = Layout(text_tabulator)
    for rows in range(2, 6):
        print(textLayout.tabulate(rows, WINNERS))


class Layout:
    # sample3.py
    # tabulatorの処理が簡単のため、classではなく関数にしたver

    def __init__(self, tabulator):
        self.tabulator = tabulator

    def tabulate(self, rows, items):
        return self.tabulator(rows, items)


class Layout:
    # sample4.py
    # そもそもcallable(関数)自体を、変数に代入できるから、method tabulatorすらいらない。
    def __init__(self, tabulator):
        self.tabulator = tabulator


def html_tabulator(rows, items):
    pass


def text_tabulator(rows, items):
    pass


if __name__ == "__main__":
    main()
