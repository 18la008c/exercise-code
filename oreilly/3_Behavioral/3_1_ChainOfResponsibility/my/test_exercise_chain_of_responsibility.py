import pytest
# testの時のmodule importはどうするべきか？ importする量が単純に多くなる。
# 更にmainではなく、module単位のtestの際の構築はどうするべき？？？
# 多分こういった制約も考慮したコード構築も見ていく必要がある。
from exercise_chain_of_responsibility import NullHandler, PowerHandler, FirewallHandler


def test_simple():
    actual = 1
    expected = 1
    assert expected == actual


def test_handle():
    # handleのtestを書きたいが、handleには返り値がない。。。→こういう時どうするべき?→　A. chainは、1つミスすると後ろのchain全てに影響を与えるためこれをtestすべき
    # 仮にgcpのインスタンスにapiでpowerONする場合も、testではpowerONしたくないので、mockさせるが、この時もtestはどうするのか？？
    pass
