# 何らか(今回はGCP)から、情報を取得して、その情報に応じた処理を行う例。
# 取得した情報をInfoクラスにして、各処理を、Chainにする。
# かなり応用が効きそうだが、Infoクラス自体を変更した際(追加情報取得時等)に、handlerクラス全てに影響がある
# handlerでは、当然別のhandlerと独立であることが重要なので、別のhandlerと情報連携が必要な場合にどうするか？？？
from logging import getLogger, StreamHandler, DEBUG
from Info import Info, Shell

logger = getLogger("main")
handler = StreamHandler()
handler.setLevel(DEBUG)
logger.setLevel(DEBUG)
logger.addHandler(handler)


def main():
    sh = Shell()
    info = Info("instance-1", sh)
    handler = PowerHandler(FirewallHandler(NullHandler()))
    handler.handle(info)


class NullHandler:

    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, info):
        if self.__successor is not None:
            # 次のchainのhandleが呼ばれる
            self.__successor.handle(info)
        else:
            logger.info(f"Passed　all Handler.\n{str(info)}")


class PowerHandler(NullHandler):

    def handle(self, info):
        if info.power == "TERMINATED":
            logger.info("Power OFF")
        else:
            # こんな感じで、継続処理もできる。ex.)PowerONなら継続処理的な
            # ただこれによってHandlerの順序に依存関係が出るのでやるべきじゃない？
            logger.info("Power ON")
            super().handle(info)


class FirewallHandler(NullHandler):

    def handle(self, info):
        if info.firewall == "":
            logger.info("Add firewall rules")
        else:
            super().handle(info)


if __name__ == "__main__":
    main()
