import subprocess
import os
from logging import getLogger

logger = getLogger("main.sub")


class Shell:
    def __init__(self):
        self.env_vars = os.environ.copy()

    def add_env_vars(self, env_var):
        self.env_vars.update(env_var)

    def run(self, cmd, env=None):
        if env is None:
            env = self.env_vars
        proc = subprocess.run(cmd, shell=True, capture_output=True, env=env)
        if proc.stderr:
            logger.error(
                f"Shell run ERROR. Check bellow msg\n{proc.stderr.decode()}")

        return proc.stdout.decode().rstrip("\n")


class Info:
    def __init__(self, instance_name, shell_instance):
        # こんな感じで、属性に入れるためだけのmethodがあっていいの？？？
        # @propertyにすると、情報取得が何回も走って無駄だし。。。
        # やるとしたら、情報取得用のclass →　add datasource →　update(dict)?
        self.sh = shell_instance
        self.instance_name = instance_name
        self.power = self.get_power_status()
        self.firewall = self.get_firewall_rules()

    def __str__(self):
        return "name:{}, power_state:{}, firewall_allowd_ip: {}".format(
            self.instance_name,
            self.power,
            self.firewall
        )

    def get_power_status(self):
        cmd = f'gcloud compute instances describe {self.instance_name} --format="value(status)"'
        return self.sh.run(cmd)

    def get_firewall_rules(self):
        cmd = 'gcloud compute firewall-rules describe my-mac --format="value(sourceRanges)"'
        return self.sh.run(cmd)


if __name__ == "__main__":
    sh = Shell()
    info = Info("instance-1", sh)
    print(info.power)
    print(info.firewall)
