# Chain of Responsibility

## 特徴
- 処理を要求する人(main)と、実際に処理を行う人(class Chain)を分離できる。
### 使う場合
- mainでは、**処理に関する情報を持たなくて良い**。要求だけ行う。
- これだと、
    - 後から処理が追加された場合、chainの追加だけで良い。

```
def main():
chain1(chain2(chain3(null_chain())))

class chain1:
   処理

class chain2:
   処理
```

### 使わない場合
- 以下のように、**mainが要求と処理の情報を持つ必要** がある。
- これだと、
    - 後から処理が追加された場合、mainの書き換えが必要。
    - 処理が増えるにつれて、mainが肥大化する。
```
def main():
    if 条件:
　　    処理
    elif 条件:
    処理
```

# いつ使う？
- 通常はif文と、関数で十分な気がする。
```
if var = 0:
    handle1(var)
elif var > 10:
    handle2(var)
else:
    pass
```
- 以下に該当したら検討の余地あり。
    - 処理の分類が多い + 分類の変更が多い場合 → if文がどんどん分かりにくくなるから。
    - 処理の分類自体が複雑 → if文の条件欄がどんどん醜くなるから。

# oraillyメモ
## generator, co-routine
- chain of resoponsibilityでは、処理の都合上、関数・インスタンスは生成するが、処理は一時停止して待ち受ける仕組みが必要。
- 以下のように、内側から関数・インスタンスの作成は行われるが、chain状にしたいため
```
key_handler(mouse_handler(timer_handler()))
```
- これを実装しているのが、インスタンス or ジェネレーター(コルーチン)である。
    - インスタンスであれば、`__init__`だけ自動実行で、methodを受けるまで待機できる。
    - generatorであれば、yieldまで待機する。
- 更にこれをwrapper関数で、decorateしてco-routine化している。
    - 理由は、自動でyieldまで進んで、待ち受け状態にしたいから。
    - https://qiita.com/koshigoe/items/054383a89bd51d099f10
- wrapper関数`@functools.wraps`って？
    - 通常wrapperを作成して、decorateする際には内部的にはwrapper関数が呼ばれることになる。
    - これだとdocstring等がずれてしまうので、この対処。
    - https://qiita.com/moonwalkerpoday/items/9bd987667a860adf80a2
- 他の参考
    - https://qiita.com/i-tanaka730/items/073c106c58d7c74c1706

## 今までの疑問点解消point
### 抽象クラスを使った場合に渡すべき値をどうするか？
- ポリモーフィズムを使う場合は、関数の引数を同じにする必要がある。→ 毎回同じという訳ではない！！
- 解消法1: `init`の差分を許して、methodの引数は同じにする。
    - この場合抽象クラスの`init`を、superで呼び出してオーバライドする。
- 解消法2: classに渡す値自体をclass化して対応する。
    - 今回のclass Eventのように

### `_`と`__`の違いについて
- どっちもprivate変数を表しているかと思ったら、違った。。。
- `_` single underscore
    - 単純にprivate変数・methodとして扱いたい場合
- `__` double underscore
    - 強制的なprivate変数化 and オーバライド不可
    - 意図的にオーバーライド不可等の設定を入れない限り使用しない
- https://stackoverflow.com/questions/1301346/what-is-the-meaning-of-single-and-double-underscore-before-an-object-name