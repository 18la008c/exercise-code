"""
memo
- 処理の命令をmainで、実際の処理をchainクラスで行う。→ mainとクラスが疎結合に!!
- 各処理で必要な要素(event)は、別のeventクラスで生成してあげれば良い！！

Q
- 今回は条件分岐で、必要に応じて処理をしていたが、全処理が必要な場合はどうするのか？
- add_event(class)等で、徐々に追加していく？
- でも、処理を行うごとに内容変化していくと、具象クラス間で依存関係が出る。。。
"""

import sys
import Event


def main():
    print("Handler Chain #1")
    handler1 = TimerHandler(KeyHandler(MouseHandler(NullHandler())))

    while True:
        event = Event.next()
        if event.kind == Event.TERMINATE:
            break
        handler1.handle(event)


class NullHandler:

    def __init__(self, successor=None):
        self.__successor = successor

    def handle(self, event):
        if self.__successor is not None:
            # 次のchainのhandleが呼ばれる
            self.__successor.handle(event)


class DebugHandler(NullHandler):

    def __init__(self, successor=None, file=sys.stdout):
        super().__init__(successor)

    def handle(self, event):
        self.__file.write("*DEBUG*: {}\n".format(event))
        super().handle(event)


class MouseHandler(NullHandler):

    def handle(self, event):
        # 該当のeventだったら、処理を行って終了する。次のchainが呼ばれない
        if event.kind == Event.MOUSE:
            print("Click:   {}".format(event))
        else:
            # overrideしつつ、super()で基底クラスを呼び出している。
            super().handle.event()


class KeyHandler(NullHandler):

    def handle(self, event):
        if event.kind == Event.KEYPRESS:
            print("Press:   {}".format(event))
        else:
            super().handle(event)


class TimerHandler(NullHandler):

    def handle(self, event):
        if event.kind == Event.TIMER:
            print("Timeout: {}".format(event))
        else:
            super().handle(event)


if __name__ == "__main__":
    main()
