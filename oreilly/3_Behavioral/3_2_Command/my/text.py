# reference
# https://refactoring.guru/design-patterns/command
# とにかくcommandは毎回instance化される。
from abc import ABC, abstractmethod


class BaseCommand(ABC):
    def __init__(self, app, editor):
        self.app = app
        self.editor = editor
        self.backup = ""

    def save_backup(self):
        self.backup = self.editor.text

    def undo(self):
        self.editor.text = self.backup

    @abstractmethod
    def execute(self):
        pass


class CopyCommand(BaseCommand):
    def execute(self):
        self.save_backup()
        self.app.clipboard = self.editor.get_selection()


class CutCommand(BaseCommand):
    def execute(self):
        self.save_backup()
        self.app.clipboard = self.editor.get_selection()
        self.editor.delete_selection()


class PasteCommand(BaseCommand):
    def execute(self):
        self.save_backup()
        self.editor.replace_selection(self.app.clipboard)


class UndoCommand(BaseCommand):
    def execute(self):
        self.save_backup()
        command = self.app.history.pop()
        command.undo()


class CommandHistory():
    def __init__(self):
        self.history = []

    def push(self, command):
        self.history.append(command)

    def pop(self):
        return self.history.pop()


class Editor:
    def __init__(self, text):
        self.text = text
        self.selected_text = ""

    def select_text(self, selected_text):
        # Editorにselected_textを保管するための関数
        self.selected_text = selected_text

    def get_selection(self):
        # 本来は、get_selectionで引数にselected_text指定したい。
        # しかし、そうするとコマンド生成時にselected_textを渡す必要があり、毎回のコマンド生成が大変。。。
        # とりあえずは、Editorに保管しておくことで対処。。。
        # またこの関数の用途は、app.clipboardに渡す用だが、commandを経由させている。
        return self.selected_text

    def delete_selection(self):
        self.text = self.text.replace(self.selected_text, "")

    def replace_selection(self, replace_text):
        """
        本来は、insert用途でも使用可能だがGUIが実装できない都合上、replaceオンリー
        """
        self.text = self.text.replace(self.selected_text, replace_text)


class Application:
    def __init__(self, editor, history):
        self.editor = editor
        self.history = history
        self.clipboard = ""

    def copy(self):
        copy_command = self.execute_command(CopyCommand(self, self.editor))
        copy_command.execute()

    def cut(self):
        cut_command = self.execute_command(CutCommand(self, self.editor))
        cut_command.execute()

    def paste(self):
        paste_command = self.execute_command(PasteCommand(self, self.editor))
        paste_command.execute()

    def undo(self):
        undo_command = UndoCommand(self, self.editor)
        undo_command.execute()
        self.execute_command(undo_command)

    def execute_command(self, command):
        if isinstance(command, BaseCommand):
            self.history.push(command)
        return command


if __name__ == "__main__":
    editor = Editor("kato123hiroki")
    cmd_history = CommandHistory()
    app = Application(editor, cmd_history)
    editor.select_text("123")
    app.cut()
