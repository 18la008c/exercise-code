class Command:
    def __init__(self, do, undo, description=""):
        assert callable(do) and callable(undo)
        self.do = do
        self.undo = undo
        self.description = description

    def __call__(self):
        """
        特殊メソッド、instance_name()で呼び出される。
        cmd = Command(do, undo)
        cmd()
        """
        self.do()


class Macro:

    def __init__(self, description=""):
        self.description = description
        self.__commands = []

    def add(self, command):

        if not isinstance(command, Command):
            # __name__の使い方
            # >>> str(type(int(10)))
            # <class 'int'>
            # >>> type(int(10)).__name__
            # 'int' ←文字列(名前だけ返ってくる)
            raise TypeError(
                f"Expected object of type Command, got {type(command).__name__}")
        self.__commands.append(command)

    def __call__(self):
        for command in self.__commands:
            command()

    # 以下と等しい
    # def do(self):
    #    self.__call__()
    do = __call__

    def undo(self):
        for command in reversed(self.__commands):
            command.undo()
