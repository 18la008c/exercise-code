# Command
- 参考
    - https://refactoring.guru/design-patterns/command
# いつ使う？
- オブジェクトと実行機能と、命令を分けたい場合

# 例. ボタンが配置されるGUIでの処理
- ボタンを押すと、特定の動作が起きるGUIを作成したい。
    - copy button
    - paste button
    - undo button
## NG例
- この場合は、BaseButtonを作り、継承するかたちで具象クラスを作れば良い。
```
class BaseButton:
    def __init__(self):
       pass
    
    def do(self):
       pass

class CopyButton(BaseButton):
    def do(self):
        # copyを実行する処理
        pass
```
- [問題1] copy処理をbuttonだけでなく、shortcut、右クリックmenuからも行いたい。
    - 全く同じ処理をclass CopyShortCut, class CopyClickMenuに追加する？？？ →　NG
- [問題2] 行いたい動作が変わった時にButtonを大幅に変更しなければならない。
    - copy動作の前に、文章整形処理と秘密情報をマスクする処理を入れたい → 色んなButtonに修正が入る。。。

## OK例
- 直接!動作を実行(copy, paste)するのではなく、間にclass Commandを経由させる。
- これにより、関心の分離が実現できる。
    - GUIオブジェクト(Button): 実際の動作と、buttonの機能を分離できる。つまりbuttonが持つGUI処理のみに注力すれば良い
    - Commandオブジェクト: 実際に行う動作を記述する。これによって、行いたい動作が変化してもGUI機能は変化しなくて良い。
- commandは、毎回インスタンス化されてから、buttonに追加される。
    - なぜなら、Command.execute()は、引数を持たないため、必要な値はコンストラクタで定義する必要があるため。
    - 逆にexecute()は、引数を持たないため、様々な関数で付け替え可能になる。