"""
memo

シーケンス型のiterator
- シーケンス型とは → 順番のある配列形式で、pythonだと`sample_list[10]`のように扱えるもの
- これは、`__getitem__`の特殊methodを定義すれば良い。
*** あくまでシーケンス型であって、itertorではない判定になるっぽい ***
"""


class AtoZ:
    def __getitem__(self, index):
        # 角カッコの内部が、indexに代入される。 ex.) AtoZ()[var] → varがindexに代入
        if 0 <= index < 26:
            return chr(index + ord("A"))
        raise IndexError()


class TestDict:
    # 辞書っぽいIterater
    def __getitem__(self, index):
        if index == "AAA":
            return "AAA"
        elif index == "BBB":
            return "BBB"
        else:
            raise IndexError()


class TestFor:
    # forループ時裏側確認用 (無限ループになるので注意)
    # 結果は単純に0,1,2...とindexに数字が入っていくだけ
    def __getitem__(self, index):
        return index


if __name__ == "__main__":

    # A〜Zが表示される。
    # ちなみに__getitem__はcallableになる。
    for n in AtoZ():
        print(n)

    # Zが表示される。
    tmp = AtoZ()
    print(tmp[0])

    # 何も表示されない。
    for n in TestDict():
        print(n)

    # AAAが表示される。
    print(TestDict()["AAA"])

    # 0~19表示される。
    for n in TestFor():
        if n < 20:
            print(n)
        else:
            break
