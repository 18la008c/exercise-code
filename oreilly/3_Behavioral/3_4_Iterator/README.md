# Iterator

## いつ使うのか？
- https://gitlab.com/18la008c/exercise-code/-/blob/master/headfirst/9_1_Iterator/sample1.py　参照
- 簡単にいうと、配列形式のobject毎に値の取り出し方法が異なるとややこしいので、Iteratorという共通interface(抽象クラス)を使いましょうということ
- pythonでは基本的に、for ... in ...が使えるのであまり意識することはないかも？