"""
memo

iter()を使ったiteraterの作成
- iter(callable, sentinal)は、iteraterを作る関数。
    - sentinalは、iterによる返り値がsentinaleと同じになった場合にiterationがstopする。
    - もしくは、StopIterationが呼ばれても止まる。
    - この場合は、単純にcallableを複数回呼ぶだけである。
- iterにsentinalを与えない場合は、`__iter__`メソッド or `__getitem__`を備えたobjが必須
- https://docs.python.org/ja/3/library/functions.html?highlight=iter#iter
"""


class Presidents:
    __names = (
        "George Washington", "John Adams", "Thomas Jefferson",
        "Bill Clinton", "George W. Bush", "Barack Obama"
    )

    def __init__(self, first=None):
        self.index = (-1 if first is None else Presidents.__names.index(first) - 1)

    def __call__(self):
        self.index += 1
        if self.index < len(Presidents.__names):
            return Presidents.__names[self.index]
        raise StopIteration()


if __name__ == "__main__":

    # Presidents()の返り値がNoneになるまで止まらない = StopIterationが呼ばれる
    for n in iter(Presidents(), None):
        print(n)

    print("---")

    # Presidents()の返り値が、"John Adams"だったら終了する。この場合の返り値は捨てられるので注意
    for n in iter(Presidents(), "John Adams"):
        print(n)
