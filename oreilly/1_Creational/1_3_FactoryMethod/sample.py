"""
memo
- このクラスはfactoryを使用せずに作成した、いわば悪い例である。
悪いpoint
- choiceのクラスが、名前しか違わないのに14個もハードコードしてある。
- boardクラスに、盤面生成methodが存在する。(factroy methodではない。)
    - これの何がNGか分かっていないが。。。
"""

import io
import os
import sys
import tempfile


BLACK, WHITE = ("BLACK", "WHITE")


def main():
    checkers = CheckersBoard()
    print(checkers)

    chess = ChessBoard()
    print(chess)

    if sys.platform.startswith("win"):
        filename = os.path.join(tempfile.gettempdir(), "gameboard.txt")
        with open(filename, "w", encoding="utf-8") as file:
            file.write(sys.stdout.getvalue())
        print("wrote '{}'".format(filename), file=sys.__stdout__)


# ifで読み込む関数を分けれる！！
if sys.platform.startswith("win"):
    def console(char, background):
        # charがTrueのときは、charを返し、charがFalse(such as None)の時は空白を返す。
        return char or " "
    sys.stdout = io.StringIO()
else:
    def console(char, background):
        # エスケープシーケンスで、terminal用の色付き文字を設定する。
        return "\x1B[{}m{}\x1B[0m".format(
            43 if background == BLACK else 47, char or " ")


class AbstractBoard:

    def __init__(self, rows, columns):
        self.board = [
            [None for _ in range(columns)]
            for _ in range(rows)
        ]
        self.populate_board()

    def populate_board(self):
        # 具象クラスでoverwriteされないとエラーが出る。 = abstractmethod
        raise NotImplementedError

    def __str__(self):
        # 盤面をprintするかつ、console()によって、色付きにする。
        # 駒がある時は piece == str, ない時は piece == None
        squares = []
        for y, row in enumerate(self.board):
            for x, piece in enumerate(row):
                # class外の関数を読んでも良い。 = クラス単体で完結させなくても良い
                square = console(piece, BLACK if (y + x) % 2 else WHITE)
                squares.append(square)
            squares.append("\n")
        return "".join(squares)


class CheckersBoard(AbstractBoard):

    def __init__(self):
        super().__init__(10, 10)

    def populate_board(self):
        for x in range(0, 9, 2):
            for row in range(4):
                column = x + ((row + 1) % 2)
                self.board[row][column] = BlackDraught()
                self.board[row + 6][column] = WhiteDraught()


class ChessBoard(AbstractBoard):

    def __init__(self):
        super().__init__(8, 8)

    def populate_board(self):
        self.board[0][0] = BlackChessRook()
        self.board[0][1] = BlackChessKnight()
        self.board[0][2] = BlackChessBishop()
        self.board[0][3] = BlackChessQueen()
        self.board[0][4] = BlackChessKing()
        self.board[0][5] = BlackChessBishop()
        self.board[0][6] = BlackChessKnight()
        self.board[0][7] = BlackChessRook()
        self.board[7][0] = WhiteChessRook()
        self.board[7][1] = WhiteChessKnight()
        self.board[7][2] = WhiteChessBishop()
        self.board[7][3] = WhiteChessQueen()
        self.board[7][4] = WhiteChessKing()
        self.board[7][5] = WhiteChessBishop()
        self.board[7][6] = WhiteChessKnight()
        self.board[7][7] = WhiteChessRook()
        for column in range(8):
            self.board[1][column] = BlackChessPawn()
            self.board[6][column] = WhiteChessPawn()


class Piece(str):
    # 駒のclassを定義する。 ただのstringでも良いが、駒のクラスであることを明示するためclassとしている。

    # slotは、継承したclassでの属性を定義し、共有する特殊メソッド。
    # 以下の場合は空にしているので、継承したclassは属性を持たないことを保証する。
    __slots__ = ()


class BlackDraught(Piece):

    __slots__ = ()
    # このクラスでは、文字列クラスに駒の名前を入れるだけである。要するに piece = "blackdraught"
    # 何でこんなに難しくなっているかは以下
    # __new__は、class自体を生成する際に呼ばれる。 (__init__は、class生成後に呼ばれる)
    # strクラスはimmutableなので、生成後にinitは呼べない。従って__new__にstringを入れている。

    def __new__(Class):
        return super().__new__(Class, "\N{black draughts man}")


class WhiteDraught(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white draughts man}")


class BlackChessKing(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess king}")


class WhiteChessKing(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess king}")


class BlackChessQueen(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess queen}")


class WhiteChessQueen(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess queen}")


class BlackChessRook(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess rook}")


class WhiteChessRook(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess rook}")


class BlackChessBishop(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess bishop}")


class WhiteChessBishop(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess bishop}")


class BlackChessKnight(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess knight}")


class WhiteChessKnight(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess knight}")


class BlackChessPawn(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{black chess pawn}")


class WhiteChessPawn(Piece):

    __slots__ = ()

    def __new__(Class):
        return super().__new__(Class, "\N{white chess pawn}")


if __name__ == "__main__":
    main()
