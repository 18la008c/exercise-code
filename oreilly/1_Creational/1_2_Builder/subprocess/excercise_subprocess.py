import subprocess
import os


class Shell:
    def __init__(self):
        self.env_vars = os.environ.copy()

    def add_env_vars(self, env_var):
        self.env_vars.update(env_var)

    def run(self, cmd, env=None):
        if env is None:
            env = self.env_vars
        proc = subprocess.run(cmd, shell=True, capture_output=True, env=env)
        return proc.stdout.decode()


class DataSource:
    # Must Use From Builder
    cmd = None

    def parse(self, data):
        # return raw data for using debug or make new DataSource class
        return data


class Ls(DataSource):
    cmd = "ls -al"

    @staticmethod
    def parse(data):
        return "parsed\n" + data


class Df(DataSource):
    cmd = "df -h"

    @staticmethod
    def parse(data):
        return "parsed\n" + data


class Builder:
    def __init__(self, shell_instance):
        self.sh = shell_instance
        self.datasource = []

    def add_datasource(self, datasource):
        self.datasource.append(datasource)

    def build(self):
        cmd = self._gen_command()
        result_all = self.sh.run(cmd).split("-sep-")
        return [d.parse(res) for res, d in zip(result_all, self.datasource)]

    def _gen_command(self):
        return "; echo -sep-; ".join(d.cmd for d in self.datasource)


if __name__ == "__main__":
    sh = Shell()
    builder = Builder(sh)
    builder.add_datasource(Ls)
    builder.add_datasource(Df)
    builder.add_datasource(Ls)
    for n in builder.build():
        print(n)
