from typing import List
import pytest

from exercise_subprocess import Shell, Builder, Ls


def test_simple():
    actual = 1
    expected = 1
    assert expected == actual


def test_shell():
    cmd = "echo $HIROKI"
    sh = Shell()
    sh.add_env_vars({"HIROKI": "dayo-"})
    assert sh.run(cmd) == "dayo-\n"


def test_builder():
    sh = Shell()
    builder = Builder(sh)
    builder.add_datasource(Ls)
    assert isinstance(builder.build(), List) is True
