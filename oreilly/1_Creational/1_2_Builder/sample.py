
"""
memo
- abstract factoryと違い、builder自体が情報を保持する。(インスタンス化する)
- 今回はbuilderに引数は渡されない(HtmlFormBuilder())が、入れても良いかも？
    - そうすると、ポリモーフィズムが難しくなるかも？
- 最終生成物(product)が、objectでなくても良い。今回はどちらもstring(HTML, tk.py)
"""

from abc import ABC, abstractmethod
import re
import sys
# python3.2以上 or 以下の判断
# tupleの比較は、要素の前から順に比較していく
if sys.version_info[:2] < (3, 2):
    from xml.sax.saxutils import escape
else:
    from html import escape


def create_login_form(builder):
    builder.add_title("Login")
    builder.add_label("Username", 0, 0, target="username")
    builder.add_entry("username", 0, 1)
    builder.add_label("Password", 1, 0, target="password")
    builder.add_entry("password", 1, 1, kind="password")
    builder.add_button("Login", 2, 0)
    builder.add_button("Cancel", 2, 1)
    return builder.form()


class AbstractFormBuilder(ABC):
    # abc.ABCMetaはmetaclass指定での呼び出しが必要
    # abc.ABCは、ただの継承で良い。
    # 両者は同じなので、ABCの方が楽

    @abstractmethod
    def add_title(self, title):
        self.title = title

    @abstractmethod
    def form(self):
        pass

    @abstractmethod
    def add_label(self, text, row, column, **kwargs):
        pass

    @abstractmethod
    def add_entry(self, variable, row, column, **kwargs):
        pass

    @abstractmethod
    def add_button(self, text, row, columns, **kwargs):
        pass


class HtmlFormBuilder(AbstractFormBuilder):

    def __init__(self):
        self.title = "HtmlFormBuilder"
        # 完成物ではなく、builderに情報をため込んでいる！！
        self.items = {}

    def add_title(self, title):
        super().add_title(escape(title))

    def add_label(self, text, row, column, **kwargs):
        # 辞書の引数にtupleを持たせられる。呼び出しは完全一致のand条件
        self.items[(row, column)] = ('<td><label for="{}">{}:</label></td>'
                                     .format(kwargs["target"], escape(text)))

    def add_entry(self, variable, row, column, **kwargs):
        html = """<td><input name="{}" type="{}" /></td>""".format(
            variable,
            kwargs.get("kind", "text")
        )
        self.items[(row, column)] = html

    def add_button(self, text, row, columns, **kwargs):
        html = """<td><input type="submit" value="{}" /></td>""".format(
            escape(text)
        )
        self.items[(row, columns)] = html

    def form(self):
        html = [
            "<!doctype html>\n<html><head><title>{}</title></head><body>".format(
                self.title), "<form><table border='0'>"]
        this_row = None
        for k, v in sorted(self.items.items()):
            row, column = k
            if this_row is None:
                html.append("  <tr>")
            elif this_row != row:
                html.append("  </tr>\n  <tr>")
            this_row = row
            html.append("    " + v)
        html.append("  </tr>\n</table></form></body></html>")
        return "\n".join(html)


class TkFormBuilder(AbstractFormBuilder):
    # tkinter用のpython scriptを生成するclass
    # つまり返り値はただのstring

    TEMPLATE = """#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk

class {name}Form(tk.Toplevel):

    def __init__(self, master):
        super().__init__(master)
        self.withdraw()     # hide until ready to show
        self.title("{title}")
        {statements}
        self.bind("<Escape>", lambda *args: self.destroy())
        self.deiconify()    # show when widgets are created and laid out
        if self.winfo_viewable():
            self.transient(master)
        self.wait_visibility()
        self.grab_set()
        self.wait_window(self)

if __name__ == "__main__":
    application = tk.Tk()
    window = {name}Form(application)
    application.protocol("WM_DELETE_WINDOW", application.quit)
    application.mainloop()
"""

    def __init__(self):
        self.title = "TkFormBuilder"
        self.statements = []

    def add_title(self, title):
        super().add_title(title)

    def add_label(self, text, row, column, **kwargs):
        name = self._canonicalize(text)
        create = """self.{}Label = ttk.Label(self, text="{}:")""".format(
            name, text)
        layout = """self.{}Label.grid(row={}, column={}, sticky=tk.W, \
padx="0.75m", pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))

    def add_entry(self, variable, row, column, **kwargs):
        name = self._canonicalize(variable)
        extra = "" if kwargs.get("kind") != "password" else ', show="*"'
        create = "self.{}Entry = ttk.Entry(self{})".format(name, extra)
        layout = """self.{}Entry.grid(row={}, column={}, sticky=(\
tk.W, tk.E), padx="0.75m", pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))

    def add_button(self, text, row, column, **kwargs):
        name = self._canonicalize(text)
        create = ("""self.{}Button = ttk.Button(self, text="{}")"""
                  .format(name, text))
        layout = """self.{}Button.grid(row={}, column={}, padx="0.75m", \
pady="0.75m")""".format(name, row, column)
        self.statements.extend((create, layout))

    def form(self):
        return TkFormBuilder.TEMPLATE.format(title=self.title,
                                             name=self._canonicalize(
                                                 self.title, False),
                                             statements="\n        ".join(self.statements))

    def _canonicalize(self, text, startLower=True):
        # 特殊文字を消去 & 始めの文字が数字なら_を追加する。
        # 生成するpythonコードでエラーを起こさないため
        text = re.sub(r"\W+", "", text)
        if text[0].isdigit():
            return "_" + text
        return text if not startLower else text[0].lower() + text[1:]
