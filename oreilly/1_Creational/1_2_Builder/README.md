# builder
- tkinterを使うには以下対処が必要
    - https://qiita.com/saki-engineering/items/92b7ec12ed07338929a3

## いつ使うの？
- builderクラス自体に情報をkeepしたい時
- ただし正直factoryと大差ないので、好みで良い気がする。
    - builder
        - InterFace → builder → product
        - builder自体に情報をためて、product生成。
    - factory
        - InterFace → facotry → product_obj → productのmethod → product
        - productに情報をためて、productのmethodに欲しい情報を生成する。

