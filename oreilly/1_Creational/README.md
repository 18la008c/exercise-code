# 生成に関するbuildパターン

## いつ使う？
- 最終的な目的はオブジェクトを作ることだ。
- 最終的に作られるオブジェクトは1つのクラスだけだ。-yes→ builder
- 最終的に作られるオブジェクトは複数のオブジェクトから構成される。-yes→ abstract factory
- factory method
- つまり
    - builder: 1つobjectをいろんなパターンで作る時(職人芸)
    - factory: ある程度決まったいろんなobjectを作るとき(工場)

## 構成
1. director
    - builderに命令を送る人
    - どんなbuilderでも使用できないとダメ
2. builder
    - 実際に最終生成物を作成する人。
    - 最終生成物を作るために必要なparameterごとに、複数のbuilderが存在する。
    - 最終的に作るobjectは同じだが、細かいparameterごとにbuilderを分ける。`builder`
    - 最終的に作るobject自体が異なるので、異なるbuilderが存在する。`factory`
3. Object
    - 最終的に生成されるモノ
    - 複雑かつ多様性があるため、自由に作られると制御しきれない。
    - 従って作るのは、職人(builder)のみ可能である。

## constraction関連
- adapter: 現状使わない
- bridge: 開発規模が小規模なので、元のclassに追加で良い。

