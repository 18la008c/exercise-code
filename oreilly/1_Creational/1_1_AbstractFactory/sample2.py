# headfirstを読んだ後に、よくあるabstract factoryに書き換えたver
# 書き変えて気づいたこと
# - Factoryと、作成されるProductsが1:1で紐づく場合には、Factroy内部に格納した方が良い。(SvgFactory→SvgDiagram等)
# - これであれば、どのproductが、どのfactoryに紐づいているか一目瞭然。
# - また、Factory固有の定数も、毎回Productsに渡さなくても、Products自身から取得できる。(DiagramFactory.BLANK等)

from abc import ABC, abstractmethod
import tempfile
import os


def main():

    textFilename = os.path.join(tempfile.gettempdir(), "diagram.txt")
    svgFilename = os.path.join(tempfile.gettempdir(), "diagram.svg")

    txtDiagram = create_diagram(DiagramFactory())
    txtDiagram.save(textFilename)
    print("wrote", textFilename)

    svgDiagram = create_diagram(SvgDiagramFactory())
    svgDiagram.save(svgFilename)
    print("wrote", svgFilename)


def create_diagram(factory):
    diagram = factory.make_diagram(30, 7)
    rectangle = factory.make_rectangle(4, 1, 22, 5, "yellow")
    text = factory.make_text(7, 3, "Abstract Factory")
    diagram.add(rectangle)
    diagram.add(text)
    return diagram


class BaseFactroy(ABC):
    @abstractmethod
    def make_diagram(self, width, height):
        pass

    @abstractmethod
    def make_rectangle(self, x, y, width, height, fill, stroke):
        pass

    @abstractmethod
    def make_text(self, x, y, text, fontsize):
        pass


class DiagramFactory(BaseFactroy):
    # factory作成されるobject(diagram, rectangel, textで共通して使用する
    BLANK = " "
    CORNER = "+"
    HORIZONTAL = "-"
    VERTICAL = "|"

    def make_diagram(self, width, height):
        return Diagram(width, height)

    def make_rectangle(self, x, y, width, height, fill="wihte", stroke="brack"):
        return Rectangel(x, y, width, height, fill, stroke)

    def make_text(self, x, y, text, fontsize=12):
        return Text(x, y, text, fontsize)

    def _create_rectangle(width, height, fill):
        """
        こんな図形を作るだけの関数
        fill = %
        +--------------------+
        |%%%%%%%%%%%%%%%%%%%%|
        |%%%%%%%%%%%%%%%%%%%%|
        |%%%%%%%%%%%%%%%%%%%%|
        +--------------------+
        """
        # width * heightの文字列fillで満たされた行列を作成(言うなればndarray)
        rows = [[fill for _ in range(width)] for _ in range(height)]
        for x in range(1, width - 1):
            rows[0][x] = DiagramFactory.HORIZONTAL
            rows[height-1][x] = DiagramFactory.HORIZONTAL
        for y in range(1, height - 1):
            rows[y][0] = DiagramFactory.VERTICAL
            rows[y][width - 1] = DiagramFactory.VERTICAL
        for y, x in ((0, 0), (0, width - 1), (height - 1, 0),
                     (height - 1, width - 1)):
            rows[y][x] = DiagramFactory.CORNER
        return rows


class SvgDiagramFactory(BaseFactroy):
    SVG_START = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
    "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
    width="{pxwidth}px" height="{pxheight}px">"""

    SVG_END = "</svg>\n"

    SVG_RECTANGLE = """<rect x="{x}" y="{y}" width="{width}" \
height="{height}" fill="{fill}" stroke="{stroke}"/>"""

    SVG_TEXT = """<text x="{x}" y="{y}" text-anchor="left" \
font-family="sans-serif" font-size="{fontsize}">{text}</text>"""

    SVG_SCALE = 20

    def make_diagram(self, width, height):
        return SvgDiagram(width, height)

    def make_rectangle(self, x, y, width, height, fill="white", stroke="brack"):
        return SvgRectangel(x, y, width, height, fill, stroke)

    def make_text(self, x, y, text, fontsize=12):
        return SvgText(x, y, text, fontsize)


class BaseDiagram(ABC):
    @abstractmethod
    def __init__(self, width, height):
        pass

    @abstractmethod
    def add(self, width, height):
        pass

    def save(self, filenameOrFile):
        pass


class Diagram(BaseDiagram):

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.diagram = DiagramFactory._create_rectangle(self.width,
                                                        self.height,
                                                        DiagramFactory.BLANK)

    def add(self, component):
        for y, row in enumerate(component.rows):
            for x, char in enumerate(row):
                self.diagram[y + component.y][x + component.x] = char

    def save(self, filenameOrFile):
        file = (None if isinstance(filenameOrFile, str) else filenameOrFile)

        try:
            if file is None:
                file = open(filenameOrFile, "w", encoding="utf-8")
            for row in self.diagram:
                # print文のfile引数に、writeメソッドをもつインスタンスをいれると書き込まれる
                print("".join(row), file=file)

        # 例外が発生してもしなくてもfinally配下を実行する。
        finally:
            if isinstance(filenameOrFile, str) and file is not None:
                file.close()


class SvgDiagram(BaseDiagram):
    def __init__(self, width, height):
        pxwidth = width * SvgDiagramFactory.SVG_SCALE
        pxheight = height * SvgDiagramFactory.SVG_SCALE
        # locals()現時点で定義している、local変数をdict形で返す。
        # formatに**kwargsの要領で、変数に値を代入している。
        self.diagram = [SvgDiagramFactory.SVG_START.format(**locals())]
        outline = SvgRectangel(0, 0, width, height,
                               "lightgreen", "black")
        self.diagram.append(outline.svg)

    def add(self, component):
        self.diagram.append(component.svg)

    def save(self, filenameOrFile):
        file = (
            None
            if isinstance(filenameOrFile, str)
            else filenameOrFile
        )
        try:
            if file is None:
                file = open(filenameOrFile, "w", encoding="utf-8")
            file.write("\n".join(self.diagram))
            file.write("\n" + SvgDiagramFactory.SVG_END)
        finally:
            if isinstance(filenameOrFile, str) and file is not None:
                file.close()


class BaseRectangel(ABC):
    # 当然こんなクラスはいらないが、abstract factoryの概要を掴みやすくるため、抽象クラスを明示
    pass


class Rectangel(BaseRectangel):
    def __init__(self, x, y, width, height, fill, stroke):
        self.x = x
        self.y = y
        self.rows = DiagramFactory._create_rectangle(width, height,
                                                     DiagramFactory.BLANK if fill == "white" else "%")


class SvgRectangel(BaseRectangel):

    def __init__(self, x, y, width, height, fill, stroke):
        x *= SvgDiagramFactory.SVG_SCALE
        y *= SvgDiagramFactory.SVG_SCALE
        width *= SvgDiagramFactory.SVG_SCALE
        height *= SvgDiagramFactory.SVG_SCALE
        self.svg = SvgDiagramFactory.SVG_RECTANGLE.format(**locals())


class BaseText(ABC):
    pass


class Text(BaseText):
    def __init__(self, x, y, text, fontsize):
        self.x = x
        self.y = y
        # なぜリストのネスト？
        self.rows = [list(text)]


class SvgText(BaseText):
    def __init__(self, x, y, text, fontsize):
        x *= SvgDiagramFactory.SVG_SCALE
        y *= SvgDiagramFactory.SVG_SCALE
        fontsize *= SvgDiagramFactory.SVG_SCALE // 10
        self.svg = SvgDiagramFactory.SVG_TEXT.format(**locals())
