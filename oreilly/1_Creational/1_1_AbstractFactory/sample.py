"""
MEMO
- 図形を作るmethod(_create_rectangle), 図形を合成するmethod(Diagram.add)
- このように、実装や引数自体は大分複雑になっているが、実際に使用する際のInterFaceとしては、
- DiagramFactoryのclassmethodだけで良く、直感的である。
- つまりプログラムを作る時に、気にするのはまずコアな機能とそれを呼び出す機能を分ける
- コアな機能に関しては、入出力を決めて基本的に変更しない & 気にする必要がない状態して、
- 呼び出すwrapperのみ変更するようにする。
"""


def create_diagram(factory):
    diagram = factory.make_diagram(30, 7)
    rectangle = factory.make_rectangle(4, 1, 22, 5, "yellow")
    text = factory.make_text(7, 3, "Abstract Factory")
    diagram.add(rectangle)
    diagram.add(text)
    return diagram


class DiagramFactory:

    @classmethod
    def make_diagram(cls, width, height):
        return cls.Diagram(width, height)

    # Factoryは最終生成物だけでなく、部品も外から作れるようにして良い。
    # 当然部品インスタンスだけでは使えないし、最終生成物も部品を追加しないと使えないという
    # factoryだけでは不完全な物になるが、wrapperとしてcreate_diagramがあるので問題ない
    # つまりFactoryで完全完成まで持っていく必要はない。別個組み立てを設けても良い
    @classmethod
    def make_rectangle(cls, x, y, width, height, fill="white", stroke="brack"):
        return cls.Rectangel(x, y, width, height, fill, stroke)

    @classmethod
    def make_text(cls, x, y, text, fontsize=12):
        return cls.Text(x, y, text, fontsize)

    BLANK = " "
    CORNER = "+"
    HORIZONTAL = "-"
    VERTICAL = "|"

    class Diagram:

        def __init__(self, width, height):
            self.width = width
            self.height = height
            # 生成物を作るためのFactoryだけでなく、生成物自体に、Factorymethodを内包させて良い。
            # またselfに他の関数を追加しても良い + 属性を引数に指定して良い
            self.diagram = DiagramFactory._create_rectangle(self.width,
                                                            self.height,
                                                            DiagramFactory.BLANK)

        def add(self, component):
            for y, row in enumerate(component.rows):
                for x, char in enumerate(row):
                    self.diagram[y + component.y][x + component.x] = char

        # 別にcamelCaseを変数に使っても良い
        def save(self, filenameOrFile):
            file = (None if isinstance(filenameOrFile, str) else filenameOrFile)

            try:
                if file is None:
                    file = open(filenameOrFile, "w", encoding="utf-8")
                for row in self.diagram:
                    # print文のfile引数に、writeメソッドをもつインスタンスをいれると書き込まれる
                    print("".join(row), file=file)

            # 例外が発生してもしなくてもfinally配下を実行する。
            finally:
                if isinstance(filenameOrFile, str) and file is not None:
                    file.close()

    class Rectangel:

        # strokeは、svg側でしか使用しないのに、こっちでも定義必要なの？
        # A. これを生成するためのclassmethodが、svgでも共用されるから！！
        # これによって、同じ関数&同じ引数で、rectangleを作れることになり、使用しやすくなっている。
        def __init__(self, x, y, width, height, fill, stroke):
            self.x = x
            self.y = y
            self.rows = DiagramFactory._create_rectangle(width, height,
                                                         DiagramFactory.BLANK if fill == "white" else "%")

    # 各種部品classがここまで簡潔だと分かりやすい！！
    # でも小規模開発だとここまで細かくする必要ない気もする。(くどい)

    class Text:

        # これも、svg側ではfontsizeが必要なので存在している。
        # 使いもしないのに、fontsize入力が必須なの？を回避するために、wrapperである
        # `make_text`では、default値が設定されているの問題ない。
        # 逆にいうと、make_textがmini factoryになっておりここからのみ作るという方式
        def __init__(self, x, y, text, fontsize):
            self.x = x
            self.y = y
            # なぜリストのネスト？
            self.rows = [list(text)]

    def _create_rectangle(width, height, fill):
        """
        こんな図形を作るだけの関数
        fill = %
        +--------------------+
        |%%%%%%%%%%%%%%%%%%%%|
        |%%%%%%%%%%%%%%%%%%%%|
        |%%%%%%%%%%%%%%%%%%%%|
        +--------------------+
        """
        # width * heightの文字列fillで満たされた行列を作成(言うなればndarray)
        rows = [[fill for _ in range(width)] for _ in range(height)]
        for x in range(1, width - 1):
            rows[0][x] = DiagramFactory.HORIZONTAL
            rows[height-1][x] = DiagramFactory.HORIZONTAL
        for y in range(1, height - 1):
            rows[y][0] = DiagramFactory.VERTICAL
            rows[y][width - 1] = DiagramFactory.VERTICAL
        for y, x in ((0, 0), (0, width - 1), (height - 1, 0),
                     (height - 1, width - 1)):
            rows[y][x] = DiagramFactory.CORNER
        return rows


class SvgDiagramFactory(DiagramFactory):
    SVG_START = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 20010904//EN"
    "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">
<svg xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve"
    width="{pxwidth}px" height="{pxheight}px">"""

    SVG_END = "</svg>\n"

    SVG_RECTANGLE = """<rect x="{x}" y="{y}" width="{width}" \
height="{height}" fill="{fill}" stroke="{stroke}"/>"""

    SVG_TEXT = """<text x="{x}" y="{y}" text-anchor="left" \
font-family="sans-serif" font-size="{fontsize}">{text}</text>"""

    SVG_SCALE = 20

    class Diagram:

        def __init__(self, width, height):
            pxwidth = width * SvgDiagramFactory.SVG_SCALE
            pxheight = height * SvgDiagramFactory.SVG_SCALE
            # locals()現時点で定義している、local変数をdict形で返す。
            # formatに**kwargsの要領で、変数に値を代入している。
            self.diagram = [SvgDiagramFactory.SVG_START.format(**locals())]
            outline = SvgDiagramFactory.Rectangel(0, 0, width, height,
                                                  "lightgreen", "black")
            self.diagram.append(outline.svg)

        def add(self, component):
            self.diagram.append(component.svg)

        def save(self, filenameOrFile):
            file = (
                None
                if isinstance(filenameOrFile, str)
                else filenameOrFile
            )
            try:
                if file is None:
                    file = open(filenameOrFile, "w", encoding="utf-8")
                file.write("\n".join(self.diagram))
                file.write("\n" + SvgDiagramFactory.SVG_END)
            finally:
                if isinstance(filenameOrFile, str) and file is not None:
                    file.close()

    class Rectangel:

        def __init__(self, x, y, width, height, fill, stroke):
            x *= SvgDiagramFactory.SVG_SCALE
            y *= SvgDiagramFactory.SVG_SCALE
            width *= SvgDiagramFactory.SVG_SCALE
            height *= SvgDiagramFactory.SVG_SCALE
            self.svg = SvgDiagramFactory.SVG_RECTANGLE.format(**locals())

    class Text:

        def __init__(self, x, y, text, fontsize):
            x *= SvgDiagramFactory.SVG_SCALE
            y *= SvgDiagramFactory.SVG_SCALE
            fontsize *= SvgDiagramFactory.SVG_SCALE // 10
            self.svg = SvgDiagramFactory.SVG_TEXT.format(**locals())
