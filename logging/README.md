# 目的
- 以下の意味を理解する。
```
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)
```
- 複数ファイルで、共通したloggerを持たせたい。

# loggingの構成要素
- pythonのloggingの主要な役割は以下classで構成される。
    - logging.Logger    # logを管理するやつ
    - logging.Handler   # logをprintするやつ
    - logging.LogRecord # log文自体
- 細かい設定として、以下もあるが本質ではないので無視
    - logging.Formatter
    - logging.Filter

# loggingの親子関係
- reference
    - https://docs.python.org/ja/3/howto/logging-cookbook.html#logging-cookbook
## 親子
- **loggingでは、名前によって親子関係を作ることができる。**
    - 親: `logger.getLogger(name="sample")
    - 子: `logging.gettLogger(name="sample.<好きな名前>)`
## 親子による設定の継承
- **親につけた設定は、子に継承される。**
    - 例)親にformatの設定を付けると、子にも継承される。
    - `logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")`
    - 従って、複数moduleでloggingを構成する場合は、親子関係を作れば楽。
## 親子によるlogging.LogRecodeの継承
- **子のLogRecode(log文)は親に継承される。**
    - 従ってlogを出力するlogging.LogHandlerは、親にだけつければOK
## sample
- main.py
```
import logging
logger = logging.getLogger("main")
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter("%(asctime)s - %(levelname)s : %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)
```
- sub1.py
```
import logging
logger = logging.getLogger("main.sub1")
```
## __name__の取り扱い
- **使わないべき。そもそも子を作らなくても良い。**
- 簡単なscriptなら、loggerを分ける必要がない。
    - もしmoduleごとにlogの出力先を変えたい等あれば、子を作る必要はある。
- __name__を使うと、自動でlogger名をつけてくれるとあるが、実は厄介
    - 親: `__name__ == "__main__"
    - 子: `__name__ == <dirname>.<module_name>`
    - となり、親子関係ができなくなる。結局これができるのはroot-loggerを使う or __main__とmoduleが分かれている場合のみ
    - https://ichiken-usa.blogspot.com/2021/06/python-logging.html
    
# logging.Logger
- logging.Loggerは階層構造をとっている。
1. root logger
    - `logging.getLogger(name=None)`
    - 自動で生成されるloggerで、実行するpythonのloggerを統括する。
    - **基本的に触らない**
2. logger
    - `logging.getLogger(name="sample")
    - nameを指定して、生成する自作するlogger。
    - 同じnameを指定すれば同じlogger instanceを取得可能
    - **メインで使うのはこれ**

> **logging.getLogger(name=None)**
>指定された名前のロガーを返します。名前が None であれば、ロガー階層のルート (root) にあるロガーを返します。name を指定する場合には、通常は 'a', 'a.b', 'a.b.c.d' といったドット区切りの階層的な名前にします。名前の付け方はログ機能を使う開発者次第です。
>
>与えられた名前に対して、この関数はどの呼び出しでも同じロガーインスタンスを返します。したがって、ロガーインスタンスをアプリケーションの各部でやりとりする必要はありません。
## setLevel
- logger自体が扱うloglevelを決定する。
- levelが低い場合は、log自体を捨てる。

# logging.Handler
- logをどこに出力させるか決定する。
- `logging.StreamHandler(stream=None)`
    - Noneならば、logは全てstderrに出力される!? ←ホント？(INFOもstderrなのか)
- Handlerは、loggerの中で1つで良い & TOP階層につけるべき！！(root loggerでもいいし、root loggerを触らないとしたら1つ下。)
>注釈 ハンドラを、あるロガー と その祖先のロガーに接続した場合、同一レコードが複数回発行される場合があります。一般的に、ハンドラを複数のロガーに接続する必要はありません。propagate 設定が True のままになっていれば、ロガーの階層において最上位にある適切なロガーにハンドラを接続するだけで、そのハンドラは全ての子孫ロガーが記録する全てのイベントを確認することができます。一般的なシナリオでは、ハンドラをルートロガーに対してのみ接続し、残りは propagate にすべて委ねます。
>https://docs.python.org/ja/3/library/logging.html#logging.getLogger

## setLevel
- 出力するloglevelを決定する。
- loggerがそもそも扱ってるloglevelであることも重要

# 試したいこと
1. stdoutとstderrで出力を分ける方法を探る。(つまりhandlerをいい感じに扱う)
2. 複数ファイルの時のloggingの扱いを知りたい。
    - 全部`__name__`で、loggerを作成して、topのloggerにhandlerをつけるだけで良い？
    - format設定も、topから派生した子供loggerが受け取ってくれる？？
    - ここ見れば良い気がする。https://docs.python.org/ja/3/howto/logging-cookbook.html#logging-cookbook


